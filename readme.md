

## 访问权限
```text
# 配置文件web/public/passwd
# 格式 '用户：密码' 如下
root:rimke@39doo.com
# 动态密码
root:r{Y}i{m}m{d}k{h}e{i}
	{Y}：四位数的年份（例如：2024）
	{m}：两位数的月份，有前导零（例如：10 代表十月）
	{d}：两位数的日期，有前导零（例如：01 代表一月的第一天）
	{H}：24 小时制的小时数，有前导零（例如：01 代表凌晨一点）
	{i}：两位数的分钟数，有前导零（例如：09 代表九分钟）
	{s}：两位数的秒数，有前导零（例如：30 代表三十秒）
	{y}：两位数的年份（例如：24 代表 2024 年）
	{M}：月份的英文缩写（例如：Oct 代表十月）
	{F}：月份的完整英文名称（例如：October 代表十月）
	{D}：星期几的英文缩写（例如：Mon 代表星期一）
	{l}：星期几的完整英文名称（例如：Monday 代表星期一）
	{h}：12 小时制的小时数，有前导零（例如：01 代表上午一点）
	{g}：12 小时制的小时数，无前导零（例如：1 代表上午一点）
	{G}：24 小时制的小时数，无前导零（例如：1 代表凌晨一点）
	{A}：AM 或 PM（大写）
	{a}：am 或 pm（小写）


```
## 常见问题处理

1、 [W] [service.go:132] login to server failed: EOF	
	Q:  add "tls_enable = true" to [common]. eg
		[common]
		tls_enable = true
		server_addr = frp1.freefrp.net
		server_port = 7000
		token = freefrp.net

2、connect() to 120.77.36.71:42201 failed (13: Permission denied)
	/usr/sbin/setsebool httpd_can_network_connect=1
	/etc/selinux/config