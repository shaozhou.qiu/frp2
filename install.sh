#! /bin/bash

source ./src/install-bashrc.sh;
source ./src/install-nginx.sh;
source ./src/initFrpApp.sh;

if [[ $PREFIX == "" ]]; then
	PREFIX="";
fi
appPath=`pwd`;
etcPath="$PREFIX/etc";
usrConf="$etcPath/frp";
usrFrpcConfPath="$usrConf/frpc";
usrFrpsConfPath="$usrConf/frps";
usrsvPath="$PREFIX/usr/lib/systemd/system";
tmpConf="$PREFIX/tmp/frp";
logConf="$PREFIX/var/log/frp";

gconf="$appPath/conf/global.conf";
exeLPath="$appPath/bin/linux";
exeWPath="$appPath/bin/windows";
wordBit="x86_64";
ret=`whereis getconf|grep /bin/`;
if [[ $ret != "" ]]; then
    wordBit=`getconf WORD_BIT`;
fi
binPath="$PREFIX/usr/bin";

if [[ ! -d $exeLPath ]]; then
	mkdir -p $exeLPath;
fi
if [[ ! -d $exeWPath ]]; then
	mkdir -p $exeWPath;
fi
if [[ ! -d "$appPath/conf/server" ]]; then
    mkdir -p "$appPath/conf/server";
fi
if [[ ! -d $tmpConf ]]; then
    mkdir $tmpConf;
    chmod 777 $tmpConf -R;
fi
if [[ ! -d $usrFrpcConfPath ]]; then
    mkdir -p $usrFrpcConfPath;
fi
if [[ ! -d $usrFrpsConfPath ]]; then
    mkdir -p $usrFrpsConfPath;
fi
if [[ ! -d $usrConf ]]; then
    mkdir $usrConf;
    chmod 777 $usrConf -R;
fi
if [[ ! -d $logConf ]]; then
    mkdir $logConf;
    chmod 777 $logConf -R;
fi

#创建配置文件
function createGlobalConfig(){
    if [ ! -f "$gconf" ]; then
        echo "# 常规配置" > $gconf;
        echo "gconf='$gconf';" >> $gconf;
        echo "usrConf='$usrConf';" >> $gconf;
        echo "usrFrpcConfPath='$usrFrpcConfPath';" >> $gconf;
        echo "usrFrpsConfPath='$usrFrpsConfPath';" >> $gconf;
        echo "tmpConf='$tmpConf';" >> $gconf;
        echo "log=0;" >> $gconf;
        echo "appPath='$appPath';" >> $gconf;
        echo "logConf='$logConf';" >> $gconf;
        echo "wordBit=$wordBit;" >> $gconf;
        echo "binPath=$binPath;" >> $gconf;
        echo "etcPath=$etcPath;" >> $gconf;
        echo "rsyncpwd=;" >> $gconf;
        echo "# 输出日记信息 trace, debug, info, warn, error" >> $gconf;
        echo "log_level=;" >> $gconf;
        if [[ $PREFIX == "" ]]; then
            echo "mobile_termux=0;" >> $gconf;
        else
            echo "mobile_termux=1;" >> $gconf;
        fi

        echo "" >> $gconf;
        echo "# 配置服务器授权信息" >> $gconf;
        echo "appid='c2d4b4b8c91eee900825026ca0d61869566bd91e';" >> $gconf;
        echo "secret='0fb67799a4f7a048';" >> $gconf;
        echo "tokenURL='http://centos7.web.home.frp6.39doo.com:88/api/gettoken'; " >> $gconf;
        echo "apiUrl='http://centos7.web.home.frp6.39doo.com:88/api/exec?usroapioid=frplist'; " >> $gconf;
        echo "" >> $gconf;
        echo "# 备用服务器 " >> $gconf;
        for ((i==0; i<10; i++))
        do
            if [[ $i == 0 ]]; then
                i='';
            fi
            echo "server$i='http://centos7.web.home.frp$i.39doo.com:88'; " >> $gconf;
        done
    fi
}

# 创建服务
function createService(){
    if [[ ! -d $usrsvPath ]]; then
    	return 0;
    fi
    vfs=(frpc frps);
    for s in ${vfs[@]}; do
        svf="$usrsvPath/$s.service";
        echo "[Unit]"                                > $svf
        echo "Description=$s"                      >> $svf
        echo "After=network.target"                  >> $svf
        echo ""                                      >> $svf
        echo "[Service]"                             >> $svf
        #默认运行模式
        echo "Type=simple"                          >> $svf
        #后台运行模式
        #echo "Type=forking"                          >> $svf
        echo "User=nobody"                          >> $svf
        # echo "Restart=on-failure"                   >> $svf
        # echo "RestartSec=5s"                        >> $svf

        echo "ExecStart=$exeLPath/$s.sh start"    >> $svf
        echo "ExecStop=$exeLPath/$s.sh stop"      >> $svf
        echo "ExecReload=$exeLPath/$s.sh restart" >> $svf
        echo ""                                      >> $svf
        echo "[Install]"                             >> $svf
        echo "WantedBy=multi-user.target"            >> $svf
    done
    systemctl daemon-reload;
}
# 创建定时器
function createTTask(){
    # 获取当前用户名
    user=$(whoami)

    # 定义crontab文件路径
    f="/var/spool/cron/crontabs/$user"

    # 定义要添加的任务
    data=("*/5 * * * * $1/frpc.sh start" "* */12 * * * $1/frps-rsync.sh" "0 * * * * $1/frps-stream.sh update")

    # 检查是否已有crontab条目
    existing_crontab=$(crontab -l 2>/dev/null || true) # 如果没有crontab，返回空字符串而不是报错

    for d in "${data[@]}"; do
        if [[ "$existing_crontab" != *"$d"* ]]; then
            # 将新的cron任务添加到现有的crontab内容中
            existing_crontab+=$'\n'"$d"
        fi
    done

    # 使用临时文件安全地更新crontab
    temp_file=$(mktemp)
    echo "$existing_crontab" > "$temp_file"
    crontab "$temp_file"
    rm -f "$temp_file"
}
function getip(){
    iplst=`ip addr|grep inet| grep brd | awk '{print $2}'|sed -r "s/(.*)\/[0-9]+/\1/g"`;
    ips=(`echo $iplst | tr ' ' ' '`);
    # if [[ ${#ips[@]} > 1 ]]; then
    #     ip=${ips[1]};
    # else
    #     ip=${ips[0]};
    # fi
    ip=${ips[0]};
    echo $ip;
    return 1;
}

    
function lnbin(){
    src=$1;
    desc=$2;
    if [[ -d "/usr/bin" ]]; then
        if [[ ! -f "/usr/bin/$desc" ]]; then
            echo "ln $src";
            ln -s $src "/usr/bin/$desc";
        fi
    fi
    if [[ -d "$PREFIX/bin" ]]; then
        if [[ ! -f "$PREFIX/bin/$desc" ]]; then
            ln -s $src "$PREFIX/bin/$desc";
        fi
    fi
}

function createApp(){
    paths=(bin bin/linux bin/windows);
    for p in ${paths[@]}; do
        f="$appPath/$p";
        if [ ! -d "$f" ]; then
            mkdir $f;
        fi
    done

    app=$exeLPath/frpc.sh;
    echo "#! /bin/bash"         >  $app;
    echo ""                     >> $app;
    echo "source $appPath/src/global.sh;" >> $app;
    # echo "$exeLPath/updatefrps.sh"        >> $app;
    echo "source $gconf;"        >> $app;
    #echo "source $appPath/src/update.sh" >> $app;
    echo "source $appPath/src/configuration.sh;" >> $app;
    echo "source $appPath/src/frpc.sh;" >> $app;
    chmod 777 $app;
    
    app=$exeLPath/frps.sh;
    echo "#! /bin/bash"         >  $app;
    echo ""                     >> $app;
    echo "/usr/bin/frps -c $usrConf/frps.conf;" >> $app;
    chmod 777 $app;

    app=$exeLPath/download.sh;
    echo "#! /bin/bash"         >  $app;
    echo ""                     >> $app;
    echo "source $appPath/src/global.sh;" >> $app;
    echo "source $gconf;"        >> $app;
    echo "source $appPath/src/download.sh;" >> $app;    
    chmod 777 $app;

    app=$exeLPath/updatefrps.sh;
    echo "#! /bin/bash"         >  $app;
    echo ""                     >> $app;
    echo "source $appPath/src/global.sh;" >> $app;    
    echo "source $gconf;"        >> $app;
    echo "source $appPath/src/updateserver.sh;" >> $app;    
    echo "# " >> $app;
    echo "ret=\`ps -aux | grep $app | grep -v \"grep\"\`;" >> $app;
    echo "if [[ \$ret == \"\" ]]; then" >> $app;
    echo "    echo \"start....\";" >> $app;
    echo "    main;" >> $app;
    echo "else" >> $app;
    echo "    error \"is started\";" >> $app;
    echo "fi" >> $app;

    app=$exeLPath/backup.sh;
    echo "#! /bin/bash"         >  $app;
    echo ""                     >> $app;
    echo "source $appPath/src/global.sh;" >> $app;    
    echo "source $gconf;"        >> $app;
    echo "source $appPath/src/rsyncdatatoserver.sh;" >> $app;    
    echo "# " >> $app;
    echo "main;" >> $app;
    lnbin $app "backup";
    
    
    chmod 777 $app;
    echo $exeLPath;
    return 0;
}

function installTermux(){
	if [[ $PREFIX == "" ]]; then
		return 0;
	fi	
	bashrc="$etcPath/bash.bashrc";
	if [[ ! -f $bashrc ]]; then
		return 0;
	fi
	ret=`cat $bashrc|grep "$exeLPath/frpc.sh"`;
	if [[ $ret == "" ]]; then
		echo "$exeLPath/frpc.sh start" >>$bashrc;
	fi
}
function hostDemo(){
    f="$appPath/conf/host.conf"
    if [[ -f "$f" ]]; then
        return;
    fi
    source $appPath/src/demo.sh;
    conf=$(createHostConf);
    echo -e $conf > $f;
}
function createRsyncSvr(){
    fn="$exeLPath/frps-rsync.sh";
    echo "#! /bin/bash" > $fn;
    echo "source $appPath/conf/global.conf" >> $fn;
    echo "source $appPath/src/global.sh;" >> $fn;
    echo "source $appPath/src/log.sh" >> $fn;
    echo "source $appPath/src/configuration.sh" >> $fn;
    echo "source $appPath/src/rsyncsvr.sh" >> $fn;

    chmod 777 $fn;
    lnbin $fn "frps-rsync";
}


function createFrpsAdd(){ 
    fn="$exeLPath/frps-add.sh";
    echo "#! /bin/bash" > $fn;
    echo "source $appPath/conf/global.conf" >> $fn;
    echo "source $appPath/src/global.sh;" >> $fn;
    echo "source $appPath/src/log.sh" >> $fn;
    echo "source $appPath/src/configuration.sh" >> $fn;
    echo "source $appPath/src/chksvr.sh" >> $fn;

    chmod 777 "$exeLPath/frps-add.sh";
    lnbin "$exeLPath/frps-add.sh" "frps-add";
}
function createshf(){
    f="$1";
    fn="$2";
    if [[ $fn == "" ]]; then return ; fi
    if [[ $f == "" ]]; then return ; fi
    if [[ -f "$fn" ]]; then
        return;
    fi
    regAppPath=`echo $appPath|sed -r "s/\//\\\\\\\\\//g"`
    echo "#! /bin/bash" > $fn;
    cat "$f" | sed -r "s/source\s+(.+)/source $regAppPath\/src\/\1/g" >> $fn;
}

function webService(){
    chmod 777 web/public;
    chmod 766 -R  web/public/tmp;
}

function install(){
    ret=`whereis $1|grep /bin/`
    if [[ $ret != "" ]]; then 
        ret=`$1 search $2 |grep $2`
        if [[ $ret != "" ]]; then 
            $1 install $2 -y;
            return 0;
        fi
    fi
    return -1;
}
function installapp(){
    install "apt" "$1";
    if [[ $? -eq 0 ]]; then return 0; fi
    install "pkg" "$1";
    if [[ $? -eq 0 ]]; then return 0; fi
    install "yum" "$1";
    if [[ $? -eq 0 ]]; then return 0; fi
}
function checkapp(){
    app=$1;
    pkg=$2;
    ret=`whereis $app|grep /bin/`
    if [[ $ret != "" ]]; then 
        echo "$pkg ... ok"
        return 0;
    fi
    installapp "$pkg";
    if [[ $? -eq 0 ]]; then
        echo "$pkg ... ok"
        return ;
    fi
    echo "$pkg ... x"
}
function chkEnvironment(){
    echo "Check for environment dependencies:";
    checkapp "sshd" "sshd";
    checkapp "nginx" "nginx";
    installapp "libnginx-mod-stream nginx-mod-stream";
    checkapp "php-fpm" "php-fpm";
    installapp "php php-gd php-mbstring php-curl php-json php-xml";
    checkapp "frpc" "frp";
    checkapp "rsync" "rsync";
    checkapp "wget" "wget";
    checkapp "aria2c" "aria2";
    checkapp "telnet" "telnet";
}
function main(){
    chkEnvironment;
    createGlobalConfig;
    
    createService;
    appRoot=$(createApp);
    installBashrc;
    installTermux;
    initNginxConf;
    hostDemo;
    createFrpsAdd;
    createRsyncSvr;
    webService;
    initFrpApp;

    createTTask "$appRoot";
    echo "You can use 'systemclt start frps.server' to start frps server";
    echo "You can also start the frpc through a scheduled task.";
    echo "install finish!";
}
main;
