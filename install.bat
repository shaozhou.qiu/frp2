@echo off

set rootPath=%~dp0
set winPath=%rootPath%bin\windows
set frpc=%winPath%\frpc.bat
set vbsfrpc=%winPath%\frpc.vbs
set confPath=%winPath%\conf
set updatefrps=%winPath%\updatefrps.bat


mkdir "%confPath%"

echo @echo off > "%frpc%"
echo. >> "%frpc%"
echo set "rootPath=%rootPath:~0, -1%">>"%frpc%" 
echo set winPath=%winPath%>> "%frpc%"
echo set frpc=%frpc%>> "%frpc%"
echo set confPath=%confPath%>> "%frpc%"
echo set updatefrps=%updatefrps%>> "%frpc%" 
echo. >> "%frpc%" 

echo Set WshShell = CreateObject("WScript.Shell") > "%vbsfrpc%" 
echo WshShell.run """%frpc%""", 0, False >> "%vbsfrpc%" 


for /f "tokens=*" %%i in ('type "%rootPath%src\frpc.bat"') do ( 
    if "%%i" == "" ( 
        echo. >> "%frpc%" 
    ) else ( 
        echo %%i >> "%frpc%" 
  ) 
) 


rem mkdir "%rootPath%\tmp"
rem curl -o "%rootPath%\tmp\frp_0.47.0_windows_386.zip" "https://github.com/fatedier/frp/releases/download/v0.47.0/frp_0.47.0_windows_386.zip"
rem del /y "%rootPath%\tmp"

echo Create check the running status every 10 minutes.
rem 10/minute
SCHTASKS /CREATE /TN Frp-run /tr "%vbsfrpc%" /sc minute /mo 10 /F 

echo Create check update every 1 hour.
rem 1/hourly
SCHTASKS /CREATE /TN Frp-updateserver /tr "%updatefrps%" /sc HOURLY /mo 1 /F


set hostconf=%rootPath%\conf\host.conf
IF EXIST "%hostconf%" (
    goto skipchd
) ELSE (
    goto chd
)
:chd
    echo Create default host
    set prename=Vm-%CLIENTNAME%-%random%

    echo [%prename%-mstsc] > "%hostconf%"
    echo type = tcp >> "%hostconf%"
    echo local_ip = 127.0.0.1 >> "%hostconf%"
    echo local_port = 3389 >> "%hostconf%"
    echo remote_port = 43300 >> "%hostconf%"
    echo [%prename%-ssh] >> "%hostconf%"
    echo type = tcp >> "%hostconf%"
    echo local_ip = 127.0.0.1 >> "%hostconf%"
    echo local_port = 22 >> "%hostconf%"
    echo remote_port = 42200 >> "%hostconf%"
    echo [%prename%-telnet] >> "%hostconf%"
    echo type = tcp >> "%hostconf%"
    echo local_ip = 127.0.0.1 >> "%hostconf%"
    echo local_port = 23 >> "%hostconf%"
    echo remote_port = 42300 >> "%hostconf%"
    echo [%prename%-http] >> "%hostconf%"
    echo type = tcp >> "%hostconf%"
    echo local_ip = 127.0.0.1 >> "%hostconf%"
    echo local_port = 80 >> "%hostconf%"
    echo remote_port = 48000 >> "%hostconf%"
    echo [%prename%-https] >> "%hostconf%"
    echo type = tcp >> "%hostconf%"
    echo local_ip = 127.0.0.1 >> "%hostconf%"
    echo local_port = 443 >> "%hostconf%"
    echo remote_port = 44400 >> "%hostconf%"


:skipchd
    echo host.conf is initzation!

pause
