
# 配置nginx
function initNginxConf(){
    ip=$(getip);
    # preip=${ip%.*};
    if [[  -d "$etcPath/nginx/conf.d/" ]]; then
        conf="$etcPath/nginx/conf.d/frpc.conf";
        if [ ! -f "$conf" ]; then
            nginxc=$appPath/conf/nginx.conf;
            if [ ! -f "$nginxc" ]; then
                exp=$appPath/conf/nginx.example;
                cp $exp $nginxc; 
                cat $nginxc | sed -r 's/set\s+\$ipv4\s+\$server_addr/set $ipv4 '$ip'/g' > "$nginxc.bak"
                mv "$nginxc.bak" "$nginxc";
            fi
            if [[ ! -f "$conf" ]]; then
                ln -s $nginxc $conf; 
            fi
        fi
    fi
    install_nginx_stream;
    nginxStream;
    nginxAutoindex;
    nginxFrpAutoindex;
}
function install_nginx_stream(){
    ret=`whereis apt|grep "/bin/"`;
    if [[ $ret != "" ]]; then
        # 健康检查 nginx-extras;
        app=('libnginx-mod-stream' 'libnginx-mod-stream-geoip' 'libnginx-mod-stream-geoip2' 'nginx-extras');
        for a in "${app[@]}"; do
            ret=`apt search $a`;
            if [[ $ret != "" ]]; then
                apt install $a -y;
            fi
        done
    fi
}
function nginxFrpAutoindex(){
    fn=$appPath/web/nginx.conf;
    echo "" > $fn;
    echo "server {" >> $fn;
    if [[ $PREFIX == "" ]]; then
        echo "    listen  80 default_server;" >> $fn;
        echo "    listen [::]:80 ipv6only=on;" >> $fn;
    else
        echo "    listen  8081 default_server;" >> $fn;
        echo "    listen [::]:8081 ipv6only=on;" >> $fn;
    fi
    echo "    # listen  8080;" >> $fn;
    echo "    # listen [::]:8080 ipv6only=on;" >> $fn;

    echo "    set \$app_path '$appPath';" >> $fn;
    echo "    set \$web_path \$app_path/web;" >> $fn;
    echo "    set \$web_root_path \$web_path/public;" >> $fn;
        
    echo "    server_name ~^web\..+$;" >> $fn;
    echo "    server_name 127.0.0.1;" >> $fn;
    echo "    root \$web_root_path;    " >> $fn;
    echo "    index index.html index.htm;" >> $fn;
        
    echo "    try_files \$uri \$uri/ @rewrite;" >> $fn;
    echo "    location @rewrite {" >> $fn;
    echo "        # rewrite ^/(.*)$ /index.php?_url=/\$1;" >> $fn;
    echo "        rewrite ^/(.*)$ /index.html?p=/\$1;" >> $fn;
    echo "    }" >> $fn;        
    echo "    location ~ ^/exec/ {" >> $fn;        
    echo "        # return 200 '$1 -- $args';" >> $fn;        
    echo "        rewrite ^/exec/datastorage/(.*)(\\?(.+))?\$ /exec.php?f=/\$1;" >> $fn;        
    echo "        rewrite ^/exec/(.*)(\\?(.+))?\$ /exec.php?f=/\$1;" >> $fn;        
    echo "     }" >> $fn;        

    echo "    location ~ /*\\.php {" >> $fn;

    nport=`netstat -anp| grep LISTEN | grep php | grep fpm | grep maste | awk '{print $4}'`;
    lport=`echo $nport | sed -r "s/.*:+([0-9])/\1/g"`;
    if [[ $lport == $nport ]]; then
        echo "        # fastcgi_pass 127.0.0.1:9000;" >> $fn;
        # fsck=`netstat -anp | grep php | grep fpm | grep LISTENING | awk '{print $11}'`;
        fsck=`netstat -anp | grep php | grep fpm | grep sock | awk '{print $11}'`;
        echo "        # fastcgi_pass unix:/run/php/php7.3-fpm.sock;" >> $fn;
        if [[ $fsck != "" ]]; then
            echo "        fastcgi_pass unix:$fsck;" >> $fn;
        fi
    else
        echo "        # fastcgi_pass 127.0.0.1:$lport;" >> $fn;
        echo "        fastcgi_pass $nport;" >> $fn;
        echo "        # fastcgi_pass unix:/run/php/php7.3-fpm.sock;" >> $fn;
    fi
    echo "        fastcgi_index /index.php;" >> $fn;
    echo "        fastcgi_split_path_info       ^(.+\.php)(/.+)$;" >> $fn;
    echo "        fastcgi_param PATH_INFO       \$fastcgi_path_info;" >> $fn;
    echo "        fastcgi_param PATH_TRANSLATED \$web_root_path/\$fastcgi_path_info;" >> $fn;
    echo "        fastcgi_param SCRIPT_FILENAME \$web_root_path/\$fastcgi_script_name;" >> $fn;
    echo "        include                       fastcgi_params;" >> $fn;
    echo "    }" >> $fn;
    echo "    location ~ ^/datastorage/LCK- {" >> $fn;
    echo "        rewrite ^/datastorage/LCK-(.*)$ /index.php?act=dwnf&f=/LCK-$1 ;" >> $fn;
    echo "    }" >> $fn;
    echo "    location /datastorage {" >> $fn;
    echo "       alias /;" >> $fn;
    echo "    #   autoindex on;" >> $fn;
    echo "    #   autoindex_exact_size off;" >> $fn;
    echo "    #   autoindex_localtime on;" >> $fn;
    echo "       charset utf-8; " >> $fn;
    echo "    }" >> $fn;

    echo "}" >> $fn;

    if [[ -d "$PREFIX/etc/nginx/conf.d" ]]; then
      if [[ ! -f "$PREFIX/etc/nginx/conf.d/FrpAutoindex.conf" ]]; then
          ln -s $fn /etc/nginx/conf.d/FrpAutoindex.conf;
      fi
    else
        ngxconf="$PREFIX/etc/nginx/nginx.conf";
        if [[ -f "$ngxconf" ]]; then
            ret=`cat "$ngxconf" | grep "$fn"`;
            if [[ "$ret" == "" ]]; then
                cp "$ngxconf" "$ngxconf.bak";
                cat "$ngxconf" | sed -r "s!(http\s*\{)!\1\n    include $fn;!g" > $ngxconf.1;
                mv "$ngxconf.1" "$ngxconf";
            fi
        fi
    fi


}
# php 解析
function nginxPhpParse(){
    annotation="";
    ppf="$tmpConf/phpparse.tmp";
    ret=`netstat -anp|grep tcp.*php-fpm|awk '{print $4}'`;
    if [ "$ret" == "" ]; then
        annotation="# ";
    fi
    ret=`echo $ret|sed -r 's/^:+/127.0.0.1:/g'`;
    echo "" > $ppf;
    # echo "${annotation}location ~ \.php\$ {" >> $ppf;
    echo "${annotation}location ~* $appPath/nginx_autoindex/.*\.php {" >> $ppf;
    echo "${annotation}    fastcgi_pass $ret;" >> $ppf;
    echo "${annotation}    # fastcgi_pass   php-fpm;" >> $ppf;
    echo "${annotation}    # fastcgi_pass unix:/run/php/php-fpm.sock;" >> $ppf;
    echo "${annotation}    fastcgi_index /index.php;" >> $ppf;

    echo "${annotation}    fastcgi_param SCRIPT_FILENAME \$document_root/\$fastcgi_script_name;" >> $ppf;
    echo "${annotation}    include                       fastcgi_params;" >> $ppf;
    echo "${annotation}}" >> $ppf;
    echo $ppf;
}
function nginxAutoindex(){
    f=$1;
    if [[ ! -f "$f" ]]; then
        def=("$etcPath/nginx/conf.d/default.conf" "$etcPath/nginx/sites-enabled/default.conf" "$etcPath/nginx/sites-enabled/default" "$etcPath/nginx/nginx.conf");
        for f in ${def[@]}; do
            if [ -f "$f" ]; then
                nginxAutoindex $f;
                return;
            fi
        done
        return ;
    fi
    aiconf=$appPath/web/autoindex.conf;
    ret=`cat $f|grep "$aiconf"`;
    # if [[ ! -f "$aiconf" ]]; then
        echo "" > $aiconf
        echo "location /frp2_autoindex {"                       >> $aiconf
        echo "    alias $appPath/web;"               >> $aiconf
        echo "} "                                                >> $aiconf
        paths=`echo ~`;
        paths="$paths /home /media /mnt /storage/emulated/0";
        paths=(`echo "$paths" | tr ' ' ' '` );        
        for p in ${paths[@]}; do
            if [[ ! -d "$p" ]]; then
                continue;
            fi
            vpath=`echo $p | sed -r "s/.*(\/.+)$/\1/g"`;
            echo "location $vpath {"                             >> $aiconf
            echo "       alias $p;"                              >> $aiconf
            echo "       autoindex on;"                          >> $aiconf
            echo "       autoindex_exact_size off;"              >> $aiconf
            echo "       autoindex_localtime on;"                >> $aiconf
            echo "       charset utf-8; "                        >> $aiconf
            echo "       error_page 404 403 /frp2_autoindex/404.html;"                        >> $aiconf
            echo "       add_before_body /frp2_autoindex/autoindex_before.html;">> $aiconf
            echo "       add_after_body /frp2_autoindex/autoindex_after.html;"  >> $aiconf
            echo "}" >> $aiconf
        done
        phpParse=$(nginxPhpParse);
        cat $phpParse >> $aiconf;
    # fi
    if [[ "$ret" == "" ]]; then
        cat $f | sed -r "s|^(\s*)(location\s+\/\s*\{)|\1 # frp autoindex.\n\1#include $aiconf;\n\1\2|g" > "$f.tmp";
        mv "$f.tmp" "$f";
    fi
}
# 端口重定向
function nginxStream(){
    nginxf="$etcPath/nginx/nginx.conf";
    if [[ -f $nginxf ]]; then
        ret=`cat $nginxf|grep -e "^\s*stream\s*{"`;
        if [[ $ret == "" ]]; then
            if [[ ! -d "$etcPath/nginx/stream.d" ]]; then
                mkdir "$etcPath/nginx/stream.d";
            fi
            echo "" >> $nginxf
            echo "# Frp returns the proxy configuration" >> $nginxf
            echo "stream {" >> $nginxf
            echo "    include $etcPath/nginx/stream.d/*.conf;" >> $nginxf
            echo "}" >> $nginxf
        fi
    fi

    app="$exeLPath/frps-stream.sh";
    echo "#! /bin/bash"         >  $app;
    echo ""                     >> $app;
    echo "source $appPath/conf/global.conf;" >> $app;
    echo "source $appPath/src/global.sh;" >> $app;
    echo "source $appPath/src/configuration.sh;" >> $app;
    echo "source $appPath/src/stream.sh;" >> $app;
    echo "main \$*;" >> $app;
    chmod 777 $app;
    if [[ ! -d "$appPath/web/public/tmp" ]]; then
        mkdir $appPath/web/public/tmp;
        chmod 777 $appPath/web/public/tmp;
    fi
    if [[ ! -d "$appPath/web/public/tmp" ]]; then
        mkdir $appPath/web/public/log;
        chmod 777 $appPath/web/public/log;
    fi
    lnbin $app "frps-stream";
}
    
