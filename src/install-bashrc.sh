#! /bin/bash

installBashrc(){
    if [[ $PREFIX == "" ]]; then
        return 0;
    fi  
    bashrc="$etcPath/bash.bashrc";
    if [[ ! -f $bashrc ]]; then
        return 0;
    fi
    ret=`cat $bashrc|grep "runapp"`;
    if [[ $ret == "" ]]; then
		# 定义函数
		echo 'function runapp(){' >> $bashrc
		echo '    ret=$(ps -ef | grep "$1" | grep -v "grep")' >> $bashrc
		echo '    if [[ "$ret" == "" ]]; then' >> $bashrc
		echo '        "$1" &'  # 注意这里添加了 & 符号让命令后台运行
		echo '    fi' >> $bashrc
		echo '}' >> $bashrc

		# 添加对特定应用的调用（如果需要）
		echo '# 调用runapp函数启动指定服务' >> $bashrc
		echo 'runapp "sshd"' >> $bashrc
		echo 'runapp "nginx"' >> $bashrc
		echo 'runapp "php-fpm"' >> $bashrc
    fi
}