
function getfrps(){
    ret=`ps -ef | grep frpc`;
    echo $ret;
}
function main(){
    if [[ ! -d "/etc/nginx/conf.d/" ]]; then
        return ;
    fi
    upn="frp_web_$RANDOM$RANDOM";
    conf="/etc/nginx/conf.d/$1.conf";
    frps=$(getfrps);
    #OIPS="$IPS";
    echo "upstream $upn {" > $conf
    for l in ${frps[@]}; do
        fn=${l##*/};
        ip=${fn%.*};
        if [[ "$fn" != "$ip" ]]; then
            echo "    server $ip;" >> $conf
        fi
    done
    echo "}" >> $conf
    echo "server {" >> $conf
    echo "    listen 80;" >> $conf
    echo "    server_name ~.*$1;" >> $conf
    echo "    location  / {" >> $conf
    echo "        proxy_pass http://$upn;" >> $conf
    echo "        index index.htm index.html index.php;" >> $conf
    echo "        proxy_set_header Host \$host;" >> $conf
    echo "        proxy_set_header X-Real-IP \$remote_addr;" >> $conf
    echo "    }" >> $conf
    echo "}" >> $conf
    ret=`cat /etc/hosts | grep " $1"`;
    if [[ "$ret" == ""  ]]; then
        echo "127.0.0.1 $1" >> /etc/hosts;
        echo "127.0.0.1 web.$1" >> /etc/hosts;
    fi
    nginx -s reload;
}
