#守护进程
 #
 # 错误消息输出
 # @param  1 
 # @return 成功返回为0， 失败返回为错误代码
 #
logf="$logConf/frpc-`date "+%Y%m%d"`.log";
touch $logf;


#根据配置文件启动frpc
function megerConf(){
	hostf="$appPath/conf/host.conf";
	if [[ ! -f "$hostf"  ]]; then
		return ;
	fi
	for f in `ls $appPath/conf/server/*.conf`; do
		nconf="$usrFrpcConfPath/${f##*/}";
        server_addr=$(getServerAddr "$f");

		cat $f > $nconf;
		echo "" >> $nconf;
		cat $hostf | sed -r "s/frp\.39doo\.com/$server_addr/g"  >> $nconf;
		
	done
}
 #
 # 合并文件
 # @param  1 
 # @return 成功返回为0， 失败返回为错误代码
 #
function megerFile(){
	sconf=$1;
	uconf=$2;
    if [ ! -f "$sconf" ]; then
    	error "Configuration file failed to load! $sconf";
    	return 1;
    fi
    if [ ! -f "$uconf" ]; then
    	error "Configuration file failed to load! $uconf";
    	return 2;
    fi
    chkTmpPath;

    fn=${sconf##*/};
    nconf="$tmpConf/$fn";

    cat $sconf > $nconf;
    if [[ -n "$log_level" ]]; then
	    echo "log_file = $logConf/$fn.log" >> $nconf;
	    echo "log_level = $log_level" >> $nconf;
	    echo "log_max_days = 1" >> $nconf;
    fi

    echo ""    >> $nconf;
    rnd=$[RANDOM%99];
    doname=`cat $sconf | grep ^server_addr|awk '{print $3}'`;
    tmpf="$tmpConf/tmp";
    cat $uconf|perl -p -e "s|frp.39doo.com|$doname|g" > "$tmpf.0";
    cat "$tmpf.0"|sed -r "s/(\[[^]]+)(])/\1-$rnd\2/g"  > "$tmpf.1";
    cat "$tmpf.1" >> $nconf;
    rm "$tmpf.*" -rf;
    echo "ok : $nconf" >> $logf;

}
function startByVersion(){
	app=$1;
	conf=$2;
	sysPath="$appPath/bin/linux";
    if [[ $wordBit == "32" ]]; then
        sysPath="$sysPath/386"; 
    else 
        sysPath="$sysPath/amd64";
    fi	
    if [[ ! -d "$sysPath" ]]; then
    	mkdir -p $sysPath;
    fi
	app="$sysPath/$app";
	if [[ ! -f $app ]]; then
		# ret=`ls ${app}* -r | sed -n '1,1p'`;
		# if [[ "$ret" != "" ]]; then
		# 	app=$ret;
		# fi
        # if [[ ! -f $app ]]; then
            app="frpc";
        # fi
		isExist=1;
	fi
	if [[ $isExist == 1 ]]; then
		if [[ "$conf" == "" ]]; then
			return 0;
		fi
        if [[ "$mobile_termux" != "1" ]]; then
          echo -ne "\r$app -c $conf ";
        fi
        svraddr=$(getServerAddr "$conf");
        if [[ $log == "1" ]]; then
		    svrlog="$logConf/${conf##*/}-`date "+%Y%m%d"`.log";
		    nohup $app -c $conf >> $svrlog 2>&1 &
        else
            nohup $app -c $conf > /dev/null 2>&1 &
        fi
		pid=`ps -aux | grep $conf | grep -v "grep" | awk '{print $2}'`;
		echo "$pid    $app -c $conf" >> $logf;
	fi
}

#
 # 重启APP
 # @return 成功返回为0， 失败返回为错误代码
 #
function restart(){
	stop;
	start;
}
#
 # 停止APP
 # @return 成功返回为0， 失败返回为错误代码
 #
function stop(){
	pids=`ps -aux | grep frpc|grep '.conf'| awk '{print $2}'`;
	for pid in ${pids[@]}; do
		kill $pid;
		echo "kill $pid" >> $logf;
	done
}
#
 # 启动APP
 # @return 成功返回为0， 失败返回为错误代码
 #
function start(){
    if [[ "$mobile_termux" == "1" ]]; then
        echo "local ip:";
        ip addr | grep "inet " | sed -r "s/inet ([0-9\.]+).*/\1/g";
        ifconfig | grep inet | awk '{print $2}';
        ports=$(getFrpcPort);
        echo "Listen port : $ports";
        if [[ $laststatus != "" ]]; then
            return ;
        fi
    fi
    # 使用 pgrep 判断程序是否在运行
    ret=$(pgrep -fc "[${0:0:1}]${0:1}")    
    if [ "$ret" -gt 1 ]; then
        echo "The program is already running..."
        exit
    fi
    echo -ne "\rMeger frpc config ...";
	megerConf;
	if [[ ! -d "$usrFrpcConfPath" ]]; then
		mkdir -p "$usrFrpcConfPath"; 
	fi
    echo -ne "\rStart config ...";
	confs=`ls $usrFrpcConfPath/*.conf`;
    count=0;
	for conf in ${confs[@]}; do
        #  判断常驻服务器有多少个
        ((count++));
        if [[ "$mobile_termux" == "1" ]]; then
            if (( count > 10 )); then
                count=0;
                sleep 1;
                ret=`ps -ef | grep frpc | wc -l`;
                if (( ret > 10 )); then
                    sleep 3;
                fi
            fi
        fi

		ret=` ps -aux | grep $conf | grep -v "grep"`;
		if [[ $ret != "" ]]; then
            if [[ "$mobile_termux" != "1" ]]; then
                error "started";
            fi
			continue;
		fi
    	fn=${conf##*.ver.};
    	if [ $fn == "$conf" ]; then
    		startByVersion "frpc" $conf;
    		continue;
    	fi
    	fnl=`expr ${#fn} - 5`;
    	fn=${fn:0:$fnl};
    	startByVersion "frpc.$fn" $conf;

	done

    if [[ "$mobile_termux" == "1" ]]; then
        showas;
    else
        showruningsvr;
        echo "ok";
    fi
	# exit;
}
function showruningsvr(){
    pid=`ps -ef | grep frpc | awk '{print $10}'`;
    ret=`echo $pid | sed -r 's|.*frpc/(.*)\.conf|\1|g'`;
    echo $ret;

}
function showas(){
    # 如果不是第一次启动则退出
    if [[ $laststatus != "" ]]; then
        return ;
    fi
    # showAvailableServer;
}
laststatus=`ps -ef | grep "frpc -c" | grep -v "grep"`;

case $1 in
    stop) stop ;;
    start) start ;;
    restart) restart ;;
    show) showas ;;
    -c) /usr/bin/frpc -c $2;;
    *) echo 'input [$1]';;
esac
