
# 毫秒转秒
function millisecond2second(){
    ms="00000$1";
    s=$(($1 / 1000));
    ms=$(($1 % 1000));
    echo "$s.$ms";
}
#function millisecond2second(){
#    sec="00000$1";
#    ms=${sec:0-3:3};
#    s=${sec:0-6:3};
#    t=`echo $s | sed -r "s/^[0-9]+//g"`;
#    s=$t;
#    if [[ $s == "" ]]; then
#    	s=0;
#    fi
#    echo "$s.$ms";
#}
 
function log(){
	echo -e "$1";
}
function error(){
	log "\e[31m $1 \e[0m";
}
function info(){
	log "$1";
}

function match(){
	txt=$1;
	start=$2;
	end=$3;
	# 删除左边字符，保留右边字符
    ret=${txt#*$start};
    echo $ret; 
    if [[ "$ret" == "$txt" ]]; then
		echo "";
		return ;
	fi
	# 删除右边字符，保留左边字符
    l=${ret%%$end*};
    if [[ "$ret" == "$l" ]]; then
		echo "";
		return ;
	fi
	echo $l;
} 
function getLocationOrigin(){
	url=$1;
	echo `echo $url|grep -Eo "http.*//[^\/]*"`;
}
function getLocationpathname(){
	url=$1;
	echo `echo $url|sed -r "s/http.*\/\/[^\/]*(\/.*)/\1/g"`;
}
function getHttpCode(){
	html=$1;
	code=`echo $html|sed -r "s/.*([0-9]{3})$/\1/g"`;
	echo $code;
}
function initServerConf(){
	echo "test default server.....";
	ret=`curl --connect-timeout 1 -w %{http_code} "$tokenURL"`;
	code=`getHttpCode "$ret"`;
	if [[ $code == '200' ]]; then
		return;
	fi
	source $appPath/conf/lastconf.conf;
	echo "test last server.....";
	ret=`curl --connect-timeout 1 -w %{http_code} "$lst_server"`;
	code=`getHttpCode "$ret"`;
	if [[ $code == '200' ]]; then
		lorigin=$(getLocationOrigin $lst_server);
		lpn=$(getLocationpathname $tokenURL);
		tokenURL="$lorigin$lpn";
		lpn=$(getLocationpathname $apiUrl);
		apiUrl="$lorigin$lpn";		
		return;
	fi
	for ((i=0; i<=50; i++))
	do
		svr="server";
		if [[ $i != 0 ]]; then
			svr="$svr$i"
		fi
		svr="\$$svr"
	  	url=`eval echo $svr`;
	  	if [[ $url == '' ]]; then
	  		break;
	  	fi
	  	echo "test backup sever [ $i ]";
	  	echo "$url....";
		ret=`curl --connect-timeout 1 -w %{http_code} "$url"`;
		code=`getHttpCode "$ret"`;
		if [[ $code == "200" ]]; then
			echo 'Currently available services..';
			lorigin=$(getLocationOrigin $url);
			lpn=$(getLocationpathname $tokenURL);
			tokenURL="$lorigin$lpn";
			lpn=$(getLocationpathname $apiUrl);
			apiUrl="$lorigin$lpn";

	  		echo $tokenURL;
	  		echo $apiUrl;
	  		echo "lst_server=$lorigin" > $appPath/conf/lastconf.conf;
	  		return ;
	  	fi
	done
}
function chkTmpPath(){
	if [[ ! -d "$tmpConf" ]]; then
    	mkdir $tmpConf;
    fi	
}
function httpget(){
    url=$1;
    referer="$url";
    chkTmpPath;
    if [[ -f "$tmpConf/referer.log" ]]; then
    	referer=`cat $tmpConf/referer.log`;
    fi
    ret=`curl --referer "$referer"  -c $tmpConf/cookies.log -b $tmpConf/cookies.log --connect-timeout 10  $url`;
    echo $url >  $tmpConf/referer.log;
    echo $ret;
}
function httppost(){
    url=$1;
    params=$2;
    referer="$url";
    chkTmpPath;
    if [[ -f "$tmpConf/referer.log" ]]; then
    	referer=`cat $tmpConf/referer.log`;
    fi
    ret=`curl --referer "$referer" -d "$params" -c $tmpConf/cookies.log -b $tmpConf/cookies.log --connect-timeout 10  $url`;
    echo $url >  $tmpConf/referer.log;
    echo $ret;
}
function oneCurl(){
    url=$1;
    # url="frpc6.39doo";
    ret=`ps -aux| grep -v grep | grep $url `;
    if [[ "$ret" != "" ]]; then
        echo "";
        return ;
    fi
    ret=$(httpget $url);
    echo $ret;
}
 ##
 # substring
 # @param  string text
 # @param  int start pos
 # @param  int end pos
 # @return [type]       [description]
 ##
 function substr(){
	txt=$1;
	start=$2;
	end=$3;
	if [[ $start == "" ]]; then
		start=0;
	fi
	if [[ $end == "" ]]; then
		end=0;
	fi
	ret=${txt:$start:$end};
	echo $ret;
 }

 
