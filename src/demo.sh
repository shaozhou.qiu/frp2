#! /bin/bash
function createGlobalFrpcDemo(){
    echo  $(createAppConf $1);
}
function createFrpc(){
    flag=$1;
    echo "[common]\n";
    echo "server_addr = frp.39doo.com\n";
    echo "server_port = 7000\n";
    echo "token = 39doo\n";
    echo "privilege_token = 39doo\n";
    echo "#user = $flag\n";
    echo "\n";
    echo $(createAppConf $flag);
}

function createHostConf(){
    # fid=`lsblk|grep '^sd'|tr -s ' '|cut -d " " -f4|head -n 1|sed -r 's/[\s\.]//g'`;
    fid=$RANDOM
    feid="rimke-$fid";
    tcpn=(ssh mstsc rsync mysql rtmp)
    tcpp=(80 22 3389 873 3306 1935)
    idx=0;
    for n in ${tcpn[@]}; do
        port=${tcpp[$idx]};
        idx=$((idx+1));
        echo "[$feid-tcp-$n]\n"
        echo "type = tcp\n"
        echo "local_ip = 127.0.0.1\n"
        echo "local_port = $port\n"
        echo "remote_port = 4${port:0:2}00\n"
        echo "\n"
    done
    echo "[$feid-http1]\n"
    echo "type = http\n"
    echo "local_port = 80\n"
    echo "custom_domains = $fid.frp.39doo.com\n"
    echo "#subdomain = web\n"
    echo "\n"
    echo "[$feid-https1]\n"
    echo "type = https\n"
    echo "local_port = 443\n"
    echo "custom_domains = $fid.frp.39doo.com\n"
    echo "#subdomain = web\n"
    echo "[$feid-http2]\n"
    echo "type = http\n"
    echo "local_port = 80\n"
    echo "custom_domains = *.$fid.frp.39doo.com\n"
    echo "#subdomain = web\n"
    echo "\n"
    echo "[$feid-https2]\n"
    echo "type = https\n"
    echo "local_port = 443\n"
    echo "custom_domains = *.$fid.frp.39doo.com\n"
    echo "#subdomain = web\n"
}

function createAppConf(){
    # flgN=$1;
    prts=`nmap 127.0.0.1`;
    ip=`ifconfig | grep inet | grep -v "inet6" | grep -v "127.0.0.1"|perl -p -e "s|.*inet\s*(\d+\.\d+\.\d+\.\d+).*|\1|g"`;
    ipsuffix=`echo $ip|perl -p -e "s|.*\.(\d+)|\1|g"`;
    flgN="`hostname`-$ipsuffix";

    echo "\n\n############# WEB #############\n" ;
    opnp=('http' 'https');
    for app in "${opnp[@]}"; do
        ret=`echo $prts | perl -p -e "s|.*?(\d+)\/tcp\s*open\s*$app .*|\1|g"`;
        echo "\n" ;
        if [[ $ret != "" ]]; then
            echo "[$flgN-$app]\n" ;
            echo "type = $app\n" ;
            echo "local_ip = 127.0.0.1\n" ;
            echo "local_port = $ret\n" ;
            echo "custom_domains = *.web.$flgN.frp.39doo.com\n" ;
            echo "#subdomain = $flgN\n" ;
        fi
    done

    echo "\n\n############# TCP #############\n" ;
    opnp=('ssh' 'mysql' 'ms-wbt-server');
    for app in "${opnp[@]}"; do
        ret=`echo $prts | perl -p -e "s|.*?(\d+)\/tcp\s*open\s*$app .*|\1|g"`;
        echo "\n" ;
        if [[ $ret != "" ]]; then
            echo "[$flgN-$app]\n" ;
            echo "type = tcp\n" ;
            echo "local_ip = 127.0.0.1\n" ;
            echo "local_port = $ret\n" ;
            echo "remote_port = $ret\n" ;
        fi
    done

}

function createFrps(){

    echo "[common]\n";
    echo "bind_addr = 0.0.0.0\n";
    echo "bind_port = 7000\n";
    echo "token = 39doo\n";
    echo "privilege_token = 39doo\n";
    echo "\n";
    echo "\n";
    echo "vhost_http_port=80\n";
    echo "vhost_https_port=443\n";
    echo "\n";
    echo "\n";
    echo "#管理面板账号 \n";
    echo "dashboard_user = admin\n";
    echo "#管理面板密码 \n";
    echo "dashboard_pwd = admin\n";
    echo "# 这个是frp内网穿透服务器的web界面的端口，可以通过http://你的ip:7500查看frp内网穿透服务器端的连接情况，和各个frp内网穿透客户端的连接情况。 \n";
    echo "dashboard_port = 65000 \n";
    echo "# 方括号内的ssh是代理名称，在同一个frp穿透服务器下的代理名称不能重复，否则不能启动。 \n";

}