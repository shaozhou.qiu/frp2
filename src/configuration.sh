

function getServerAddr(){ echo $(getConf "$1" "server_addr"); }
function getConf(){
    val=`cat $1 | grep "$2"|sed -r "s/.*=\s*(.+)/\1/g"`;
    echo $val;
}


function getFrpcPort(){
    lport=`cat $appPath/conf/host.conf | grep "remote_port" | awk '{print $3}'`;
    # ports=(`echo "$lport" | tr ' ' ' '` );
    echo $lport;
}
function showAvailableServer(){
    confs=`ls $usrFrpcConfPath/*.conf`;
    rport=`cat $appPath/conf/host.conf | grep "remote_port" | sed -r "s/.* ([0-9]+)/\1/g"`;
    rport=`echo $rport | sed -r ":a;N;s/\s*\n\s*/ /g;ta"`;
    echo "ports : $rport";
    rport=(`echo "$rport" | tr ' ' ' '` );
    availableFrps='frps server list:';
    ts=100; n=0;
    for (( i=0; i<3; i++ )); do
        sec=$(millisecond2second $ts);
        for conf in ${confs[@]}; do
            ret=`ps -ef| grep "$conf"| grep -v "grep"`
            if [[ $ret == "" ]]; then
                continue;
            fi
            svraddr=$(getServerAddr "$conf");
            for p in ${rport[@]}; do
                echo -ne "\r $svraddr $p ..."
                ret=`echo -e '\x1dclose\x0d' | timeout --signal=9 $sec telnet $svraddr $p  2>&1`;
                connected=`echo $ret | grep "Connected"`;
                if [[ $connected != "" ]]; then
                    # echo -ne "\r";
                    # echo "frp server : $svraddr";
                    availableFrps="$availableFrps\n$svraddr    ";
                    n=$(( n + 1 ));
                    break;
                fi
                break;
            done
        done
        if [[ $n > 0 ]]; then
            break;
        fi
        ts=$(( ts + 500 ));
    done    
    echo -e "$availableFrps";
}