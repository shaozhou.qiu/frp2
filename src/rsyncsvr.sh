chkTmpPath;
function rsyncwsvr(){
    rspwd="$tmpConf/rsync.pwd";
    echo "$rsyncpwd" > $rspwd;
    chmod 600 $rspwd;    
    svr="$1";
    port="$2";
    ret=`echo -e '\x1dclose\x0d' | timeout --signal=9 1 telnet $svr $port  2>&1`;
    connected=`echo $ret | grep "Connected"`;
    if [[ $connected != "" ]]; then
        info=`rsync -rzv --port=$port --contimeout 10 --delete --password-file=$rspwd rsyncusr@$svr::frpsserver $appPath/conf/server/`;
        ret=`echo $info|grep "received"`
        if [[ $ret != "" ]]; then
            echo "$svr" > $tmpConf/rsyncsuccess.conf
            echo 0;
            break;
        else
            echo 1;
        fi
    fi
    rm -rf $rspwd;
}


if [[ -f "$tmpConf/rsyncsuccess.conf" ]]; then
    sucs=`cat $tmpConf/rsyncsuccess.conf`
else 
    sucs="frp6.39doo.com";
fi

servers=`ls  $appPath/conf/server/ | sed -r ":a;N;s/\.conf[\n\s]/ /g;ta"`;
servers=(`echo "$sucs 127.0.0.1 $servers" | tr ' ' ' '` );
port=48700;
ret=$(rsyncwsvr 127.0.0.1 48700);
if [[ $ret == "0" ]]; then
    echo " ok";
else
    for svr in ${servers[@]}; do
        echo  -ne "\rtest $svr:$port ...       ";
        ret=$(rsyncwsvr $svr $port);
        if [[ $ret == "0" ]]; then
            echo " ok";
            break;
        fi
    done    
fi


# linux
# rsync  -rzv -e "ssh -o PubkeyAuthentication=yes -o stricthostkeychecking=no -p42201 "  127.0.0.1:/opt/frp2/conf/server/ /data/data/com.termux/files/usr/opt/frp2/conf/server/ 

# android
# rsync  -rzv -e "ssh -o PubkeyAuthentication=yes -o stricthostkeychecking=no -p42201 "  root@127.0.0.1:/opt/frp2/conf/server/ /data/data/com.termux/files/usr/opt/frp2/conf/server/