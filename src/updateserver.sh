

tmpFile="$tmpConf/cache.tmp";

chkTmpPath;
# /**
# * 获取access_token
# * @return {[type]} [description]
# */
function get_token(){
    oneCurl  "$tokenURL?appid=$appid&secret=$secret"  > $tmpFile ;
    data=$(cat $tmpFile);
    rm $tmpFile -rf;
    access_token=${data#*\"access_token\":\"};
    if [[ "$access_token" == "$data" ]]; then
        return 0;
    fi
    access_token=${access_token%%\"*};
    echo $access_token;
    return 1; 
}
# /**
# * 获取access_token
# * @return {[type]} [description]
# */
function get_api(){
    url=$1;
    oneCurl  "$url"  > $tmpFile ;
    data=$(cat $tmpFile);
    echo $data;
}
# /**
# * 解析frps
# * @return {[type]} [description]
# */
function analysisFrps(){
    frp=$1;
    if [[ "$frp" == "" ]]; then
        return 0;
    fi
    idx=$(getParams "$frp" "id");
    doname=$(getParams "$frp" "doname");
    bind_port=$(getParams "$frp" "bind_port");
    token=$(getParams "$frp" "token");
    version=$(getParams "$frp" "version");
    if [[ "$idx" == "" ]]; then
        return 0;
    fi
    if [[ "$version" == "" ]]; then
        return 0;
    fi
    if [[ "$doname" == "" ]]; then
        return 0;
    fi
    if [[ "$bind_port" == "" ]]; then
        return 0;
    fi
    if [[ "$token" == "" ]]; then
        return 0;
    fi

    #fpath=$appPath/conf/server/frpc$idx.39doo.ver.$version.conf;
    fpath=$tmpConf/frpc$idx.39doo.ver.$version.svr;
    echo "[common]" > $fpath;
    echo "server_addr = $doname" >> $fpath;
    echo "server_port = $bind_port" >> $fpath;
    echo "privilege_token = $token" >> $fpath;
    echo "token = $token" >> $fpath;
    # echo $fpath;
    return 1;
}
function rmServerConf(){
    svrPath="$appPath/conf/server";
    rm -rf $svrPath/frp*.conf;
}
function moveTmp2conf(){
    svrs=`ls $tmpConf/*.svr`;
    svrPath="$appPath/conf/server";
    if [ ! -d "$svrPath" ];then
        mkdir $svrPath;
    fi
    clearOldConf=0;
    n=0;
    for svr in ${svrs[@]}; do
        if [[ "$clearOldConf" == "0" ]]; then
            rmServerConf;
        fi
        clearOldConf=1;
        fn=${svr%.*};
        fn=${fn##*/};
        mv $svr $svrPath/$fn.conf
        ((n++));
        # echo "----- $svr  $fn -- ";
    done
    echo "";
    echo "save to : $svrPath";
    echo "Successfully downloaded $n servers";
    return $n;
}
function getParams(){
    txt=$1;
    key=$2;
    val=${txt#*\\\"$key\\\":\\\"};
    if [[ "$val" == "$txt" ]]; then
        val=${txt#*\\\"$key\\\":};
    fi
    if [[ "$val" == "$txt" ]]; then
        echo "";
        return 0;
    fi
    val=${val%%,*};
    val=${val%%\\\"*};
    echo $val;

}


function main(){
    initServerConf;
    echo ""; echo "";
    echo "getn token....";
    token=$(get_token);
    if [[ $token != '' ]]; then
        echo "token is $token";
        echo ""; echo "";
        page=0;
        while [ $page -lt 20 ]
        do
            page=$[page+1];
            echo "get frps servers with page [ $page ] ....";
            # get_api "$apiUrl&access_token=$token&page=$page";
            data=$(get_api "$apiUrl&access_token=$token&page=$page");
            frps=$(match "$data" "[" "]");
            if [[ "$frps"x = ""x ]]; then
                error "req $apiUrl&access_token=$token";
                error "Err : Failed to get frps list!";
                cat $tmpFile;
                exit;
            fi

            i=1;
            while [ $i == 1 ]
            do
                frp=${frps#*\{};
                frps=${frp#*\}};
                if [[ "$frps" == "$frp" ]]; then
                    i=0;
                fi
                frp=${frp%%\}*};
                analysisFrps "$frp";
            done
            moveTmp2conf;
            # n=`moveTmp2conf`;
            if [[ $? -eq 0 ]]; then
                break;
            fi
        done  
    else
        error "Error getting token";
        error "$token";
        echo "$tokenURL?appid=$appid&secret=$secret"
    fi
}

