
# 修复连接
function fixAppLink(){
    if [[ ! -f "$binPath/frpc" ]]; then
        app=`find "$binPath/linux" -name "*frpc*" -type f | grep -v ".sh" | head -n 1`;
        if [[ $app != "" ]]; then
            ln -s "$app" "$binPath/frpc"
        fi
    fi
    if [[ ! -f "$binPath/frps" ]]; then
        app=`find "$binPath/linux" -name "*frpcs*" -type f | grep -v ".sh" | head -n 1`;
        if [[ $app != "" ]]; then
            ln -s "$app" "$binPath/frps"
        fi
    fi
}

function initFrpApp(){
    fixAppLink;
}

