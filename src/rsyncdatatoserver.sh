
svr="127.0.0.1";
port=48700;
dataNode=sdb1;

ret=`whereis getprop | grep /bin/`
if [[ $ret != "" ]]; then 
    brand=`getprop ro.product.brand`;
    model=`getprop ro.product.model`;
else
    brand=`cat /sys/class/dmi/id/sys_vendor`
    model=`cat /etc/hostname`;
fi
devname="$brand-$model";
if [[ ! -d $tmpConf ]]; then
    mkdir -p $tmpConf;
fi
pwdfile="$tmpConf/rsync.pwd";
# pwdfile="/data/data/com.termux/files/usr/tmp/rsync.pwd";
if [[ ! -f $pwdfile ]]; then 
    # echo "create passwrod file.";
    echo $rsyncpwd > $pwdfile;
    chmod 600 $pwdfile;
fi
storagelist(){
    dirs=('DCIM' 'Documents' 'Download' 'Downloads' 'Movies' 'Music' 'Pictures' 'Sounds' 'backup' 'Desktop' 'Videos');
    for d in "${dirs[@]}"; do
        if [[ -d $HOME/storage ]]; then 
            echo "$HOME/storage/shared/$d";
        else
            if [[ -d /storage/emulated/0 ]]; then 
                echo "/storage/emulated/0/$d";
            else
                echo "$HOME/$d";
            fi
        fi
    done
}

rsyncMainData(){
    echo -en "\rcheck passwrod file ...";
    if [[ ! -f $pwdfile ]]; then 
        return 1;
    fi
    echo -en "\rcheck server ...       ";
    ret=`echo -e '\x1dclose\x0d' | timeout --signal=9 1 telnet $svr $port  2>&1`;
    connected=`echo $ret | grep "Connected"`;
    if [[ $connected == "" ]]; then
         echo "";
         echo -e "\033[31mE : Please check if the service is available\033[0m"
         echo -e "\033[31mE : can't connecte to $svr:$port \033[0m"
         echo -e "\033[31mI : you can run frps-stream $port\033[0m"
         exit;
    fi
    echo "start rsync ...";
    for p in `storagelist`; do
        echo "from : $p ...";
        echo "to  : 127.0.0.1:$dataNode/$devname/";
        rsyncData "$p";
    done;
}
rsyncData(){
    p=$1;
    if [[ -d $p ]]; then 
        rsync -rltgoDvzP --port=$port --password-file=$pwdfile --exclude='.*' --exclude='.*/' $p rsyncusr@$svr::$dataNode/$devname/;
    else
            echo -e "\033[31mE : can't found $p\033[0m"
    fi    
}

main(){
    echo "Synchronize local data to the server"
    echo "-> $devname"
    echo "-> $svr:$port  $dataNode"
    rsyncMainData;
    p="/storage/emulated/0/Android/media/org.telegram.messenger/Telegram";
    rsyncData $p;
    rm -rf $p/*;

    rm -rf $pwdfile;
}

