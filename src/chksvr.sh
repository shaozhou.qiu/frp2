
if [[ ! -d $logPath ]]; then mkdir -p $logPath; fi
    
function configuration(){
    echo "[common]\n";
    echo "tls_enable = true\n";
    echo "server_addr = $1\n";
    echo "server_port = $2\n";
    echo "\n";

}

function moveToServer(){
    server_addr=$(getServerAddr $1);
    fn="$appPath/conf/server/$server_addr.conf";
    cp $1 $fn;
    echo 0;
    return 0;
}
function isFailed(){
    n=1;
    msgs=("doesn't match token" "login to server failed");
    # echo ${#msgs[@]};
    for (( i = 0; i < ${#msgs[@]}; i++ )); do
        msg=${msgs[$i]};
        ret=`echo $1 | grep "$msg"`;
        if [[ $ret != "" ]]; then
            error "$(getServerAddr $2):$(getConf $2 "server_port")    $msg";
            echo $n;
            return $n;
        fi
        n=$((n+1));
    done
    # login to server success
    log "$(getServerAddr $2):$(getConf $2 "server_port")    Ok";
    echo 0;
    return 0;
}

function testServer(){
    svr="$1";
    host="${svr%:*}"
    port=7000;
    if [[ "$host" != "$svr" ]]; then
        port=${svr#*:};
    fi
    conf=$(configuration "$host" "$port");
    f="$tmpConf/tmp.conf";
    rf="$tmpConf/runtime.tmp";
    echo -e $conf > $f;
    frpc -c $f > "$rf" &
    sleep 1;
    rlog=`cat $rf`;
    ret=$(isFailed "$rlog" "$f");
    if [[ $ret == "0" ]]; then
        # e=$(moveToServer $f);
        echo "$host:$port ---- ok"
    else
        echo "Failed!";
    fi
    pid=`ps -ef | grep "frpc -c $f" | grep -v "grep"|sed -n '1p'|awk '{print $2}'`;
    if [[ $pid != "" ]]; then
        kill $pid;
    fi
    rm $rf -rf;
}
function autoremove(){
    for f in $appPath/conf/server/*.conf; do
        svr=`echo $f | sed -r "s/.*\/(.*).conf/\1/g"`;
        port=`cat $f|grep "server_port" | awk '{print $3}'`
        echo -ne "\r$svr:$port ... ";
        ret=$(testServer $svr:$port);
        if [[ `echo $ret|grep Failed` == "" ]]; then
            continue;
        fi
        echo $ret;
        echo "remvoe config file $f"
        rm $f;
    done
}
function help(){
    echo "Nmap 7.94SVN ( https://nmap.org )";
    echo "Usage: frps-add [Options]";
    echo "EXAMPLES:";
    echo "  frps-add 127.0.0.1 ";
    echo "  frps-add 127.0.0.1:7000 ";
    echo "  frps-add /media/root/server.lst ";
}
function locat(){
    msg="$1";
    mt="$2";    
    if [[ $mt == "" ]]; then
        echo $msg;
    fi
}
function main(){
    svrs="$1";
    mt="$2";
    if [[ $svrs == "" ]]; then 
        autoremove;
        return ; 
    fi
    if [[ -f "$svrs" ]]; then
        while read rows
        do
            if [[ $rows == "" ]]; then
                continue;
            fi
            echo -ne "\r$rows ..."
            main $rows 1;
        done < $svrs
    else
        if [[ $svrs == "-h" ]]; then
            help;
            exit;
        fi
        locat "checking $svrs ..." $mt;
        ret=$(testServer $svrs);
        if [[ `echo $ret|grep Failed` == "" ]]; then
            f="$tmpConf/tmp.conf";
            if [[ $mt == "" ]]; then
                echo "";
            fi
            ret=$(moveToServer $f);
            if [[ $ret == "0" ]]; then
                echo "ok";
            fi
        fi
    fi
}
main $1
