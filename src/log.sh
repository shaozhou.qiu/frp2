
logPath="/var/log/frp";
logName="";
function logUtil(){
    if [[ $logName == "" ]]; then
        logf="$logPath/`date "+%Y%m%d"`.log";
    else
        logf="$logPath/$logName";
    fi
    d=`date "+%Y-%m-%d %H:%M:%S.%s"`;
    log="$d    $1    $2";
    echo $log >> $logf;
}
function log(){ logUtil "I" "$1"; }
function error(){ logUtil "E" "$1"; }