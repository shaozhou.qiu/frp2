
root_path=`pwd`

version=(0.60.0 0.48.0)
#https://github.com/fatedier/frp/releases/download/v0.29.0/frp_0.29.0_windows_amd64.zip
#https://github.com/fatedier/frp/releases/download/v0.29.0/frp_0.29.0_linux_386.tar.gz
#https://github.com/fatedier/frp/releases/download/v0.60.0/frp_0.60.0_linux_amd64.tar.gz
#https://github.com/fatedier/frp/releases/download/v0.29.0/frp_0.29.0_linux_amd64.tar.gz
function download()
{
    savepwd=$1;
    sys=$2;
    mkdir tmp
    if [ -f "$binPath/frpc" ];then
        rm $binPath/frpc -rf;
    fi
    if [ -f "$binPath/frps" ];then
        rm $binPath/frps -rf;
    fi
    for ver in ${version[@]} 
    do
        rm ./tmp/* -Rf
        fn="frp_"$ver"_"$sys
        if [ ! -f "./$savepwd/frpc.$ver" ]; then #如果文件不存在则下载
            if command -v aria2c &>/dev/null; then 
                aria2c -s 11 -c -x 11 -d ./tmp https://github.com/fatedier/frp/releases/download/v$ver/$fn.tar.gz
            else 
                wget -P ./tmp https://github.com/fatedier/frp/releases/download/v$ver/$fn.tar.gz
            fi
            if [[ ! -f "./tmp/$fn.tar.gz" ]]; then
                continue;
            fi
            tar xzvf ./tmp/$fn.tar.gz -C ./tmp
            mv ./tmp/$fn/frpc ./$savepwd/frpc.$ver
            mv ./tmp/$fn/frps ./$savepwd/frps.$ver
        fi
        if [ -f "$binPath/frpc.$ver" ];then
            rm $binPath/frpc.$ver -rf;
        fi
        if [ -f "$binPath/frps.$ver" ];then
            rm $binPath/frps.$ver -rf;
        fi
        if [ ! -f "$binPath/frpc.$ver" ]; then #如果连文件不存在
            ln -s $root_path/$savepwd/frpc.$ver $binPath/frpc.$ver
            ln -s $root_path/$savepwd/frps.$ver $binPath/frps.$ver
        fi
        if [ ! -f "$binPath/frpc" ];then
            ln -s $root_path/$savepwd/frpc.$ver $binPath/frpc
        fi
        if [ ! -f "$binPath/frps" ];then
            ln -s $root_path/$savepwd/frps.$ver $binPath/frps
        fi
    done
    rm ./tmp -Rf
}

function removeInvalidFiles(){
    frpbin=`ls $binPath/frp*`
    for f in ${frpbin[@]} 
    do
        if [ ! -f "$f" ]; then
            echo -e "\033[41;37m error : $f \033[0m";
            rm $f -rf;
        fi
    done
}
function downloadLinux(){
    removeInvalidFiles;
    echo "linux $1 download..."
    if [ ! -d "./bin" ]; then #如果文件不存在则下载
        mkdir bin
    fi
    if [ ! -d "./bin/linux" ]; then #如果文件不存在则下载
        mkdir bin/linux
    fi
    if [ ! -d "./bin/linux/$1" ]; then #如果文件不存在则下载
        mkdir bin/linux/$1
    fi
    download bin/linux/$1 linux_$1;
    #download bin/linux/x86 linux_386;
}
function downloadWindows(){
    echo "windows $1 download..."
    if [ ! -d "./bin" ]; then #如果文件不存在则下载
        mkdir bin
    fi
    if [ ! -d "./bin/windows" ]; then #如果文件不存在则下载
        mkdir bin/windows
    fi
    if [ ! -d "./bin/windows/$1" ]; then #如果文件不存在则下载
        mkdir bin/windows/$1
    fi
    # download bin/windows/$1 windows_$1;
    download bin/windows/$1 windows_amd64;
}
function autoDownload(){
    #if [[ $wordBit == "32" ]]; then
    #    downloadLinux 386; 
    #else 
        downloadLinux amd64; 
    #fi
}

if [ ! -n "$binPath"  ];then
    binPath=/usr/bin;
fi

case $1 in
    windows) downloadWindows amd64;;
    # windows) downloadWindows 386;;
    linux) downloadLinux amd64;;
    #linux) downloadLinux 386;;
    *)    autoDownload;;
esac


