
function telnetfrps(){
    port=$1;
    f=$2;
    par=$3;
    if [[ $port == "" ]]; then
        echo "Err:No port specified!";
        return ;
    fi
    
    echo "" > $f;
    confs=`ls $usrFrpcConfPath/*.conf`;
    n=0;
    ts=100;
    for (( i=2; i<5; i++ ));do
        sec=$(millisecond2second $ts);
        for conf in ${confs[@]}; do
            ret=`ps -ef| grep "$conf"| grep -v "grep"`
            if [[ $ret == "" ]]; then
                continue;
            fi
            svraddr=$(getServerAddr "$conf");
            echo -ne "\r$svraddr:$port ...       ";
            ret=`cat $f | grep $svraddr:$port`;
            if [[ $ret != "" ]]; then
                continue;
            fi
            if addServer "$svraddr" "$port" "$f" "$par" "$sec"; then
                echo " ok";
                n=$(( n + 1 ));
            fi            
        done
        if [[ $n > 4 ]]; then
            break;
        fi
        #ts=$(( ts + 500 ));
        ts=$(( ts * 5 ));
        echo -ne "\rtry again [$i] with $ts ...              ";
        echo "";
    done
    if [[ $n < 1 ]]; then
        rm -f $f;
    fi
    echo "";
}
function addServer(){
    svraddr=$1;
    port=$2;
    f=$3;
    par=$4;
    sec=$5;
    if [[ $sec == "" ]]; then 
        sec=1; 
    fi
    ret=`echo -e '\x1dclose\x0d' | timeout --signal=9 $sec telnet $svraddr $port  2>&1`;
    connected=`echo $ret | grep "Connected"`;
    if [[ $connected != "" ]]; then
        echo "server $svraddr:$port $par;" >> $f;
        return 0;
    fi
    return 1;
}
function sortbyspeed(){
    f=$1;
    tf="$tmpConf/ping.log";
    echo "" > $tf;
    while read l; do
        if [[ $l == "" ]]; then
            continue;
        fi
        ip=`echo $l | sed -r "s/.* ([0-9a-z\.]+)+.*/\1/g"`;
        ret=`timeout --signal=9 1 ping  -c 1 $ip|grep from| sed -r 's/.*=([0-9]+)(\.[0-9]+)?\s*ms/\1/g'`;
        if [[ $ret == "" ]]; then ret="999"; fi
        ret="0000$ret";
        echo "${ret:0-4:4}  $l"  >> $tf;
    done < $f
    sort $tf > "$tf.tmp";
    mv "$tf.tmp" $tf;
    cat $tf|sed -r "s/^[0-9]+\s*(.*)/\1/g" > $f;
    rm -f $tf;
}
function createStream(){
    port=$1;
    fser=$2;
    f=$3;
    echo "upstream server$port{" > $f;
    cat $fser  >> $f;
    echo "}" >> $f;
    echo "# upstream server$port{" >> $f;
    echo "#     # 30秒内请求失败3次，则认为该应用为宕机。 等待30后再重新尝试连接" >> $f;
    echo "#     server 172.16.116.100:3389 max_fails=3 fail_timeout=30s;" >> $f;
    echo "#     # 健康检查（需要安装 nginx-upstream-check-module）" >> $f;
    echo "#     # check interval=3000 rise=2 fall=5 timeout=1000 type=tcp;" >> $f;
    echo "#     health_check interval=3000 rise=2 fall=5 timeout=1000;" >> $f;
    echo "# }" >> $f;
    echo "server {" >> $f;
    echo "    listen $port;" >> $f;
    echo "    listen [::]:$port ipv6only=on;" >> $f;
    echo "    proxy_pass server$port;" >> $f;
    echo "    proxy_connect_timeout 2s;" >> $f;
    echo "    proxy_timeout 300s;" >> $f;
    echo "    proxy_next_upstream on;" >> $f;
    echo "    # 故障转移重试次数" >> $f;
    echo "    proxy_next_upstream_tries 3;" >> $f;
    echo "    # 故障转移超时时间" >> $f;
    echo "    proxy_next_upstream_timeout 5s;" >> $f;
    echo "    # 启用持久连接，减少每次请求的握手开销" >> $f;
    echo "    proxy_socket_keepalive on;" >> $f;
    echo "    # access_log /var/log/nginx/stream_access.log;" >> $f;
    echo "    # error_log /var/log/nginx/stream_error.log;" >> $f;
    echo "}" >> $f;
}


function addStreamPort(){
    port=$1;
    fser="$tmpConf/server-$port.conf";
    fstr="$tmpConf/frps-stream-$port.conf";
    addServer "10.0.0.8" "$port" "$fser" "max_fails=3 fail_timeout=30s";
    telnetfrps "$port" "$fser" "max_fails=3 fail_timeout=30s";
    if [[ -f "$fser" ]]; then
        echo "Testing network speed...";
        sortbyspeed "$fser";
        createStream "$1" "$fser" "$fstr";
        rm -f $fser;
        if [[ -d "$etcPath/nginx/stream.d" ]]; then
            mv $fstr "$etcPath/nginx/stream.d/";
            #rm -f $fstr;
            echo "create proxy stream to $etcPath/nginx/stream.d/";
        fi
        echo "The configuration file is added successfully. ";
        echo "Please restart the nginx server.";
        echo "eg: systemctl restart nginx.service or nginx -s reload";
    else
        echo "Err : can't found frps servers!";
    fi
}
function update(){
    ports="";
    for f in `ls $etcPath/nginx/stream.d/frps-stream-*.conf`
    do
        p=`echo $f | sed -r 's/.*\-([0-9]+)\.conf/\1/g'`;
        ports="$ports $p";
    done
    frps-stream $ports;
}

function main(){
    if [[ "$1" == "update" ]]; then
        update;
        return 0;
    fi
    chkTmpPath;
    if command -v nginx >/dev/null 2>&1; then
        for arg in $*; do
            echo "Adding port[$arg] to nginx stream ..."
            addStreamPort "$arg";
        done
        echo ""
        echo -ne "\rAttempting to restart nginx service ...."
        nginx -s reload;
        echo "ok"
    else
        echo "Err: nginx command not found!";
    fi
}
