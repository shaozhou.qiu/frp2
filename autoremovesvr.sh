#! /bin/bash

source `pwd`/conf/global.conf
source $appPath/src/log.sh
source $appPath/src/configuration.sh

path="$appPath/conf/server"

function isFailed(){
    if [[ "$1" == "" ]]; then
        error "$(getServerAddr $2):$(getConf $2 "server_port")    i/o timeout.";
        echo -1;
        return -1;
    fi    
    n=1;
    msgs=("doesn't match token" "login to server failed");
    # echo ${#msgs[@]};
    for (( i = 0; i < ${#msgs[@]}; i++ )); do
        msg=${msgs[$i]};
        ret=`echo $1 | grep "$msg"`;
        if [[ $ret != "" ]]; then
            error "$2  $msg";
            echo $n;
            return $n;
        fi
        n=$((n+1));
    done
    # login to server success
    log "$2   Ok";
    echo 0;
    return 0;
}

function main(){
    n=0;
    for f in `ls $path/`; do
        cf="$path/$f";
        echo -ne "\r Testing : $cf ...            ";
        frpc -c $cf >nohup.out &
        sleep 1;
        pid=`ps -ef|grep "frpc -c $cf" | grep -v "grep" |sed -n '1p'|awk '{print $2}'`
        
        txt=`cat nohup.out`;
        ret=$(isFailed "$txt" "$cf");
        if [[ $ret != "0" ]]; then
            rm $cf -rf;
            n=$((n+1));
            rcf="$usrFrpcConfPath/${cf##*/}";
            if [[ -f $rcf ]]; then
                rm $rcf -rf;
            fi
        fi
        rm nohup.out -rf
        if [[ $pid != "" ]]; then
            kill $pid;
        fi
    done
    echo -e "\n[ $n ] invalid configuration files were removed!";
    echo "finish!";
}

main