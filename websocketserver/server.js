/*

node server.js --port=3000

 */

const WebSocket = require('ws');
const Minimist = require('minimist');
const readLine = require('readline');
const args = Minimist(process.argv.slice(2));

const rl = readLine.createInterface({
    input: process.stdin
    , output: process.stdout
//    , prompt: '>'
});
let port = 8080;
if(args.port) port = args.port;
const wss = new WebSocket.Server({port:port});

// 存储客户端
const clients = new Set();
wss.on('connection', (ws, req)=>{
    const ip = req.socket.remoteAddress;
    const port = req.socket.remotePort;
    const addr = ip+':'+port;

    console.log(addr + ' connected');
    clients.add(ws);

    ws.on('close', ()=>{
        console.log(addr + ' lost!');
        clients.delete(ws);
    });
    ws.on('message', (msg) => {
        console.log("======= "+addr+" =======");
        console.log("time\t : " + (new Date).getTime() + '\n');
        try {
            // 解析消息为JSON对象
            let e = JSON.parse(msg);
            if (e.type === 'error') console.error(e.content);
            else if (e.type === 'warn') console.warn(e.content);
            else if (e.type === 'log') console.log(e.content);
            console.log(e.error);
        } catch (e) {
            console.error(`Failed to parse message: ${msg}`);
        }
    });    
});

console.log('WebSocket listening '+port+'...');
rl.prompt();
rl.on('line', (data)=>{
    for (let client of clients) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(data);
        }
    }
});
