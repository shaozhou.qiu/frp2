<?php
include_once "src/Firewall.php";
$f=$_GET["f"]??null;
if($f != null){
    $f = preg_replace("/.*\/datastorage\/(.+)/im", "/$1", $f);
}
if($f == null){
    header("HTTP/1.1 404 Not Found!");
    die;
}
if(!is_readable($f)){
    header("HTTP/1.1 403 Access denial!  $f");
    die;
}

$finf=pathinfo($f);
$_SERVER["DOCUMENT_ROOT"] = $finf["dirname"];
$_SERVER["DOCUMENT_URI"] = "/".$finf["basename"];
$_SERVER["SCRIPT_NAME"] = "/".$finf["basename"];
$_SERVER["PHP_SELF"] = "/".$finf["basename"];
$_SERVER["REQUEST_URI"] = $finf["basename"];
$_SERVER["SCRIPT_FILENAME"] = $f;
$_SERVER["PATH_TRANSLATED"] = $finf["dirname"]."/";

// 将url转换为frp专属url
function toFrpUrl($url){
    if(!$url) return $url;
    if(!preg_match("/(.*:\/\/[^\/]+\/)(.+)/im", $url, $m))return $url;
    $nurl = $m[1]."datastorage".$_SERVER['DOCUMENT_ROOT']."/".$m[2];
    return $nurl;
}

include($f);
