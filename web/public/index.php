<?php
ini_set("display_errors", 0);
include_once "src/Firewall.php";
include_once "src/FileUtil.php";
include_once "src/Thumbnail.php";
include_once "src/Cache.php";
include_once "src/share.php";
include_once "src/Compress.php";
$share = new EtSoftware\Share();
$ret = $share->author();
if($ret['errcode']!=0)
    include_once "src/Author.php";

include_once "src/Image.php";
include_once "src/Log.php";


$tPath = $_SERVER["DOCUMENT_ROOT"]."/tmp";
if(!is_dir($tPath))mkdir($tPath);

function showServer(){
    echo "<table><tbody>";
    foreach ($_SERVER as $k => $v){
      echo "<tr>";
       echo "    <th>$k</th>";
       echo "    <td>$v</td>";
       echo "</tr>";
    }
    echo "</tbody></table>";
}


function equalProportions($x1, $y1, $x2, $y2){
    if($x2==0) return ($x1*$y2)/$y1;
    if($y2==0) return ($y1*$x2)/$x1;
}

function download($f){
    ini_set('memory_limit', '512M');
    if($f == null || !file_exists($f))return null;
    $fs = filesize($f);
    $f = urldecode($f);
    if(!$f || !is_file($f)) return null;
    $fu = new EtSoftware\FileUtil();
    $content = $fu->readBFile($f);
    $ctype = $fu->contentType($f);
    // Parse Info / Get Extension    
    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false); // required for certain browsers
    header("Content-Type: $ctype");
    header("Content-Disposition: attachment; filename=\"".basename($f)."\";" );
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".$fs);
    ob_clean();
    flush();
    echo $content;
    die;
}


function usrinfo(){
    echo(json_encode($_SERVER, true));
}

function downloadbin($f){
    if(!file_exists($f)){
        header("HTTP/1.1 404 Not Found $f");
        die;
    }
    set_time_limit(0);
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($f) . '"');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($f));

    readfile($f);
}
function downloadFile($f){
    $f = urldecode($f);
    // echo $f; die;
    downloadbin($f);
    die;
}
function filePermission($p){
   $reVal = "";
    
    // 检查是否是目录
    $reVal .= is_dir($p) ? "d" : "-";
    
    // 检查读权限
    if (is_readable($p)) {
        $reVal .= "r";
    } else {
        $reVal .= "-";
    }
    
    // 检查写权限
    if (is_writable($p)) {
        $reVal .= "w";
    } else {
        $reVal .= "-";
    }
    
    // 对于文件，可以进一步检查执行权限（如果是Unix-like系统）
    //if (!is_dir($p)) {
        if (is_executable($p)) {
            $reVal .= "x";
        } else {
            $reVal .= "-";
        }
    //}
    
    // 如果需要，可以尝试以写模式打开文件来验证写权限
    if (is_file($p)) {
        $f = @fopen($p, "w+");
        if ($f !== false) {
            fclose($f);
        } else {
            // 如果 fopen 失败，说明没有写权限
            if (strpos($reVal, 'w') !== false) {
                // 如果之前认为有写权限，但实际无法打开文件，则修正
                $reVal = substr_replace($reVal, '-', strpos($reVal, 'w'), 1);
            }
        }
    }
    
    return $reVal;
}
function actzip($p){
    $pwd = $_SERVER['PHP_AUTH_PW']??null;
    $idx = intval($_GET["idx"]??-1);
    if(!$p)return array("errcode"=>10001, "data"=>"Err:unable to find the file!");
    $p = urldecode($p);
    if(!file_exists($p))return array("errcode"=>10001, "data"=>"Err:unable to find the file!");
    $finfo = pathinfo($p);
    if(!is_writable($GLOBALS["tPath"])){
        return array("errcode"=>10002, "data"=>"Err : Permission denied ! \n".$GLOBALS["tPath"]);
    }
    $fs = scandirs($p);
    $cnt = count($fs);
    if(!($idx < $cnt))
        return array("errcode"=>10003, "data"=>"Err : Out of range!");
    $f = $fs[$idx]; 
    $sp = $GLOBALS["tPath"]."/".$finfo["filename"];
    $rpath = str_replace($p, "", $f);
    // if(preg_match("/^(\/[^\/]+)\/.+/im", $rpath, $m)){
    $toPath = null; 
    if(preg_match("/^(.+)\/.+/im", $rpath, $m)){
        $toPath = $m[1]; 
    }
    $compress = new EtSoftware\Compress;
    $ret = $compress->compress($f, "$sp.7z", $toPath, $pwd);
    $reVal = array_merge(array(
        "idx" => $idx
        ,"count" => $cnt
    ), $ret);
    if($reVal["errcode"] !=0 ){
        $reVal["permission"]=array(
            filePermission($GLOBALS["tPath"]).":".$GLOBALS["tPath"]
            , filePermission($f).":".$f
        );
    }

    return $reVal;

}
function scandirs($d){
    $data=[];
    $files = scandir($d);
    foreach($files as $v){
        if($v == "." || $v == "..")continue;
        $f = "$d/$v";
        if(is_dir("$f")){
            $data=array_merge($data, scandirs("$f"));
            continue;
        }
        array_push($data, $f);
    }
    return $data;
}
function thumbnailwh($f){
    $w = $_GET["w"]??0;
    $h = $_GET["h"]??0;
    if($f == null)return null;
    $f = rawurldecode($f);
    if(!$f || !is_file($f)) {
        if(preg_match("/^\/datastorage(.+)/im", $f, $m)){
            $f = $m[1];
            if(!$f || !is_file($f)) {
                return null;
            }
        }else{
            if(is_dir($f)){
                //$f = $_SERVER["DOCUMENT_ROOT"]."/img/dir.png";
                $f = $_SERVER["DOCUMENT_ROOT"]."/img/dir.jpg";
            }else{
                return null;
            }
        }
    }

    $img = new EtSoftware\Thumbnail($f);
    $img->resize($w, $h);
    $fn = $img->getFile();
    $img->response();
    $img->destroy();
    if($fn){        
        $source = imagecreatefromjpeg($fn);
        header('Content-Type: image/jpeg');
        imagejpeg($source);
        imagedestroy($source);
    }else{
        echo "Err : Failed to create thumbnail!";
        echo "<br>Possible causes:";
        echo "<br>1. Lack of space";
        echo "<br>2. Insufficient permissions";
    }
    // 释放内存
    die;
    
}


function renamefile($f, $fn){
    if(!$f || !$fn) {
        return array('errcode'=>20001, 'data'=>"Failed: Missing required parameters.");
    }
    if (strpos($fn, '/') !== false || strpos($fn, '\\') !== false) {
        return array('errcode'=>20002, 'data'=>"Failed: Illegal file name detected.");
    }    
    if(preg_match("/(.*)\/[^\/]+$/", $f, $m)){
        $nf = "$m[1]/$fn";
        if ($nf === null) {
            return array('errcode'=>20003, 'data'=>"Failed: Path replacement failed.");
        }
        if(!file_exists($f)){
            return array('errcode'=>20004, 'data'=>"Failed: file is not exists.");            
        }
        rename($f, $nf);
        return array('errcode'=>0, 'data'=>'ok');
    }
    return array('errcode'=>20005, 'data'=>'error');
}

function lockPath($p){
    $share = new EtSoftware\Share();
    $ttl = $_GET['ttl']??3600;
    $pwd = $_GET['pwd']??null;
    return $share->encode($p, $ttl, $pwd);
}
$act = $_GET['act']??null;
$f = $_GET['f']??null;
$p = $_GET['p']??null;
$f = $share->decode($f);

$log = new EtSoftware\Log();
$log->log("action:$act, file:$f, path:$p");
if($act == "getfiles"){
    $f = new EtSoftware\FileUtil();
    $data = $f->getfiles($p);
    echo json_encode($data, true);
    die;
}else if($act == "download"){
    download($f);
    die;
}else if($act == "readtext"){
    (new EtSoftware\FileUtil())->readtext($f);
    die;
}else if($act == "savefile"){
    (new EtSoftware\FileUtil())->savefile($f);
    die;
} else if($act == "delf"){
    (new EtSoftware\FileUtil())->unlinkfile($f);
    die;
} else if($act == "renf"){
    $fn = $_GET['fn']??null;
    $data = renamefile($f, $fn);
} else if($act == "showpic"){
    thumbnailwh($f);
    die;
} else if($act == "usrinfo"){
    usrinfo();
    die;
} else if($act == "dwnf"){
    downloadFile($f);
    die;
} else if($act == "touch"){
    (new EtSoftware\FileUtil())->touchFile($f);
    die;
} else if($act == "zip"){
   $data = actzip($f);
} else if($act == "share" || $act == "lckp"){
    $data = lockPath($p);
}
echo json_encode($data, true);
die;
