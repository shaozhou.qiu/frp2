<?php
class FileUtil{
    public function __construct(){}
    static public function lockEx($fid, $cb){
        $fn = "$fid";
        $fp = fopen($fn, "w+");
        $ret = null;
        if(flock($fp, LOCK_EX)){
          $ret = $cb();
          flock($fp, LOCK_UN);
          fclose($fp);
          if(file_exists($fn))
            unlink($fn);
        }
        return $ret;
    }
    static public function mkdirs($p){
        if(preg_match_all("/\/[^\/]+/im", $p, $mc, PREG_SET_ORDER)){
            $cp="";
            foreach($mc as $m){
                $cp .= $m[0];
                if(is_dir($cp))continue;
                if(!mkdir($cp)) return false;
            }
        }
        return true;
    }
    // 删除目录下所有文件及子目录
    static public function rmdirs($dir) {
        if (!file_exists($dir))return true;
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir), RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            if ($file->isDir()) {
                self::rmdir($file->getPathname());
            } else {
                unlink($file->getPathname());
            }
        }
        return rmdir($dir);
    }
    static public function base64FileToFile($file){
        if(!file_exists($file))return null;
        $fd = fopen($file, "rb");
        if(!$fd)return null;
        $data = fread($fd, 30);
        fclose($fd);
        if(preg_match("/^\s*data\s*:\s*.*?\/(.+)\s*;\s*base64\s*,.*/im", $data, $m)){
            $fn = "$file.$m[1]";
            $fd = fopen($file, "rb");
            $dataUrl = fread($fd, filesize($file));
            fclose($fd);
            // 分离Base64数据部分
            list(, $data) = explode(',', $dataUrl);
            // 解码Base64字符串
            $imageData = base64_decode($data);
            // 尝试将图像数据写入文件
            if (file_put_contents($fn, $imageData)) {
                // echo "图像保存成功，文件名为 {$fn}";
                return $fn;
            } else {
                // echo "保存图像失败，请检查目录权限或其他错误";
            }

        }
        return null;
    }
}


class App{
    private $basePath;
    private $blob;
    private $count;
    private $f;
    private $act;
    public function __construct(){
        $this->getArguments();
        $this->initialize();
    }
    public function start(){
        $ret = array('errcode'=>0, 'data'=>'ok');
        // dataUrl 方式上传
        if($this->act == "updu"){
            $ret = $this->uploadByDataUrl();
        }else if($this->act == "mkdir"){
            $ret = array("errcode" => 10003 ,"data" => "Err : Unable to create a catalog!");
            if(!is_dir($this->basePath) )
                FileUtil::mkdirs($this->basePath);
            if( is_dir($this->basePath) ){
                $ret = array("errcode" => 0 ,"data" => "ok");                
            }
        }else if($this->act == "rmdir"){
            $ret = $this->cleardir($this->basePath);
        }else{
            $ret = $this->uploadByBlobs();
        }
        return $ret;
    }
    private function rename($fn){
        while(file_exists($fn)){
            if(preg_match("/(.+?)(\((\d+)\))?(\.\w+)$/im", $fn, $m)){
                if($m[3]=="")$m[3]=0;
                $fn="$m[1](".($m[3]*1+1).")$m[4]";
            }
        }
        return $fn;
    }
    private function uploadByDataUrl(){
        $ret = $this->checkPath();
        if($ret['errcode'] != 0) return $ret;
        $data = file_get_contents('php://input'); 
        if(!$data) $data = $_POST['data']??null;
        if(!$data) $data = $_GET['data']??null;
        $fnpre = "dataUrl-blob";
        $fn = "00000000$this->blob";
        $fn = substr($fn, -8, strlen($fn));
        $bfn = "$this->basePath/$fnpre-$fn";
        $fnid = sha1($fn);
        $fd = fopen($bfn, "wb+");
        if($fd){
            fwrite($fd, $data);
            fclose($fd);
            $fs = glob("$this->basePath/$fnpre*", GLOB_BRACE);
            if(count($fs) == $this->count){
                sort($fs);
                $ufn = "$this->basePath/".time()."-".rand(1000, 9999);                
                if(file_exists($ufn))unlink($ufn);
                foreach($fs as $k=>$v){
                    if(!file_exists($v))continue;
                    file_put_contents($ufn, file_get_contents($v), FILE_APPEND);
                    unlink($v);
                    // echo "$v\n";
                }
                $file = FileUtil::base64FileToFile($ufn);
                if($file)unlink($ufn);
                $pi = pathinfo($file);
                $ret = array(
                    "errcode" => 0
                    ,"data" => "ok"
                    ,"file" => $file
                    ,"filename" => $pi['filename']
                    ,"extension" => $pi['extension']
                    ,"basename" => $pi['basename']
                    ,"dirname" => $pi['dirname']
                );
            }else{
                $ret = array(
                    "errcode" => 1
                    ,"data" => "upload completion."
                    ,"uploaded_blob" => count($fs)
                    ,"count" => $this->count
                    ,"fnid" => $fnid
                );;
            }
        }else{
            $ret = array("errcode" => 10005 ,"data" => "Err : can't save blob data.");

        }
        return $ret;
    }
    private function uploadByBlobs(){
        $ret = $this->checkPath();
        if($ret['errcode'] != 0) return $ret;
        if(!$this->f)
            return array("errcode" => 10001 ,"data" => "Err :  \$_FILES is empty.");

        $fn = $this->f["name"];
        $ufn = "$this->basePath/$fn";
        $fnid = sha1($fn);

        $tmpPath = preg_replace("/(.*)\/[^\/]*/im", "$1", $this->f["tmp_name"]);
        if(!is_dir($tmpPath)) $tmpPath="/tmp";
        $tmpfn = "$tmpPath/$fnid";
        $tmpfnb = "$tmpfn".substr("00000$this->blob", -6);
        if(!file_exists($tmpfnb)){
            move_uploaded_file($this->f["tmp_name"], $tmpfnb);
        }
        $fs = glob("$tmpfn*", GLOB_BRACE);
        if(count($fs) == $this->count){
            sort($fs);
            $ufn = $this->rename($ufn);
            if(file_exists($ufn))unlink($ufn);
            FileUtil::lockEx("$tmpPath/$fnid", function()use($fs, $ufn){
                foreach($fs as $k=>$v){
                    if(!file_exists($v))continue;
                    file_put_contents($ufn, file_get_contents($v), FILE_APPEND);
                    unlink($v);
                }
            }); 
            $pi = pathinfo($ufn);
            return array(
                "errcode" => 0
                ,"data" => "ok"
                ,"file" => $ufn
                ,"filename" => $pi['filename']
                ,"extension" => $pi['extension']
                ,"basename" => $pi['basename']
                ,"dirname" => $pi['dirname']
                ,"url" => str_replace($_SERVER['DOCUMENT_ROOT'], "", $ufn)
            );
        }
        return array(
            "errcode" => 1
            ,"data" => "upload completion."
            ,"uploaded_blob" => count($fs)
            ,"count" => $this->count
            ,"fnid" => $fnid
        );

    }
    private function checkPath(){
        if(!is_writable($this->basePath)){
            return array(
            "errcode" => 10002
            ,"data" => "Err : path is can't writable!"
            ,"path" => $this->basePath
            );
        }        
        return array("errcode" => 0 ,"data" => "ok" );
    }
    private function cleardir($dir){
        if(file_exists($dir)){
            FileUtil::rmdirs($dir);
            return array("errcode" => 0 ,"data" => "ok" );
        }else{
            return array("errcode" => 10004 ,"data" => "Err : does not exist");
        }
    }
    private function getArguments(){
        $this->basePath = urldecode($_GET["p"]??null);
        $this->blob = $_POST["idx"]??$_GET["idx"]??0;
        $this->count = ($_POST["count"]??$_GET["count"]??0)*1;
        $this->f = $_FILES["file"]??null;
        $this->act = $_GET["act"]??null;        
    }
    private function initialize(){
        if($this->basePath==null || $this->basePath==""){
            $this->basePath = $_SERVER["DOCUMENT_ROOT"]."/uploadfiles/".date("y/m/d");
        }
        if(!is_dir($this->basePath))FileUtil::mkdirs($this->basePath);
    }
}

$ret = (new App())->start();
echo json_encode($ret);
die;