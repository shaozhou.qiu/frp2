(function(global, factory){
    factory(global);
})(window, function(win){
    const quote = (str, delimiter) => {
        if(!str || !delimiter) return str;
        let re = new RegExp("("+delimiter+")", "gi");
        return str.replace(re, "\\$1");
    }
    const arraySerialization = (o) => {
        let s = '';
        for(let i=0; i<o.length; i++){
            let v = o[i];
            s += ',';
            if(typeof(v) == 'string')
                s += '"' + quote(v, '"').replace(/\n/gi, '\\n') + '"';
            else if(v instanceof Array)
                s +=  arraySerialization(v);
            else if(typeof(v) == 'object')
                s +=  jsonSerialization(v);
            else
                s +=  v;
        }
        return "["+ s.substr(1) +"]"
    }
    const jsonSerialization = (o) => {
        let s = '';
        for(let k in o){
            let v = o[k];
            s += ', "'+k+'":';
            if(typeof(v) == 'string')
                s += '"' + quote(v, '"').replace(/\n/gi, '\\n') + '"';
            else if(v instanceof Array)
                s +=  arraySerialization(v);
            else if(typeof(v) == 'object')
                s +=  jsonSerialization(v);
            else
                s +=  v;
        }
        return "{"+ s.substr(1) +"}"
    }
    const objectSerialization = (o) => {
        if(o instanceof Array)
            return arraySerialization(o);
        else if(typeof(o) == 'object')
            return jsonSerialization(o);
        else
            return o;
    }
    const objectDeserialization = (o) => {
        if(!o) return o;
        if(typeof(o) != 'string') return o;
        o = o.replace(/\n/gi, '\\n');
        if(! (/^\s*(\[|\{)[\w\W]+(\]|\})\s*$/gi.test(o)) )return o;
        return new Function("return "+o+";")();
    }
    const objectCopy = (o) => {
        return objectDeserialization(objectSerialization(o));
    }
    if(Object.serialization == undefined)
        Object.serialization = objectSerialization;
    if(Object.deserialization == undefined)
        Object.deserialization = objectDeserialization;
    if(Object.copy == undefined)
        Object.copy = objectCopy;
});