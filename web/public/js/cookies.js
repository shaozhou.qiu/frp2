(function(global, factory){
  factory(global);
})(window, function(win){
    function cookies(txt){
        this.pathName = txt;
    }
    cookies.prototype.get=function(k){
        var t = null;var cn = this.encode(k);
        var re = new RegExp("(;\\s*)?("+cn+"=[^;]+)(\\s*;)?", "gi")
        var m = document.cookie.match(re);
        if(m){
           re = new RegExp(".*"+cn+"=([^;]+).*", "gi")
           t = m[0].replace(re, "$1");
        }
        return t;
    }
    cookies.prototype.set=function(k, v){
        document.cookie=this.encode(k)+'='+v;
    }
    cookies.prototype.encode=function(txt){
        var reVal = 0;
        var entory='39doo', n=0;
        for(var i=0; i<this.pathName.length;i++){
            var d=this.pathName.substr(i,1).charCodeAt();
            var c=entory.substr(n,1).charCodeAt();
            reVal+=d+c;
            n++;
            if(!(n<entory.length))n=0;
        }
        return txt+reVal+''+this.pathName.length;
    }
    win.cookies=new cookies(location.pathname);
})