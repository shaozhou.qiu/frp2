(function(global, factory){
  factory(global);
})(window, function(win){
    function menu(div){
        if(div.menu)return;
        var el = ce("div", {"class":"menu"});
        el.ce('a', {'href':'javascript:void(0);', 'class':'btn close'}).bind('click', function(){
            var ha = div.getElementsByTagName('a')[0];
            var fn = ha.href.replace(/.*datastorage(.+)/gi, "$1");
            el.remove();
            ajax({
                url:'/index.php?act=delf&f='+encodeURIComponent(fn)
                , dataType:'json'
                , success: function(e){
                    if(e.errcode == 0){
                        div.remove();
                    }else{
                        alert(e.data+":"+(e.message?e.message:''));
                    }
                }
            });
            div.menu = null ;
        }).innerHTML='X';
        div.menu = el;
        return el;
    }
    function ItemEvent(el){
        this.el = el;
        var _me = this;
        this.LClick(function(){
            ce(this).append(menu(_me.el));
        });
        if(cookies.get('operater') == '1')
            ce(this.el).append(menu(_me.el));
    }
    ItemEvent.prototype.LClick=function(fun){
        function down(){
            if(event.buttons !=1)return;
            var _me = this;
            if(_me.eventLC)return;
            _me.eventLC=true;;
            var tid = setTimeout(function(){
                fun.call(_me); _me.eventLC=false;
            }, 1000);
            ce(this).bind('mouseup, touchend, mouseout', function(){
                clearTimeout(tid);
                _me.eventLC=false;;
            });
        }
        ce(this.el).bind('mousedown, touchstart', down);
    }
    function FileOperate(el){
        this.el = el;
        for(var i=0; i<el.childNodes.length; i++){
            if(/^\.\.?$/gi.test(el.childNodes[i].getElementsByTagName("h4")[0].innerText))
                continue;
            new ItemEvent(el.childNodes[i]);
        }
    }
    win.FileOperate = FileOperate;
    //    setTimeout(function(){
    //    new FileOperate(document.all('hdivlst'));
    //}, 5000);
})