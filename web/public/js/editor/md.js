(function(editor, el, factory){
	factory(editor, el);
})(editor, el, function(o, el){
	var v = el.innerHTML.replace(/(<[^>]+?>)/gi, '');
	v = v.replace(/(&[\w\d]+;)/gi, ' ');
	var cn = null;
	for(var i=6; i>0; i--){
		var l='#######';
		var re = new RegExp('^'+l.substr(0, i)+'[ ]+.+', 'gi');
		if(re.test(v)){
			cn = 'h'+i;
			break;
		}
	}
	if(!cn){
		var m = /^```(\w+)[\w\W]*```$/gi.exec(v);
		if(m){cn = 'code'; }
	}
	if(!cn){
		var m = /^((\*\*.+?\*\*)|(\_\_.+?\_\_))$/gi.exec(v);
		if(m){cn = 'strong'; }
	}
	if(!cn){
		var m = /^(\*[^\*]+?\*)$/gi.exec(v);
		if(m){cn = 'italic'; }
	}
	if(!cn){
		var m = /^(\~\~[\w\W]+\~\~)$/gi.exec(v);
		if(m){cn = 'delline'; }
	}
	if(cn)
		el.classList.add('editor_md_'+cn);
	console.log(el)
});