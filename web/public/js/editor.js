(function(global, factory){
    factory(global);
})(window, function(win){
    var libs=[];
    function text2html(v){
        v = v.replace(/</gi, '&lt;');
        v = v.replace(/>/gi, '&gt;');
        v = v.replace(/\t/gi, '    ');
        v = v.replace(/ /gi, '&nbsp;');
        v = v.replace(/\n/gi, '<br />');
        return v;
    }
    function createline(){
        var el = ce("div", {"style":"display:inline;float:left;overflow:hidden;"});
        var ul = el.ce('ol', {style:";color:#756a54;margin: 0rem;"});
        return el;
    }
    function autoAdjLine(el){
        var obsLine = new MutationObserver(function(mutationslist, observer){
            mutationslist.forEach(function(mutation) {
                if (mutation.type === 'childList') {
                    // 如果有子元素被添加或删除
                    mutation.addedNodes.forEach(function(node) {
                        var ol = mutation.target.elline.getElementsByTagName('ol')[0];
                        var li = ce('li');
                        if(!node.parentNode)return;
                        const chs = Array.from(node.parentNode.children);
                        const idx = chs.indexOf(node);
                        ol.insertBefore(li, ol.children[idx]);
                        node.lli=li;
                        li.style.height = node.offsetHeight+'px';
                        li.style.lineHeight = li.style.height;
                        node.resizeLine=function(){
                            this.lli.style.height = this.offsetHeight + 'px';
                            this.lli.style.lineHeight = this.lli.style.height;
                        };
                        node.resizeLine();
                        window.addEventListener('resize', function(){
                            node.resizeLine();
                        }, false);
                        (new MutationObserver(function(mls){
                            mls.forEach(function(ml) {
                                if(node.resizeLine)
                                    node.resizeLine();
                            });
                        })).observe(node, {characterData:true, subtree:true});
                    });
                    mutation.removedNodes.forEach(function(node) {
                        if(node.lli)node.lli.remove();
                    });
                }
            });
        });
        obsLine.observe(el, {
            childList:true // 观察子节点的添加或移除
        });
    }
    function createContent(){
        var el = ce("div", {"contenteditable":"true", "style":"float:left;display:inline;outline:none;height:100%"});
        el.onchange=function(){};
        var observer = new MutationObserver(function(mutationslist, observer){
            if(el.onchange) el.onchange();
        });
        observer.observe(el, {
            attributes:true
            , childList:true
            , characterData:true //目标节点的文本内容变化
            , subtree:true //递归地观察目标节点的所有后代节点
        });
        autoAdjLine(el);
        return el;
    }
    function Editor(el){
        if(!el){
            var id = 'hdivEditor'+parseInt(Math.random()*9999);
            el = document.write("<div id=\""+id+"\">");
            el = document.all(id);
        }else{
            el=$(el);
        }
        var o ={onchange:function(){}};
        var div = ce("div", {"style":"width:100%;height:99%;overflow:auto;position:fixed;top:0px;left:0px"});
        var helLine = createline(); div.appendChild(helLine);
        var helCont = createContent(); div.appendChild(helCont);
        helCont.elline = helLine
        helCont.onchange=function(){
           o.onchange && o.onchange();
        }
        div.onload(function(){
            var w = $(this).width();
            var lw = $(helLine).width()+10;
            $(helCont).css({
                "width":(100-(lw/w*100+0.5)) + "%"
            });
        });
        el.append(div);
        o.append=function(txt){
            if(txt===undefined || txt === null)return null;
            txt = txt.replace(/<br\s*\/?\s*>/gi, "\n");
            var ls = txt.split(/\n/gi);
            var el = null;
            if(ls.length>1){
                for(var i=0; i<ls.length; i++){
                    el = this.append(ls[i]);
                }
                return el;
            }
            el = ce('div') ;
            el.style.margin='0px';
            el.innerHTML = txt;
            if(libs.length>0){
                for(var i=0; i<libs.length; i++){
                    var c = libs[i];
                    try{
                        new Function('editor, el', c)(o, el);
                    }catch(e){}
                }
            }
            helCont.appendChild(el);
            return el;
        }
        o.setLib=function(ext){
            this.setExtend(ext);
            this.setSyntax(ext);
        }
        o.setExtend=function(ext){
            $.ajax({
                url : '/js/editor/'+ext+'.js'
                , async : false
                , success : function(e){
                    libs.push(e);
                }
                , error : function(e){
                    if(e.readyState = 4 && e.status == 200){
                        libs.push(e.responseText);
                    }
                }
            });
        }
        o.setSyntax=function(ext){
            $.ajax({
                url : '/css/syntax/'+ext+'.css'
                , async : false
                ,success : function(e){
                    var el = document.createElement('style');
                    el.type = 'text/css';
                    if (el.styleSheet) {
                        el.styleSheet.cssText = e;
                    } else {
                        el.appendChild(document.createTextNode(e));
                    }
                    document.head.appendChild(el);
                }
            });
        }
        Object.defineProperty(o, 'innerHTML', {
            //set:function(v){ helCont.innerHTML = v;}
            set:function(v){ this.append(v);}
            ,get:function(){ return el.innerHTML; }
        });
        Object.defineProperty(o, 'innerText', {
            set:function(v){ this.innerHTML = text2html(v); }
            ,get:function(){ return helCont.innerText; }
        });
        o.focus=function(){
            helCont.focus();
        }
        return o;
    }
    win.Editor = win.editor = Editor;
});