(function(global, factory){
    factory(global);
})(window, function(win){
    function copy(o) {
        if( o instanceof HTMLElement){
            return copy(o.outerHTML);
        }
        var ret = false;
        // 创建一个新的 textarea 元素
        var tempTextArea = document.createElement('textarea');
        tempTextArea.value = o;
        // 将 textarea 添加到 DOM 中
        document.body.appendChild(tempTextArea);
        // 选择 textarea 的内容
        tempTextArea.select();
        tempTextArea.setSelectionRange(0, 99999); // 对于移动设备
        // 复制选中的文本到剪贴板
        try {
            ret = document.execCommand('copy');
        } catch (err) {
            console.error('Unable to copy text: ', err);
        }
        // 移除临时的 textarea
        document.body.removeChild(tempTextArea);
        return ret?true:false;
    }
    if(!win.copy)
        win.copy=copy;
    function MOTreeChange(el, cb, config){
        if(!(el instanceof HTMLElement))return;
        if(!cb) return;
        if(typeof(MutationObserver) == 'undefined') return;
        var observer = new MutationObserver(function(mutations){
            mutations.forEach(function(mutation){
                // 遍历所有新增的节点
                mutation.addedNodes.forEach(function(node){
                    cb && cb.call(node);
                });
            });
        });
        if(!config){
            // 配置观察选项
            var config = {
                childList: true, // 观察子节点的变化
                subtree: true    // 观察整个子树的变化
            };
        }
        // 开始观察 <body> 元素
        observer.observe(el, config);
    }
    // function MOTreeChange(el, cb, config){
    //     if(!(el instanceof HTMLElement))return;
    //     if(!cb) return;
    //     if(typeof(MutationObserver) == 'undefined') return;
    //     var observer = new MutationObserver((mutations) => {
    //         mutations.forEach((mutation) => {
    //             // 遍历所有新增的节点
    //             mutation.addedNodes.forEach((node) => {
    //                 cb && cb.call(node);
    //             });
    //         });
    //     });
    //     if(!config){
    //         // 配置观察选项
    //         var config = {
    //             childList: true, // 观察子节点的变化
    //             subtree: true    // 观察整个子树的变化
    //         };
    //     }
    //     // 开始观察 <body> 元素
    //     observer.observe(el, config);
    // }
    win.MOTreeChange = MOTreeChange;
});