(function(global, factor){
    factor(global);
})(window, function(win){
    var files = [], idx=0, page=0, isend=false, isfullscreen=false
        ,fsort = null , eidx = -1
        ,effect=[
            "top:-%h%px; left:0;  opacity:0;"
			,"top:%h%px; left:0;  opacity:0;"
			,"top:%h%px; left:0;  opacity:0;"
            ,"top:-%h%px; left:0;  opacity:0;"
            ,"top:-%h%px; left:%w%px; transform: rotate(180deg); opacity:0;"
            ,"top:%h%px; left:-%w%px; transform: rotate(180deg); opacity:0;"
            ,"top:%h%px; left:%w%px; transform: rotate(270deg); opacity:0;"
            ,"top:-%h%px; left:50%; opacity:0;"
            ,"opacity:0;"
        ]
    ;
    function addEffectIn(el){ return addEffect(el, 0); }
    function addEffectOut(el){ return addEffect(el, 1); }
    function addEffect(el, io){
        var  w = document.body.clientWidth;
        var  h = document.body.clientHeight;
        var n = parseInt(Math.random()* (effect.length-1));
		if( eidx > -1){
			n = io? eidx: eidx+2;
		}
        var css = effect[n];
        css = css.replace("%w%", w);
        css = css.replace("%h%", h);
        el.style = "position: fixed;" +
            "transition : all 0.5s ease-out 0s;" +
            // "transform: rotate(0deg);" +
             css;
        return el;
    }
    function createImg(url){
        var w = document.body.clientWidth;
        var h = document.body.clientHeight;
        var el = ce("img", {
            "src": durl(url,  240, 180)
            ,"scr-src": durl(url)
            ,"style": (w>h?"height":"width")+":100%"
        });
        el.bind("click,touch", function(){
            win.location = this.src;
        });
        return el;
    }
    function durl(url, w, h){
        if(!w && !h){
            w = document.body.clientWidth;
            h = document.body.clientHeight;
        }
        return "/index.php?act=showpic&w="+w+"&h="+h+"&f=" + encodeURIComponent(url);
    }
    function app(f, p, n, sort, el){
        var _me = this;
        idx = n;
        fsort = sort;
        this.$el = el;
        this.path = f.replace(/^(.+)(\/.+\.\w+)$/gi, "$1");
        this.loadPage(p, function(){
            _me.show();
        });
        // files.push(f);
        // this.show();
    }
    app.prototype.add=function(f){
        files.push(f);
    }
    app.prototype.stop=function(){
        var img = this.$el.getElementsByTagName("img");
        if(img){
            clearTimeout(img.tid);
            if(img.status < 2)
                img.src = img.minsrc;
        }
    }
    app.prototype.setEffect=function(v){ eidx=v; }
    app.prototype.previous=function(){
        idx--;
        if(idx<0)idx = files.length-1;
        this.show();
    }
    app.prototype.next=function(){
        idx++;
        if(idx>=files.length){
            if(isend){
                idx = 0;
                isend = false;
                var f = files[idx];
                files = [];
                files.push(f);
            }else{
                idx = files.length-1;
            }
        }
        this.show();
    }
    app.prototype.fullscreen=function(){
        isfullscreen = true;
    }
    app.prototype.show=function(){
        var _me = this;
        if(!this.tid)
            this.tid = (new Date).getTime()-10000;
        if(idx > (files.length-2))
            this.loadNextPage();
        var oldimg = this.$el.getElementsByTagName("img");
        for(var i=0; i<oldimg.length-1; i++){
            oldimg[i].remove();
        }
        oldimg = (oldimg.length>0)? oldimg[oldimg.length-1]:null;
        if(oldimg && oldimg.tid){
            if(oldimg.status < 2)
                oldimg.src = oldimg.minsrc;
            clearTimeout(oldimg.tid);
        }
        this.url = files[idx];
        var img = createImg(this.url);
        img.style.opacity= "0";
        if((new Date).getTime() - this.tid > 1000){
            addEffectIn(img);
            img.effect = true;
        }
		var hdiv = ce("div", {"style":"position: fixed; top: 0; left: 0; opacity:0; transition: all 0.5s ease-out 0s;"});
		hdiv.innerHTML = "Loading...";
		$(oldimg).before(hdiv);
		setTimeout(function(){ $(hdiv).css("opacity", 1); }, 500);
        img.onerror=function(){ $(hdiv).remove(); }
        img.status = 0;
        img.onload=function(){
            var _me = this;
            var w = document.body.clientWidth;
            var h = document.body.clientHeight;
			$(hdiv).remove();
            img.style.position = 'fixed';
            img.style.top = '50%';
            img.style.left = '50%';
            _me.minsrc = _me.src;
            var fn = decodeURIComponent(_me.src.replace(/.*\/(.*)/gi, '$1'));
            fn = fn.replace(/.*=.*\/(.+\..+)/gi, '$1')
            _me.alt = fn;
            document.title = fn + ' - Pictrue Preview'
            _me.status = 1;
            this.tid = setTimeout(function(){
                _me.onload=function(){
                    this.status = 2;
                    this.style.transition='';
                    _me.style.width= 'auto';
                    _me.style.height= 'auto';
                    var w = $(_me).width(), h= $(_me).height();
                    if(w > h && w > document.body.clientWidth){
                        w = document.body.clientWidth;
                        _me.style.width = w;
                    }else if(w <  h && h > document.body.clientHeight){
                        h = document.body.clientHeight;
                        _me.style.height = h;
                    }
                    this.style.marginTop = h/-2;
                    this.style.marginLeft = w/-2;
                };
                var src = _me.attributes['scr-src'].value;
                _me.src = src;
            }, 2500);
             if(isfullscreen){
                 if(img.width > img.height )
                     img.style.width = "100%";
                 else
                     img.style.height = "100%";
             }
             img.style[(w>h?"height":"width")] = '100%';
             img.style.marginTop = img.height/-2;
             img.style.marginLeft = img.width/-2;
             img.style.transform = "rotate(0deg)";
             img.style.opacity= "1";
            if(oldimg){
                (function(img){
                    if(img.effect){
                        addEffectOut(img);
                        setTimeout(function(){img.remove();}, 600);
                    }else{
                        img.remove();
                    }
                })(oldimg);
            }
        };
        this.$el.appendChild(img);
        this.tid = (new Date).getTime();
    }
    app.prototype.stop=function(){
        if(this.tid) clearInterval(this.tid);
    }
    app.prototype.delete=function(){
        files.splice(idx, 1) ;
        this.show();
    }
    app.prototype.autoplay=function(){
        this.stop();
        var _me = this;
        this.tid=setInterval(function(){_me.next();}, 8000);
    }
    app.prototype.loadNextPage=function(){
        page++;
        return this.loadPage(page);
    }
    app.prototype.loadPage=function(p, fun){
        if(isend)return;
        var url = "/index.php?act=getfiles&p=" + decodeURIComponent(this.path) + "&page=" +p;
        if(fsort)url += "&sort="+fsort;
        ajax({
            url : url
            , dataType : 'json'
            , success:function(e){
                if(!e || e.errcode != 0)return;
                if(e.files.length == 0) {
                    isend=true;
                    page = 0;
                    return ;
                }
                for(var i=0; i<e.files.length; i++){
                    var f = e.files[i];
                    var fext = f.basename.replace(/.*\.(\w+)$/gi, '$1');
                    //if("bmp;jpg;jpeg;gif;png;webp;svg;tiff;raw;pwd".indexOf(fext.toLocaleLowerCase()) < 0)continue;
                    files.push(f.path+'/'+f.basename);
                }
                fun && fun(e);
            }
        });
    }
    window.PictureCarousel = app;
});