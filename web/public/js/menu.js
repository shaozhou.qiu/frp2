(function(global, factory){
    factory(global);
})(window, function(win){
    function registerHotKey(hk, el){
        var ha = document.createElement('a');
        ha.innerHTML = "hotkey";
        ha.href="javascript:void(0)"
        ha.style='width:1px;height:1px; opacity:0; display:inline-block;position:absolute;overflow:hidden';
        el.appendChild(ha);
        ha.focus();
        el.addEventListener('keyup', function(){
            event.preventDefault();
            event.stopPropagation();
        });
        el.addEventListener('keydown', function(){
            event.preventDefault();
            event.stopPropagation();
            var ekey = event.key.toLocaleUpperCase();
            var els = hk[ekey];
            if(!els)return;
            if(els.length < 1)return;
            if(els.length == 1){
                els[0].click();
                return ;
            }
            var ael = null;
            for(var i=0; i<els.length; i++){
                var el = els[i];
                if(document.activeElement == el){
                    ael = el;
                    continue;
                }
                if(ael != null){
                    el.focus();
                    return;
                }
            }
            if(ael == null){
                els[0].focus();
                return;
            }
        });
    }
    function menu(){
        this.hotkey=[];
        this.data = [];
    }
    menu.prototype.destroy=function(){
        if(this.el){
            this.el.parentElement.removeChild(this.el);
            this.el = null;
        }
    }
    // 显示菜单
    menu.prototype.show=function(el, x, y){
        if(/^[\d\s]+$/gi.test(x))x +='px';
        if(/^[\d\s]+$/gi.test(y))y +='px';
        if(this.el != null)return;
        var div = this.create();
        if(x)div.style.left = x;
        if(y)div.style.top= y;
        if(el)
            el.appendChild(div);
        else
            document.body.appendChild(div);
        registerHotKey(this.hotkey, div);
        this.el = div;
    }
    menu.prototype.create=function(){
        var id = parseInt(Math.random()*999);
        var div = document.createElement("div");
        var _me = this;
        div.style.border = '1px solid #f3f3f3';
        div.style.position = 'fixed';
        div.style.borderRadius = '8px';
        div.style.background = '#fff';
        div.style.color = '#595858';
        div.id = "menu_"+id;
        var css = document.createElement("style");
        css.innerHTML = "#"+div.id+">ul>li:hover{background:#cbcbcb;}" +
                        "#"+div.id+">ul{list-style-type:none; }";
                        "#"+div.id+" .sl{ margin: 0px 5px; color: #b3b3b3; }";
        var ul = document.createElement("ul");
        ul.style.padding = '0px';
        ul.style.margin= '0px';
        ul.addEventListener('click', function(){_me.destroy(); });
        this.hotkey=[];
        for(var i=0; i< this.data.length; i++){
            var m = this.data[i];
            var li = document.createElement("li");
            li.style.padding = '3px 15px';
            li.style.margin= '3px 0px';
            li.style.cursor= 'pointer';
            li.style.borderRadius = '8px';
            var spn = document.createElement("span");
            (function(key, el, _me){
                if(!_me.hotkey[key])_me.hotkey[key]=[];
                _me.hotkey[key].push(el);
            })(m.hotkey.toLocaleUpperCase(), li, this);
            spn.innerHTML = '<u>'+m.hotkey.toLocaleUpperCase()+'</u>';
            li.appendChild(spn);
            spn = document.createElement("span");
            spn.className = 'sl';
            spn.innerHTML = '&nbsp;|&nbsp;';
            li.appendChild(spn);
            spn = document.createElement("span");
            spn.innerHTML = m.text;
            if(typeof(m.callback) == "string"){
                var a = document.createElement("a");
                a.innerHTML = m.text;
                a.href = m.callback;
                spn.innerHTML = '';
                spn.appendChild(a);
            }
            li.appendChild(spn);
            li.addEventListener("contextmenu", function(){
                event.preventDefault();
                event.stopPropagation();
            });
            li.addEventListener("mousedown", function(){
                event.preventDefault();
                event.stopPropagation();
            });
            li.addEventListener("mouseup", function(){
                event.preventDefault();
                event.stopPropagation();
            });
            if(typeof(m.callback) == 'function'){
                (function(li, _me, m){
                    li.addEventListener("click", function(){
                        event.preventDefault();
                        event.stopPropagation();
                        try{
                          m.callback.call(this);
                        }catch(e){}
                        _me.destroy();
                        // m.callback();
                    });
                })(li, this, m);
            }
            ul.appendChild(li);
        }
        div.appendChild(ul);
        div.appendChild(css);
        return div;
    }
    // 添加菜单
    menu.prototype.add=function(text, fun, hotkey){
        this.data.push({
            text : text
            , callback : fun
            , hotkey: hotkey
        });
    }
    if(!win.Menu)
        win.Menu = menu;
});