(function(global, factory, title, data){
    factory(global, title, data);
})(window, function(win, title, data){
    function Share(url){
       var vt=prompt("Valid Time (d,h,m,s):", "1h");
       if(!vt)return;
       var pwd='';
       if(confirm('Whether a password is required for access')){
           while(pwd.length<4){
                var n = parseInt(Math.random()*35);
                pwd += (n<25)?String.fromCharCode(n+65):n-25;
           }
       }
        var ttl=0;
        var mc = vt.match(/\d+[dhms]?/gi);
        if(mc){
            for (var i = 0; i < mc.length; i++) {
                var m = /(\d+)([dhms]?)/gi.exec(mc[i]);
                var dw = m[2].toLowerCase();
                if(dw == "d"){m[1] = m[1]*24; dw='h';}
                if(dw == "h"){m[1] = m[1]*60; dw='m';}
                if(dw == "m"){m[1] = m[1]*60; dw='s';}
                ttl += m[1];
            }
        }
       $.ajax({
            "url":"/index.php?act=lckp&ttl="+ttl+"&p="+encodeURIComponent(url) + "&pwd="+pwd
            , "dataType":"json"
            , success:function(e){
                if(!e)return;
                var el = ce('div',{'style':'position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: #000;'});
                document.body.appendChild(el);
                html = "链接:  "+e.data;
                if(pwd) html += " 提取码: "+pwd;
                el.innerHTML = html;
                // location.href = e.data;
            }
       });
    }
    win.Share = Share;
});