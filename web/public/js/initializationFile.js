(function(global, factory){
  factory(global);
})(window, function(win){
    var initializationFile=function(str){
        this.section = {};
        this.load(str);
    }
    initializationFile.prototype.load=function(str){
       var mc = str.match(/[^\n]+/gi);
       if(mc){
          var k = null;
          for(var i=0; i<mc.length; i++){
              var m =/^\s*\[(.+)\]\s*$/gi.exec(mc[i]);
              if(m){k=m[1];this.section[k]={}; continue;}
              m =/^\s*(.+?)\s*=\s*(.*?)\s*$/gi.exec(mc[i]);
              if(m && k!=null){this.section[k][m[1]]=m[2]; continue;}
          }
       }
    }
    win.initializationFile=initializationFile;
});