(function(global, factory){
    factory(global);
})(window, function(win){
    var keyMap={
        // '27' : [function(){}]
        // '8' : [function(){}]
    }, touchMap={}, isIniTouchEvent=false
        , stch=null, etch=null
    ;
    function HotKey(){
        var _me = this;
        win.onkeydown = function(){
            if(!event)return;
            _me.listenerKeydown(event.keyCode);
        }
        win.onkeyup = function(){
            if(!event)return;
            _me.listenerKeyup(event.keyCode);
        }
    }
    function iniTouchEvent(){
        if(isIniTouchEvent)return;
        isIniTouchEvent = true;
        window.addEventListener('touchstart', function(){
            if(event.touches.length>1)return;
            stch=event.touches[0];
        });
        window.addEventListener('touchmove', function(){
            if(event.touches.length>1)return;
            event.preventDefault();
            event.stopPropagation();
            etch=event.touches[0];
        }, {passive: false});
        window.addEventListener('touchend', function(){
            if(!stch || !etch)return;
            event.preventDefault();
            event.stopPropagation();
            var x = stch.pageX - etch.pageX;
            var y = stch.pageY - etch.pageY;
            var tMap=-1, step=80;
            if( (x<0?x*-1:x) > (y<0?y*-1:y) ){
                if(x > step) tMap = 3;
                if(x < step*-1) tMap = 1;
            }else{
                if(y > step) tMap = 0;
                if(y < step*-1) tMap = 2;
            }
            if(tMap > -1){
                if( touchMap[tMap] ){
                    for(var i=0; i<touchMap[tMap].length; i++)
                        touchMap[tMap][i].call(event);
                }
            }
            stch = null;
            etch = null;
        });
    }
    HotKey.prototype.moveUp = function(fun){this.move(0, fun); }
    HotKey.prototype.moveRight = function(fun){this.move(1, fun); }
    HotKey.prototype.moveDown = function(fun){this.move(2, fun); }
    HotKey.prototype.moveLeft = function(fun){this.move(3, fun); }
    HotKey.prototype.move = function(idx, fun){
        if(!touchMap[idx]) touchMap[idx]=[];
        touchMap[idx].push(fun);
        iniTouchEvent();
    }
    HotKey.prototype.add = function(keyCode, fun){
        if(/^\d+$/gi.test(keyCode)){
            return this.add("down:"+keyCode, fun);
        }else if(/^\s*([a-z]+)\s*$/gi.test(keyCode)){
            return this.add(keyCode+":-1", fun);
        }
        var m = /^(\w+):(-?\d*)$/gi.exec(keyCode);
        if(!m)return;
        var t = m[1]; keyCode = m[2]?m[2]:-1;
        t = t.replace(/^key(.+)/gi, "$1");
        if(!keyMap[t]) keyMap[t]={};
        if(!keyMap[t][keyCode])
            keyMap[t][keyCode] = [];
        keyMap[t][keyCode].push(fun);
    }
    HotKey.prototype.listenerKeydown= function(e){
        return this.listener(keyMap["down"], e);
    }
    HotKey.prototype.listenerKeyup= function(e){
        return this.listener(keyMap["up"], e);
    }
    HotKey.prototype.listener = function(keyMap, keyCode){
        if(!keyMap)return;
        var funs = keyMap[keyCode];
        if(keyCode >0) this.listener(keyMap, -1);
        if(!funs || funs.length<1)return;
        for(var i=0; i<funs.length; i++)
            funs[i]();
    }
    if(!win.HotKey)
        win.HotKey = new HotKey();
});