(function(win, factory){
    factory(win);
})(window, function(win){
    function ca(attr){
        for(var k in attr){
            var att = document.createAttribute(k);
            att.value = attr[k];
            this.attributes.setNamedItem(att);
        }
    }
    function ce(tn,attr){
        var el = document.createElement(tn);
        el.ca=ca;
        el.ce=ce;
        el.append=function(el){
            if(el instanceof Array){
                for(var i=0; i<el.length; i++)
                    this.append(el[i]);
                return this;
            }
            this.appendChild(el);
            return this;
        };
        if(attr)el.ca(attr);
        if(typeof(HTMLElement) != 'undefined' )
            if(this instanceof HTMLElement)
                this.appendChild(el);
        return el;
    }
    function createPrecessBar(){
        var div=ce('div', {
            'style':"width: 98%; height: 23px; border: 1px solid #353535; border-radius: 5px;"
        });
        var block=div.ce('div', {
            'style':"width: 1%; height: 23px; background:#7b1616; transition: all 0.8s ease-out 0s;"
        });
        var hdivtxt=div.ce('div', {
            'style':"position: absolute; margin-top: -20px; width: 100%; text-align: center; "
        });
        Object.defineProperty(div, 'value', {
            set:function(v){
                var v = (v*100).toFixed()+'%';
                hdivtxt.innerText = v;
                block.style.width = v;
            }
        });
        return div;
    }
    function createUploadUI(){
        var div=ce('div');
        div.ce('div', {
            'style':"position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: #000; opacity: 0.5; "
        });
        var dlg = div.ce('div', {
            'style':"padding:3px;width: 50%; height: 65px; background: #000000; position: fixed; top: 50%; left: 50%; margin-left: -25%; margin-top: -32px; border-radius: 10px;"
        });
        var b1 = createPrecessBar(), b2 = createPrecessBar();
        b2.style.marginTop='5px';
        dlg.append([b1, b2]);
        document.body.appendChild(div);
        // fn 文件名
        // s  总文件数
        // c  第几个文件
        // i
        // fs 当前文件总blob数
        // fi 当前正在下传文件的第几个blob
        div.callback=function(fn, s, c, i, fs, fi){
            if(s==undefined) s=1;
            if(c==undefined) c=1;
            if(i==undefined) i=1;
            if(fs==undefined) fs=1;
            if(fi==undefined) fi=1;
            var r = c/s;
            var fr = fs?fi/fs:r;
            b1.value=r;
            b2.value=fr;
            if(fr==1 && r==1) {
                setTimeout(function(){div.remove();}, 2500);
            }
        }
        return div;
    }
    function ajax(options){
        return (function(o, factory){
            var xhr = null;
            if (window.XMLHttpRequest)
                xhr = new XMLHttpRequest();  // 判断浏览器类型并且创建对象
            else
                xhr = new ActiveXObject();
            return factory(xhr, o);
        })(options, function(xhr, o){
            if(!xhr)return;
            xhr.onreadystatechange=function(){
                if(o.success && xhr.readyState==4 && xhr.status == 200){
                    var v = xhr.response;
                    if(o.dataType == "json"){
                        if( /\s*{.*\}\s*/gi.test(v)){
                            v = new Function('try{return '+v+';}catch(e){debugger;}')();
                        }else{ v = null; }
                    }
                    o.success.call(xhr, v);
                    o.complete.call(xhr);
                } else if(o.error && xhr.readyState==4 && (xhr.status!=200 || xhr.status!=304) ){
                    o.error.call(xhr);
                    o.complete.call(xhr);
                }
            };
            xhr.tryAgain=function(){
              if(!this.trytime)this.trytime=0;
              this.trytime++;
              this.send(o.data);
            }
            xhr.open("POST", o.url, true);
            xhr.send(o.data);
        });
    }
    function UploadThread(url, blobs, cb){
        this.url = url;
        this.blobs = blobs;
        this.blocks = [];
        this.cb = cb;
        this.count=0;
        this.idx=0;
        this.blobsCount=this.blobs.length;
        if(this.blobsCount<1)return;
        for(var i=0; i<this.blobsCount; i++)
            this.blocks.push(0);
        this.postBlobs(this.blobs[this.idx], this.idx);
    }
    UploadThread.prototype.info=function(info){
        var el = $("#hUploadThreadInfo");
        if(el.length<1){
            el = ce('div',{
                'id':"hUploadThreadInfo"
                ,'style':"position: fixed; bottom: 0px; text-align: center; width: 100%;"
            });
            document.body.appendChild(el);
        }else{el =el[0]; }
        var hitm = ce('div', {'style':'border-radius: 5px; padding: 15px; width: 50%; margin: 5px auto; background: #953939; color: #fff;transition: all 0.8s ease-out 0s;'});
        setTimeout(function(){hitm.remove();}, 2000);
        hitm.innerHTML = info;
        el.appendChild(hitm);
    }
    UploadThread.prototype.serverReturn=function(e, idx, fname){
        if(e.errcode==0 ||e.errcode==1){
            this.blocks[idx]=1;
        }else{
            this.blocks[idx]=-1;
        }
        this.count++;
        this.cb(fname, this.blobsCount, this.count, this.idx, e);
        this.idx++
        if(!(this.idx<this.blobsCount))return;
        this.postBlobs(this.blobs[this.idx], this.idx);
    }
    UploadThread.prototype.postBlobs=function(b, idx){
        var fd = new FormData(), _me = this;
        fd.append("file", b.blob, b.filename);
        fd.append("idx", idx);
        fd.append("count", this.blobsCount);
        ajax({
            "url" : this.url
            , "type" : "POST"
            , "data" : fd
            , "dataType" : "json"
            , "processData" : false
            , "contentType" : false
            , success : function(e){
                if(e.errcode>10000){
                    console.error(e.data);
                    _me.info(e.data);
                }
                _me.serverReturn(e, idx, b.filename);
            }
            , error : function(){
                if(!b.fails)b.fails=0;
                b.fails++;
                if(b.fails > 3)return;
                setTimeout(function(){_me.postBlobs(b, idx);}, 100);
            }
            , complete : function(){}
        });
    }
    function UploadDUThread(url, dataUrl, cb, idx){
        if(!idx)idx=0;
        var dl = dataUrl.length;
        var e = 1024*10;
        var c = parseInt(dl/e)+(dl%e?1:0);
        var s = e * idx;
        if(s > dl){cb();return;}
        var surl=url;
        if(!surl.match(/.*\?.+/gi)){
            surl += "?"
        }
        surl += "&act=updu";
        surl += "&count=" + c;
        surl += "&idx=" + idx;
        var d = dataUrl.substr(s, e);
        ajax({
            "url" : surl
            , "type" : "POST"
            , "data" : d
            , "dataType" : "json"
            , "processData" : false
            , "contentType" : false
            , success : function(e){
                if(e.errcode>1000){
                    console.error(e.data);
                    _me.info(e.data);
                }
                if(e.errcode==0) cb(e);
                UploadDUThread(url, dataUrl, cb, idx+1);
            }
            , error : function(){
                if(!b.fails)b.fails=0;
                b.fails++;
                if(b.fails > 3)return;
                setTimeout(function(){UploadDUThread(url, dataUrl, cb, idx);}, 100);
            }
            , complete : function(){}
        });
    }
    function file2blob(f){
        var size = 1024*512; // 1M
        var fs = f.size;
        var fn = f.name;
        var num = Math.ceil(fs / size);
        var data=[], s=0, e=0;
        for(var i=0; i<num; i++){
            e = s + size;
            if(e>fs) e=fs;
            data.push({
                "blob" : f.slice(s,e)
                ,"index" : i
                ,"filename" : fn
            });
            s = e;
        }
        return data;
    }
    // 多文件上传
    function uploads(url, files, rcb, cb, idx, rev){
        if(!this.files)this.files=[];
        var _me = this;
        var fs = files.length;
        if(!cb){
            var view = createUploadUI();
            cb = view.callback;
        }
        if(!idx)idx=0;
        if(!(idx<files.length))return;
        var f = files[idx];
        if(!rev)rev=[];
        upload(url, f, function(fn, s, c, i, e){
            var args = [fn, s, c, i];
            if(c/s==1) idx++;
            args.push(fs);
            args.push(idx);
            cb.apply(this, args);
            _me.files.push(args);
            if(e.errcode == 0) rev.push(e);
            if(c/s==1){
                if(idx<files.length)
                   return uploads(url, files, rcb, cb, idx, rev);
                if(rcb)rcb.call(this, _me.files, rev.length<2?rev[0]:rev);
            }
        });
    }
    // 单文件上传
    // url  指定后端处理程序
    // file 上传控制读取到的FileList文件
    // cb    上传成功后回调程序
    function upload(url, file, cb){
        var blobs = file2blob(file);
        var count=0;
        if(!cb){
            var view = createUploadUI();
            cb = view.callback;
        }
        new UploadThread(url, blobs, cb);
    }
    // 上传图片格式的url数据
    // url  指定后端处理程序
    // dataUrl 可以从canvas.toDataURL('img/png'); 格式如"data:image/png;base64,iVBORwokGg...."
    // cb    上传成功后回调程序
    function UploadDataUrl(url, dataUrl, cb){
        //var blobs = file2blob(file);
        var count=0;
        if(!cb){
            var view = createUploadUI();
            cb = view.callback;
        }
        new UploadDUThread(url, dataUrl, cb);
    }
    // upload dialog
    function onupload(url, cb){
        var ipt = ce('input');
        ipt.type = 'file';
        ipt.style.width = '0px';
        ipt.style.height = '0px';
        ipt.onchange = function(){
            if(this.files == undefined){alert("Err:Browsers do not support uploads!");return;}
            uploads(url, this.files, cb);
        };
        document.body.appendChild(ipt);
        ipt.click();
    }
    // 绑定一个Element控件，挡用户拖放文件到些控件时实现自动上传
    // 被绑定的控件
    // 指定后端处理上传程序地址
    // 上传完成后回调函数
    function drag(el, url, cb){ this.dragUpload(el, url?url:'/upload.php', cb);}
    drag.prototype.addListener = function (el, evt, fun){
        if(el.attachEvent){
            el.attachEvent('on'+evt, fun);
        } else if(el.addEventListener){
            el.addEventListener(evt, fun, false);
        }
        return el;
    }
    drag.prototype.dragUpload = function(el, url, cb){
        // 获取拖拽区域
        // var el = document.getElementById('drop-area');
        // 添加拖拽事件监听器
        this.addListener(el, 'dragover', function(e) {
          // 阻止默认行为
          if(e.preventDefault)
          e.preventDefault();
          // 添加拖拽样式
          if(el.classList)
          el.classList.add('drag-over');
        }, false);
        this.addListener(el, 'dragleave', function(e) {
          // 移除拖拽样式
          if(el.classList)
          el.classList.remove('drag-over');
        }, false);
        this.addListener(el, 'drop', function(e) {
          // 阻止默认行为
          e.preventDefault();
          // 移除拖拽样式
          if(el.classList)
          el.classList.remove('drag-over');
          // 获取拖拽的文件
          // var file = e.dataTransfer.files[0];
          // 进行文件上传操作
          // uploadFile(file);
          // url += /\?/gi.test(url)?"&":"?"
          // url +=  "p="+encodeURIComponent(location.pathname);
          // upload(url, file);
          uploads(url, e.dataTransfer.files, cb);
        }, false);
    }
    var defurl = '/upload.php';
    win.upload={
        on:function(url, cb){ return onupload(url?url:defurl, cb);}
        , dataUrl : function(url, imgDataUrl, cb){ return UploadDataUrl(url?url:defurl, imgDataUrl, cb); }
        , file:function(url, file){ return upload(url?url:defurl, file);}
        , files:function(url, file, cb){ return uploads(url?url:defurl, files, cb, idx);}
        , bind:function(el, url, cb){ return new drag(el, url?url:defurl, cb); }
        , canvas:function(url, htmlCanvas, cb){if(htmlCanvas)return this.dataUrl(url?url:defurl, htmlCanvas.toDataURL('image/png'), cb); }
    };
});