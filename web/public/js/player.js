(function(global, factory){
  factory(global);
})(window, function(win){
    var title = document.title;
    function player(data, idx){
        var _me = this;
        this.data = data;
        this.idx = idx?idx:0;
        var div = document.createElement('div');
        div.id = 'hplayer-' + parseInt(Math.random()*999);
        div.style="width: 100%; height: 100%; background: #000; position: fixed; top: 0; left: 0; ";
        var hdivcls = document.createElement('div');
        hdivcls.style="position: fixed; z-index:"+(document.all.length+1)+";";
        var tlsUtils=[
            {id:"iina", img:"iina"}
            ,{id:"potplayer", img:"potplayer"}
            ,{id:"vlc", img:"vlc"}
            ,{id:"nplayer", img:"nplayer"}
            ,{id:"infuse", img:"infuse"}
            ,{id:"intent", img:"mxplayer"}
            ,{id:"haclose", innerHTML:"x"}
          ];
        for (var i = 0; i < tlsUtils.length; i++) {
          var d = tlsUtils[i];
          this[d.id] = document.createElement('a');
          this[d.id].style = "margin-right:0.05rem;overflow:hidden;display: inline-block;width: 0.3rem; height: 0.3rem; border-radius: 0.5rem; background:#1a1a1a; color: #fff; text-align: center; line-height: 0.3rem;";
          if(d.img)
            this[d.id].innerHTML = "<img src=\"/nginx_autoindex/img/"+d.img+".webp\" style=\"width: 0.3rem; \">";
          if(d.innerHTML)
            this[d.id].innerHTML = d.innerHTML;
          hdivcls.appendChild(this[d.id]);
        }
        this.haclose.onclick=function(){ _me.close(); };
        div.appendChild(hdivcls);
        var v = document.createElement('video');
        v.style="width: 100%; height: 100%";
        var attrs={"preload":"metadata", "autoplay":"autoplay", "controls":"controls"};
        for (var k in attrs) {
            var att = document.createAttribute(k);
            att.value=attrs[k];
            v.attributes.setNamedItem(att);
        }
        v.attributes.setNamedItem(att);
        v.addEventListener('ended', function(){_me.next();});
        div.appendChild(v);
        this.video = v;
        this.el = div;
        this.tools( data[this.idx] );
        div.onkeydown=function(){
            event.preventDefault();
            event.stopPropagation();
            if(event.keyCode == 27){ // esc
                _me.close();
                return;
            }
        }
        return div;
    }
    player.prototype.tools = function(d){
        var url=d.href
        this.iina.href = "iina://weblink?url=" + url;
        this.potplayer.href = "potplayer://" + url;
        this.vlc.href = "vlc://" + url;
        this.nplayer.href = "nplayer-" + url;
        this.infuse.href = "infuse://x-callback-url/play?url=" + url;
        this.intent.href = "intent:" + url;
        this.video.src = url;
        document.title = d.title;
    }
    player.prototype.close = function(){
        document.title = title;
        this.el.remove();
    }
    player.prototype.next = function(){
        this.idx++;
        if(!(this.idx < this.data.length)){this.close(); return;}
        this.tools( data[this.idx] );
    }
    win.player=player;
})