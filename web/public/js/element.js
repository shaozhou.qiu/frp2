(function(global, factory){
    factory(global);
})(window, function(win){
     function ce(tag, attr){
         var el = null;
         if(typeof(tag) == "string"){
             el = document.createElement(tag);
         }else{
             if(typeof(HTMLElement) == "undefined"){
                 el = tag;
             }else{
                 if(typeof(HTMLElement) != 'undefined'
                  || typeof(HTMLDocument)!= 'undefined'
                 ){
                     if(tag instanceof HTMLElement
                         || tag instanceof HTMLDocument
                         || tag instanceof Window
                     ){
                         el = tag;
                     }
                 }
             }
         }
         var fun={};
        fun.ce = function(){
            var e = ce.apply(this, arguments);
            if(this.appendChild)
                this.appendChild(e);
            return e;
        };
        fun.setProto = function(k, v){
            if(!this[k])return false;
            if(k == 'style'){
                var mc = v.match(/([^:;]+)\s*:\s*([^; ]+)/gi);
                if(!mc)return this.style;
                for(var i=0; i<mc.length; i++){
                    var m =/\s*([^:;]+)\s*:\s*([^; ]+?)\s*$/gi.exec(mc[i]);
                    if(!m)continue;
                    try{
                        this.style[m[1]] = m[2];
                    }catch(e){
                        this.style[m[1]] = m[2].replace(/(\d)rem/gi, '$1');
                    }
                }
                return this.style;
            }
            this[k] = v;
            return this[k];
        }
        fun.ca = function(k, v){
            if(!k)return null;
            var ret = this.setProto(k, v);
            if(ret)return ret;
            var att = document.createAttribute(k);
            att.value = v;
            try{
                  this.attributes.setNamedItem(att);
              }catch(e){
                  try{ this[k] = v;}catch(e){}
             }
             return att;
        };
        fun.bind=function(event, callback){
            var mc = event.match(/\w+/gi);
            if(mc.length>1){
                for(var i=0; i<mc.length; i++)
                    this.bind(mc[i], callback);
                return this;
            }
            if(this.addEventListener){
                el.addEventListener(event, callback, false);
            }else if(this.attachEvent){
                this.attachEvent('on'+event, callback);
            }
            return this;
        }
        fun._style={};
        fun._style.set=function(k,v){
            if(this[k] != undefined){this[k] = v; return this;}
            var ks = k.toLocaleLowerCase();
            var mc = ks.match(/\w+/gi);
            var k2 = mc[0];
            for(var i=1; i<mc.length; i++)
              k2 += mc[i].substr(0,1).toLocaleUpperCase()+mc[i].substr(1);
            if(this[k2] != undefined){this[k2] = v;}
            if(this["webkit-"+k2] != undefined){this["webkit-"+k2] = v;}
            if(this["-moz-"+k2] != undefined){this["-moz-"+k2] = v;}
            if(this["-ms-"+k2] != undefined){this["-ms-"+k2] = v;}
            if(this["-o-"+k2] != undefined){this["-o-"+k2] = v;}
            return this;
        }
        fun.unbind=function(event, callback){
            if(this.removeEventListener){
                this.removeEventListener(event, callback, false);
            }else if(this.detachEvent){
                this.detachEvent('on'+event, callback);
            }
            return this;
        }
        if(!this.remove)
             this.remove=function(){
                 try{
                     this.parentElement.removeChild(this);
                 }catch(e){}
             }
        fun.append=function(e){
            if(!e)return this;
            if(e instanceof Array){
                for(var i=0; i<e.length; i++)
                    this.append(e[i]);
                return this;
            }
            this.appendChild(e);
            return this;
        }
        fun.css=function(e){
            if(typeof(e) == 'string'){
                var mc = e.match(/([^;:\s]+)\s*:\s*([^;]+)/gi);
                if(!mc)return this;
                var css={};
                for(var i=0; i<mc.length;i++){
                    var m = /([^;:\s]+)\s*:\s*([^;]+)\s*/gi.exec(mc[i]+"");
                    if(!m)continue;
                    css[m[1]] = m[2];
                }
                return this.css(css);
            }
            for(k in e) try{this.style[k]=e[k];}catch(e){}
            return this;
        }
        fun.onload = function(cb){
            if(typeof(cb) != "function")return;
            var _me = this;
            var ifm = this.ce("iframe", {'style':"opacity: 0; visibility: hidden; width: 1px; height: 1px;"} );
            ifm.onload=function(){
                this.parentElement.removeChild(this);
                cb.call(_me);
            };
        }
         fun.onViewport = function(fun){
             if(!fun)return;
             var _me = this;
             if(!this.parentElement){
                 this.onload(function(){ _me.onViewport(fun); });
                 return;
             }
             var win = ce(window), tid=null;
             win.bind('scroll', function(){
                 clearTimeout(tid);
                 tid=setTimeout(function(){
                     if(_me.inScreen()) fun.call(_me);
                 }, 500);
             });
            setTimeout(function(){
                if(_me.inScreen()) fun.call(_me);
            }, 100);
         }
         fun.inViewport = function(fun){this.onViewport(fun); }
         fun.inScreen = function(){
             var wins={
                 w : window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
                 ,h : window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
             }, imgp= {
                 x : this.offsetLeft
                 ,y : this.offsetTop
             };
             var rct = this.getBoundingClientRect();
             return (rct.right < wins.w
                 && rct.left > 0
                 && rct.bottom < wins.h
                 && rct.top >0
             );
         }
         function attach(o){
             for(var k in o){
                 if(typeof(o[k]) == 'object'){
                     if(!this[k]) this[k]={};
                     attach.call(this[k], o[k]);
                     continue;
                 }
                 this[k] = o[k];
             }
         }
         attach.call(el, fun);
        for(var k in attr){el.ca(k, attr[k]);}
        return el;
     }
    win.ce=ce;
});