(function(global, factory, title, data){
    factory(global, title, data);
})(window, function(win, title, data){
    if (!window.getComputedStyle) {
        getComputedStyle = function (el, pseudo) {
            this.el = el;
            this.getPropertyValue = function (prop) {
                var re = /(\-([a-z]){1})/g;
                if (prop == 'float') prop = 'styleFloat';
                if (re.test(prop)) {
                    prop = prop.replace(re, function () {
                        return arguments[2].toUpperCase();
                    });
                }
                return el.currentStyle[prop] ? el.currentStyle[prop] : null;
            };
            return this;
        };
    }
    function remToPx(v) {
        // var baseFontSize = parseFloat(getComputedStyle(document.documentElement).fontSize);
        // return v * baseFontSize;
        // return parseFloat(v * 100);
        if(v>13 && v<15){
            debugger;
        }
        return (parseFloat(v * 100)).toFixed(2);
    }
    function text2rem(txt) {
        var mc = txt.match(/([\d\.]+)[ \t]*rem/gi);
        for(var i=0; i<mc.length; i++){
            var m = /([\d\.]+)[ \t]*rem/gi.exec(mc[0]);
            txt = txt.replace(m[0], remToPx(m[1]) +'px')
        }
        return txt;
    }
    var lnks = document.getElementsByTagName('link');
    for(var i =0; i<lnks.length; i++){
        var lnk = lnks[i];
        ajax({
            'url' : lnk.href
            , success : function(e){
                var css = text2rem(e);
                console.log(css);
                var style = document.createElement('style');
                style.type = 'text/css';
                if (style.styleSheet) {
                    // This is required for IE8 and below.
                    style.styleSheet.cssText = css;
                } else {
                    style.appendChild(document.createTextNode(css));
                }
                document.body.appendChild(style);
            }
        });
    }
});