(function(global, factory, title, data){
    factory(global, title, data);
})(window, function(win, title, data){
    var page=1, idx=0, sort=null;
    function scrollbar(min, max, cb){
        var el = ce("div", {"style":"width:100%;height:10px"});
        var bar = el.ce("div",{"style":"background:#fff;height: 100%;width: 1%;transition:all 0.8 ease-out "});
        var scroll = false, range=0, v=0;
        function mv(){
          if(!scroll)return;
          if(el.offsetWidth<1)return;
          range = (event.pageX/el.offsetWidth).toFixed(2);
          bar.css("width:"+(range*100)+"%");
        }
        function mu(){
          scroll = false;
          ce(this).unbind("mousemove", mv);
          ce(this).unbind("mouseup", mu);
          if(cb) cb(range, parseInt((max-min)*range), min, max);
        }
        el.bind("mousedown", function(){
          scroll = true;
          ce(document).bind("mousemove", mv).bind("mouseup", mu);
        });
        Object.defineProperty(el, "max", {set:function(v){max=v;}, get:function(){return max;}})
        Object.defineProperty(el, "value", {set:function(e){
          v=e;
          if(scroll==false)
            bar.css("width:"+((v/max)*100).toFixed(2)+"%");
        }, get:function(){return v;}});
        Object.defineProperty(el, "part", {set:function(e){
            if(!scroll) bar.css("width:"+e);
        }});
        return el;
    }
    function createBar(video){
        var el = ce("div", {
            "style":"width: 100%;height: 42px;position: fixed;top:0;left:0; background: #000;opacity: 0.6;"
        });
        var ctlel = el.ce("div", {"style":"height:32px"});
        var ctl = ctlel.ce("div", {"style":"float:left"});
        var ctlsty="width: 20px; height: 20px; display: inline-block;border: 1px solid #fff;border-radius: 32px;font-size: 15px;text-align: center;margin:5px;line-height:18px";
        var btns=[
            {"css":ctlsty, "text":"+", "event": { "click" : function(){ if(video.playbackRate >2)return; video.playbackRate += 0.25; } }}
            ,{"css":ctlsty, "text":"&#9658", "event": { "click" : function(){ if(video.paused) play(); else video.pause(); } }}
            ,{"css":ctlsty, "text":"-", "event": { "click" : function(){ if(video.playbackRate <0.5)return; video.playbackRate -= 0.25; } }}
            ,{"css":ctlsty, "text":"F", "event": { "click" : function(){ video.requestFullscreen(); } }}
            ,{"css":ctlsty+";transform: rotate(90deg);", "text":"&#x21e5", "event": {
                "click" : function(){ location.href = video.currentSrc; }
            }}
        ];
        for(var i=0; i<btns.length; i++){
            var btn=btns[i];
            var ha=ctl.ce("a", {"href":"javascript:void(0);", "style":btn.css});
            if(btn.text) ha.innerHTML = btn.text;
            for(var k in btn.event) ha.bind(k, btn.event[k]);
        }
        var sclb = scrollbar(0, 100, function(rang, ts, min, max){
            var isp = !video.paused;
            video.pause();
            // video.currentTime = ts;
            if(isp)video.play();
        });
        var duration = 0;
        var ctime = 0;
        var cidx = 0;
        var vfn = null;
        function play(){
            video.play();
            // playBlob(ctime, cidx, vfn);
        }
        video.bind("play", function(){
          // btnPlay.innerHTML = "||";
        }).bind("pause", function(){
            // btnPlay.innerHTML = "&#9658;";
        }).bind("timeupdate", function(){
            // var t = Math.ceil(this.currentTime);
            // sclb.value = t;videos/%E7%BD%91%E7%AB%99%E7%BB%BC%E5%90%88%E6%9E%B6%E6%9E%84%E9%AB%98%E5%8F%AF%E7%94%A8
              sclb.part = Math.ceil(this.currentTime / this.duration * 100)+"%";
        }).bind("ended", function(){
            this.next();
        });
        function playNext(v){
        }
        function getBlob(ts, idx, file, cb){
            ajax({
                "url" : "/video.php?f=" + encodeURIComponent(file) + "&duration=" + duration + "&ts="+ts
                , "dataType" : "blob"
                , "success" : function(e){
                    cb && cb.call(this, e);
                }
            });
        }
        function playBlob(ts, idx, file){
            getBlob(ts, idx, file, function(e){
                console.log(this);
                console.log(e);
                video.src = blob2URL(e);
            });
        }
        function blob2URL(blob){
            if(window.URL)
              return window.URL.createObjectURL(blob);
            if(window.webkitURL)
              return window.webkitURL.createObjectURL(blob);
            return null;
        }
        function initBlob(e){
            var vdo = video;
            vdo.onloadeddata=function(){
                this.pause();
                vdo.onloadeddata = function(){};
                duration = this.duration;
                vfn = this.src;
                ajax({
                  "url" : "/video.php?f=" + encodeURIComponent(this.src) + "&duration=" + duration
                  , "dataType":"json"
                  , "success" : function(e){
                      if(!e)return;
                      sclb.max = e.duration
                      //playBlob(0, vdo.src);
                  }
                });
            }
            setVideo(e);
        }
        function setVideo(url){
            // vdo.src=e;
            var t = ["mp4","ogg","webm"];
            video.innerHTML = '';
            for(var i=0; i<t.length;i++)
                video.ce("source", {"src":url, "type":"video/"+t[i]});
        }
        var app = ctlel.ce("div", {"style":"float:left"});
        var hdivtit = document.createElement("div");
        el.setUrl=function(e){
            var url = location.origin+e;
            var apps=[
                {img:"iina", proto:"iina://weblink?url={url}"}
                ,{img:"potplayer", proto:"potplayer://{url}"}
                ,{img:"vlc", proto:"vlc://{url}"}
                ,{img:"nplayer", proto:"nplayer-{url}"}
                ,{img:"infuse", proto:"infuse://x-callback-url/play?url={url}"}
                ,{img:"mxplayer", proto:"intent:{url}"}
              ];
            app.innerHTML='';
             for(var i=0; i<apps.length; i++){
                var ap = apps[i];
                var a = app.ce("a", {
                  "style":"background-image: url(\"/img/"+ap.img+".webp\"); background-size: 18px 18px;width: 18px; height: 18px; display: inline-block;margin:5px"
                  , "href":ap.proto.replace(/\{url\}/gi, url)
                  , "title": ap.img
                });
             }
            //video.src = e;
            initBlob(e);
            var tit = decodeURIComponent(e);
            tit = tit.replace(/.*\/(.*)/gi, "$1");
            hdivtit.innerHTML = tit;
        };
        el.ce("div").append(sclb);
        el.ce("div").append(hdivtit);
        return el;
    }
    function createView(){
        var el = ce("div", {"style":"width:100%"});
        var vel = el.ce("video", {"style":"width:100%;background:#000"
            , "preload":"metadata"
            , "autoplay":"autoplay"
            , "controls":"controls"
        });
        vel.volume = 1;
        vel.onerror=function(){
            var hdiv = el.ce("div", {"style":"position: fixed;bottom: 0px;color: #fff;width:100%;background:red;"});
             hdiv.innerHTML = "err: cant't read " + vel.src;
        }
        vel.repostion=function(){
            this.css({
                "position" : "fixed"
                ,"top" : "50%"
                ,"left" : "50%"
                ,"margin-top" : this.clientHeight/-2
                ,"margin-left" : this.clientWidth/-2
            });
        }
        vel.bind("play", function(){this.repostion(); });
        vel.onload( function(){this.repostion(); } );
        window.addEventListener("resize", function(){ vel.repostion(); });
        if(document.body.clientWidth>document.body.clientHeight){
           vel.css("height:100%");
        }else{
            vel.css("width:100%");
        }
        vel.oncanplay=function(){
            try{
                this.play();
            }catch(e){}
        }
        var b = createBar(vel);
        vel.idx=idx;
        vel.next=function(){
            if(!this.flist)return;
            this.idx++;
            if(this.idx > (this.flist.length-1))return;
            if(!el.play(this.flist[this.idx])){
                this.next(); return;
            }
            setTimeout(function(){ vel.play(); },1000);
        }
        el.setPlaylist=function(e){
            vel.flist = e;
        }
        el.play=function(v) {
            if(!v)return false;
            if("mp3;mkv;webp;mp4;mpg;avi;flv;rm;rmvb;mkv;wmv".indexOf(v.replace(/.*\.(.+)$/gi, '$1')) == -1)return false;
            b.setUrl(v);
            vel.src = v;
            document.title = decodeURIComponent(v.replace(/.*\/(.*)/gi, "$1"));
            return true;
        };
        el.append(b);
        el.bind("mouseover, touchstart", function(){
            b.style.display = "block";
        }).bind("mouseout, touchend", function(){
            setTimeout(function(){
                b.style.display = "none";
            }, 3000);
        });
        return el;
    }
    function video(url, fn,  el, p, n, s){
        page = p; idx = n; sort = s;
        var elv = createView();
        var _me = this;
        if(el) el.appendChild(elv);
        else document.body.appendChild(elv);
        elv.play(url);
        var p = url.replace(/(.+)\/.*$/gi, "$1");
        ajax({
            url : "/index.php?act=getfiles&p=" + encodeURIComponent(p) +
                  "&page=" + page +
                  "&sort=" + sort
            , dataType : "json"
            , success : function(e){
                if(e.errcode != 0)return;
                var data = [];
                for(var i=0; i< e.files.length; i++){
                    var f = e.files[i];
                    data.push("/datastorage"+f.path+"/"+f.basename);
                }
                elv.setPlaylist(data);
            }
        });
    }
    win.Video = video;
});