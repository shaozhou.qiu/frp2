(function(global, factory){
    var mc = factory.toString().match(/api\.\w+/gi);
    var webapi = {};
    if(mc){        
        for(var i=0; i<mc.length; i++){
            var m = /api\.(\w+)/gi.exec(mc[i]);
            if(!m)continue;
            webapi[m[1]] = function(){};
        }
    }
    if(typeof(WebAppInterface) == "object"){
        webapi = WebAppInterface;
    }   
    factory(global, webapi);
})(window, function(win, api){
    var wsconnected =false;
    var tidWS = null;
    var ws = null;
    function reConnectWebSocketServer(url){
        if(wsconnected)return;
        connectWebSocketServer(url);
        // setTimeout(function(){reConnectWebSocketServer(url);}, 3000);
    }
    function connectWebSocketServer(url){
        if (typeof WebSocket !== 'undefined' && WebSocket) {
            if (ws && ws.readyState === WebSocket.OPEN) {
                return ws;
            }
            ws = new WebSocket(url);
            ws.onopen=function(){
                wsconnected = true;
                cslLog('websocket is connected');
            };
            ws.onerror=function(){
                wsconnected = false;
                if(tidWS)clearTimeout(tidWS);
                tidWS = setTimeout(function(){reConnectWebSocketServer(url);}, 3000);
            };
            ws.onclose=function(){
                wsconnected = false;                
                if(tidWS)clearTimeout(tidWS);
                tidWS = setTimeout(function(){reConnectWebSocketServer(url);}, 3000);
            }
            ws.onmessage=function(e){
                cslLog('from:'+e.origin);
                cslLog(e.data);
            }
        }
        return ws;
    }
    var logs =function () {
        var url = (typeof(WebSocketUrl)!='undefined')?WebSocketUrl:'ws://10.0.0.8:8080';
        connectWebSocketServer(url);
        var id = parseInt(Math.random()*9999);
        var css = 'position: fixed;'+
                'bottom: 0;'+
                'background: #333;'+
                'color: #f3f3f3;'+
                'width: 100%;'+
                'border-top: 1px solid #222;'+
                'max-height: 60%;'+
                'overflow: auto;'+
                'display: none;';
        var el = ce('div', {'id':'log-'+id, 'class':'console', 'style':css});
        var li = el.ce('ul', {'v-on:dblclick':'toggleHeight'}).ce('li', {'v-for':'d in data', 'v-bind:class':'d.type'});
        li.ce('span', {'class':'date'}).innerHTML='{{ dataformate(d.time) }}';
        // li.ce('span').innerHTML='{{ d.content }}';
        li.ce('span', {'v-html':'toHTML(d.content)'});
        li.ce('span', {'class':'superscript', 'v-show':'d.count>1'}).innerHTML='{{ d.count }}';
        el.ce('i', {'class':'bi bi-arrow-bar-up', 'v-on:click':'unfold'});
        
        
        var VueMount = function(elid){return new Vue({
            el : elid
            , data:{
                data : []
            }
            , methods : {
                dataformate : function (v) {
                    var d = new Date(v);
                    return (d.getMonth()+1)
                    + "/" + ("00"+d.getDate()).substr(-2, 2)
                    + " " + ("00"+d.getHours()).substr(-2, 2)
                    + ":" + ("00"+d.getMinutes()).substr(-2, 2);
                }
                , toggleHeight : function (v) {
                    el.style.height='80%';
                }

                , unfold : function () {
                    var hi = el.getElementsByTagName('i')[0];
                    if($(hi).hasClass('bi-arrow-bar-up')){
                        hi.classList.remove('bi-arrow-bar-up');
                        hi.classList.add('bi-arrow-bar-down');
                        el.style.height = '80%';
                    }else{
                        hi.classList.remove('bi-arrow-bar-down');
                        hi.classList.add('bi-arrow-bar-up');                    
                        el.style.height = '30%';
                    }
                }
                , toHTML : function (v) {
                    if(!v || typeof(v) != 'string')return v;
                    v = v.replace(/</gi, '&lt;');
                    v = v.replace(/>/gi, '&gt;');
                    v = v.replace(/\n/gi, '<br>');
                    return v;
                }
                , addd : function (t, d, error) {
                    var isSame = false;
                    for(var i=0; i<d.length; i++){
                        var v=d[i], count=1;
                        if(this.data.length>0 
                            && this.data[this.data.length-1].type == t
                            && this.data[this.data.length-1].content == v
                            ){
                                this.data[this.data.length-1].count++;
                                isSame = true;
                                break;
                        }
                        var msg = {
                            type : t, 
                            error : error, 
                            content : v, 
                            count : count, 
                            time : (new Date).getTime()
                        };
                        if(ws && ws.readyState === WebSocket.OPEN){
                            var o = Object.copy(msg);
                            o.error = stack(msg.error);
                            ws.send(Object.serialization(o));
                        }
                        if(t == 'error' || t == 'warn'){
                            var dbg = api.get('debugger');
                            if(dbg!='1') return;
                        }
                        this.data.push(msg);
                    }
                    if(isSame)return;
                    if(typeof(storage) != 'object')return;
                    // var logd = Array.from(this.data);
                    var logd = this.data;
                    if(logd.length > 200)
                        logd.splice(0, logd.length-200);
                    storage.setItem('log', logd);
                }
                , loadHistory : function () {
                    if(typeof(storage) != 'object')return;
                    var logs = api.getLog(-50, -1);                    
                    if(logs != null && logs != "" ){
                        var lins = logs.split(/\n/gi);
                        logs = [];
                        for(var i=0; i<lins.length; i++){
                            var l = lins[i];
                            var m = /(\d+([\-\s:]\d+){5})\s+(\d+)\s+(.+)/gi.exec(l);
                            if(m){
                                logs.push({
                                    type: m[3]=="2"?"warn":m[3]=="3"?"error":"log"
                                    , content: m[4]
                                    , count: 1
                                    , time: (new Date(m[1])).getTime()
                                });
                            }
                        }
                    }else{
                       logs = storage.getItem('log');                        
                    } 
                    if(logs == null)return;
                    for(var i=0; i<logs.length; i++){
                        this.data.push(logs[i]);
                    }
                }    
            }
            , watch:{
                'data' : function(){
                    setTimeout(function(){
                        el.scrollTop = el.scrollHeight - el.clientHeight;
                    }, 100);
                }
            }
            , Updated : function () {
                el.scrollTop = el.scrollHeight - el.clientHeight;
            }
            , mounted : function () {
                this.loadHistory();
            }
        });}

        var app = null;
        if(document.body){
            document.body.appendChild(el);
            app = VueMount('#'+el.id);
            el = $('#'+el.id)[0];
        }else{
            // 当 DOM 加载完毕时执行
            document.addEventListener('DOMContentLoaded', function() {
                document.body.appendChild(el);
                app = VueMount('#'+el.id);
                el = $('#'+el.id)[0];
            });
        }
        var o =  {
            show : function() {
                if(app == null){
                    document.body.appendChild(el);
                    app = VueMount('#'+el.id);
                }
                $(el).show(); 
                el.scrollTop = el.scrollHeight - el.clientHeight;
            }
            , hide : function() {$(el).hide(); }
            , toggleDisplay : function() { $(el).is(':visible')?this.hide():this.show(); }
            , error : function(){o._log('error', arguments, this);}
            , warn : function(){o._log('warn', arguments, this);}
            , log : function(){o._log('log', arguments, this);}
            , _log : function(t, args, me){                    
                    var e = o.data;
                    if(!e) e=[];
                    e.push({t:t, args:args, target:me});
                    o.data = e;
                }
            , data:{}
        }
        Object.defineProperty(o, 'data', {set:(e) => {
            o.value = e;
            if(!app)return;
            while(o.value.length>0){
                var d = o.value[0];
                o.value.splice(0,1);
                app.addd(d.t, d.args, d.target);
            }
        },get:() => {
            return o.value;
        }});
        document.addEventListener('keydown', function() {
            if(event.keyCode == 123){
                o.toggleDisplay();
            }
        });
        return o;
    }
    var cslErr = console.error;
    var cslWarn = console.warn;
    var cslLog = console.log;
    var lsttime = (new Date).getTime();
    var eln = 0;
    var log = logs();
    var endlessloop=function(){
        var t  = (new Date).getTime();
        if((t - lsttime ) < 100){
            if(eln > 20)return true;
        }else{
            eln = 0;
        }
        lsttime = t;
        return false;
    }
    function stack(err){
        var err = err?err:new Error();
        var args = [];
        if(err.stack != undefined){
            var stackLines = err.stack.split('\n');
            var callerLine = stackLines[2].trim(); // 第三行是调用者
            args.push(callerLine);
        }
        return args;
    }
    console.show=function () {log.show(); }
    console.hide=function () {log.hide(); }
    console.toggleDisplay=function () {log.toggleDisplay(); }
    console.error=function(){
        if(endlessloop())return ;
        cslErr.apply(this, arguments);            
        log.error.apply(new Error(), arguments);
    }
    console.warn=function(){
        if(endlessloop())return ;
        cslWarn.apply(this, arguments);            
        log.warn.apply(new Error(), arguments);
    }
    console.log=function(){
        if(endlessloop())return ;
        cslLog.apply(this, arguments);
        log.log.apply(new Error(), arguments);
    }   
    var winErr = window.onerror;
    window.onerror = function(){
        if(endlessloop())return ;
        var d = [];
        for(var i=0; i<arguments.length; i++){
            var o = arguments[i];
            if(typeof(o) == 'object'){
                var v = o.toString();
                if(o.stack) v+= '\n'+o.stack;
                d.push(v);
                continue;
            }
            d.push(o);
        }
        log.error.apply(new Error(), d);
    }

});