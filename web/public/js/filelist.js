(function(global, factory){
    factory(global, function(el, event, handler){
        if (el.addEventListener) {
            // 现代浏览器
            el.addEventListener(event, handler, false);
        } else if (el.attachEvent) {
            // IE 8 及更早版本
            el.attachEvent('on' + event, handler);
        } else {
            // 非常老的浏览器
            el['on' + event] = handler;
        }
    });
})(window, function(win, addEvent){
    var data = [];
    var fext={
        "img": "jpg,jpeg,png,gif,bmp,webp"
        , "video": "mp3;3gp;mp4;mpg;avi;flv;rm;rmvb;mkv;wmv;ts"
        , "txt": "txt,md,log,cpp,c,h,php,ini,inf,conf,html,xml,sh,bat,json,cs,css,java,js,jsx,mjs,asp,vbs,py,rb,swift,go,m,asp,csv"
        , "exe": "php"
    }
    var vs = cookies.get('view')*1;
    function ds(f){
        var furl = f.path+'/'+ f.basename;
        furl = furl.replace(/^\/\/(.*)/gi, "\/$1");
        if(f.isfile) furl = "/datastorage" + furl;
        return furl;
    }
    function ellipsisstring(txt, count){
        if(!count) count = 20;
        if(txt.length < count)return txt;
        var p = count/2;
        return txt.substr(0, p) + "..." + txt.substr(txt.length-(p-5), p-5);
    }
    function fico(f){
        var el  = ce("span");
        if(!f.isfile) {
            el.innerHTML = "➤&nbsp;&nbsp;";
        }else if(existFext(f.basename, "img")){
            el.ce('img', {'src':'/img/pic.png', 'style':'width:0.14rem;height:0.14rem;'});
        }else if(existFext(f.basename, "video")){
            el.ce('img', {'src':'/img/play.png', 'style':'width:0.14rem;height:0.14rem;'});
        }else{
            el.ce('img', {'src':'/img/unkonw.png', 'style':'width:0.14rem;height:0.14rem'});
        }
        return el;
    }
    function fthum(f){
        var el = ce("span");
        if(f.basename=="." || f.basename==".."){
            el.innerHTML = f.basename;
            el.style.fontSize="0.8rem";
            return el;
        }
        var img = el.ce("img", {
            "thum-src":"/index.php?act=showpic&w=0&h=80&f=" +encodeURIComponent(f.path +'/'+f.basename)
            //"thum-src":"/index.php?act=showpic&w=300&h=80&f=" +encodeURIComponent(f.path +'/'+f.basename)
            ,"src":"/img/pic.png"
        });
        img.onViewport(function(){
            var attr = this.attributes.getNamedItem('thum-src');
            if(!attr)return;
            var v = attr.value;
            this.attributes.removeNamedItem('thum-src');
            this.src = v;
        });
        return el;
    }
    function num2(v){
        v = "000"+v;
        return v.replace(/.*(.{2})$/gi, '$1');
    }
    function existFext(ext, n){
        var exts = (fext[n]+'').toUpperCase();
        if(!exts)return false;
        var m = /.*\.([^\.]+)$/gi.exec(ext);
        if(!m)return false;
        var ext = (m[1]+"").toUpperCase();
        return exts.indexOf(ext)>-1?true:false;
    }
    function fsizef(v){
        var n = 0;
        while(v>1024){
            v = v/1024;
            n++;
        }
        return v?v.toFixed(2)+["b", "kb", "M", "G", "T"][n]:0;
    }
    function onQrcode(){
        if(!this.tagName)return;
        var _me = this;
        clearTimeout(this.tid);
        this.tid=setTimeout(function(){
            qrcode.call(_me);
        }, 500);
        this.bind("mouseout", function(){
            clearTimeout(this.tid);
        });
    }
    function qrcode(){
        if(!this.getElementsByTagName)return;
        var ha = this.getElementsByTagName("a");
        if(ha.length<1)return;
        ha = ha[0];
        var _me =  this;
        var url =  ha.href;
        var fn = decodeURIComponent(url).replace(/.*?([^\/]+)$/gi, "$1");
        var hel = ce("div", {"class":"qrcode", "style":"margin-top:0.2rem;display:none"});
        hel.style.position="absolute";
        hel.ce("p").innerHTML = location.hostname;
        hel.ce("h5").innerHTML = "Copyright 39doo.com";
        function release(){
            if(_me.tid)clearTimeout(_me.tid);
            _me.tid = null;
            _me.unbind('mousemove', f2m);
            if(hel)hel.remove();
        }
        this.bind("mouseout", function(){release(); });
        var l = url.length;
        var qrcw = l>100?(l>300?300:200):100;
        hel.style.width = (qrcw+10) + "px";
        var qrc = new QRCode(hel.ce('div',{"style":"display: block ruby;"}), {
                width : qrcw,
                height : qrcw,
            correctLevel : 3
            });
            qrc.makeCode( url );
        if(fn.length >30)
            hel.ce("p").innerHTML = fn.substr(0,16)+'...'+fn.substr(-12);
        else
            hel.ce("p").innerHTML = fn;
        function f2m(){
            if(typeof(event)=="undefined")return;
            var e = event;
            if(event.touches) e=event.touches[0];
            var x = event.clientX + window.pageXOffset; //e.x;
            var y = event.clientY + window.pageYOffset; //e.y;
            var cw = document.body.clientWidth;
            var ch = document.body.clientHeight;
            // if( ( ch - y) < 350 )
            if( ( ch - y) < ch/2 )
                    hel.style.top = (y - qrcw-50) + 'px';
                else
                    hel.style.top = (y+30) + 'px';
            if( (cw - x) < 350 )
                    hel.style.left = (x - qrcw-50) + 'px';
                else
                  hel.style.left = (x+30) + 'px';
            $(hel).show();
        }
        this.bind('mousemove', f2m);
        // document.body.appendChild(hel);
        $(this).append(hel);
        setTimeout(function(){ release();}, 6000);
    }
    function fclk(url, f, page, idx, isfile){
        if(!f && this instanceof HTMLElement){
            var url = this.href;
            if(/(.*\.\w+$)|(\/LCK\-)/gi.test(url)){
                event.preventDefault();
                event.stopPropagation();
            }
            return fclk(url
                , this.attributes.href.value
                , this.attributes.page.value
                , this.attributes.idx.value
                , (this.attributes.isfile.value=='true'?true:false)
                );
        }
        var u=null;
        if(existFext(url, "img")){
            u = "/pic.html";
        }else if(existFext(url, "video")){
            u = "/video.html";
            // }else if(existFext(url, "exe")){
            //     u = "/exec/";
        }else if(existFext(url, "txt")){
            u = "/txt.html";
        }
        if(u){
            var rf = f.replace(/^\/datastorage/gi, '');
            var sort= cookies.get('sort_name');
            location.href = u+"?page="+page
                +"&idx="+idx
                +"&sort="+sort
                +"&f="+encodeURIComponent(rf);
        }else{
            var url = f;
            if(isfile){
                if(!/^\/datastorage/gi.test(url))
                    url = "/datastorage"+f;
            }
            location.href = url;
        }
    }
    function libSelect(el, cb){
        if(!cb){cb=function(){}}
        function childNodes(src, pel){
            var el = src;
            if(!el || el==pel)return;
            while(el.parentElement!=null && el.parentElement != pel){
                el = el.parentElement;
            }
            return el;
        }
        function toggle(src, pel, cb, add){
            var el = childNodes(src, pel);
            if(!el)return;
            if (!add && el.classList.contains('selected')) {
                el.classList.remove('selected');
                cb(el, 1);
            }else{
                el.classList.add('selected');
                cb(el, 2);
            }
            return el;
        }
        // el.addEventListener('mousedown', function(){
        addEvent(el, 'mousedown', function(){
            if (event.button !== 0)return;
            var _me = this;
            if(!this.data) this.data=[];
            if(event.ctrlKey){
                event.preventDefault();
                event.stopPropagation();
                this.lvcldnode = toggle(event.srcElement, this, function(el, e){
                    if(e==1) _me.data.splice(_me.data.indexOf(el), 1);
                    if(e==2) _me.data.push(el);
                });
            } else if(event.shiftKey){
                if(!this.lvcldnode){
                    this.lvcldnode = childNodes(event.srcElement, this);
                    return;
                }
                event.preventDefault();
                event.stopPropagation();
                var cldNodes = Array.from(this.childNodes);
                var eel =childNodes(event.srcElement, this);
                var s = cldNodes.indexOf(this.lvcldnode);
                var e = cldNodes.indexOf(eel);
                if(s>e){
                    s+=e; e=s-e; s-=e;
                }
                for(var i=s; i<=e; i++){
                    toggle(cldNodes[i], this, function(el, e){
                        if(e==1) _me.data.splice(_me.data.indexOf(el), 1);
                        if(e==2) _me.data.push(el);
                    }, true);
                }
                this.lvcldnode = eel;
            }else{
                for(var i=0; i<this.childNodes.length; i++)
                    this.childNodes[i].classList.remove('selected');
                this.data=[];
                var el =childNodes(event.srcElement, this);
                this.lvcldnode = el;
                el.classList.add('selected');
            }
        });
    }
    function FileList(path, el){
        this.$el = el;
        libSelect(el);
        var _me = this;
        getfilelst(path, 1, function(e){
            el.innerHTML = '';
            if(!e){
                el.innerHTML = "<span style=\"color:red\">Err : undefined !</span>";
                return;
            }
            if(e.errcode != 0){
                el.innerHTML = "<span style=\"color:red\">Err : "+e.data+"</span>";
                if(e.errcode == 30001){
                    setTimeout(function(){
                        location.href = '/share.html?key='+location.pathname.replace(/.*(LCK-\w+).*/gi, '$1')
                    }, 100);
                }else if(e.errcode == 1003){
                    //if(e.extension == 'mp4')
                    fclk(location.href+'.'+e.extension, location.pathname, 0, 0);
                }
                return ;
            }
            if(e.files.length){
                _me.add(e.files, 1);
                _me.addMore(e);
            }
        });
    }
    FileList.prototype.add=function(itm, page, index){
        if(itm instanceof Array){
            for(var i=0; i<itm.length; i++)
                this.add(itm[i], page, i);
            return;
        }
        if(vs == 0) this.addTree(itm, page, index);
        else if(vs == 1) this.addIcoList(itm, page, index);
    }
    FileList.prototype.addMore=function(e){
        var _me = this, el = ce("div", {"style":"text-align:center;width:100%;float:left;"});
        el.innerHTML = 'loading ...';
        el.onViewport(function(){
            _me.nextPage(e, function(){
                el.parentElement.removeChild(el);
            });
        });
        this.$el.appendChild(el);
    }
    FileList.prototype.nextPage=function(f, fun){
        var _me = this;
        var p = f.page*1+1;
        getfilelst(f.path, p, function(e){
            fun();
            if(e.files.length){
                _me.add(e.files, p);
                _me.addMore(e);
            }
        });
    }
    FileList.prototype.rm=function(f, cb){
        var _me = this;
        ajax({
            url : "/index.php?act=delf&f=" + encodeURIComponent(f)
            , dataType : 'json'
            , success : function(e){
                if(e && e.errcode == 0){
                    location.reload();
                }else{
                    alert(e && (e.data || e.message || e.msg));
                }
                if(cb)cb(e);
            }
        });
    }
    FileList.prototype.rename=function(f, el){
        var m = /.*\/([^\/]+)$/gi.exec(f);
        if(!m)return;
        var fname = prompt("rename file", m[1]);
        if(!fname)return;
        var _me = this;
        ajax({
            url : "/index.php?act=renf&f=" + encodeURIComponent(f) + '&fn=' + encodeURIComponent(fname)
            , dataType : 'json'
            , success : function(e){
                if(e && e.errcode == 0){
                    location.reload();
                }else{
                    alert(e && (e.data || e.message || e.msg));
                }
                location.reload();
            }
        });
    }
    FileList.prototype.contextMenu=function(f, el){
        var _me = this;
        if(!f.isfile)return;
        var m = new Menu();
        m.add('copy link address', function(){
            var url = location.origin + ds(f);
            copy(url);
        }, "c");
        m.add('Save as Link ..', function(){
            var url = ds(f);
            var ha = document.createElement('a');
            ha.setAttribute("href", url);
            ha.setAttribute("download", url.replace(/.*\/(.+)/gi, "$1"));
            document.body.appendChild(ha);
            ha.click();
            ha.parentElement.removeChild(ha);
        }, "s");
        m.add('Share ...', function(){
            Share( f.path + '/' + f.basename );
        }, "S");
        m.add('Rename', function(){
            _me.rename( f.path + '/' + f.basename, el);
        }, "r");
        m.add('delete', function(){
            _me.rm( f.path + '/' + f.basename );
        }, "d");
        if(existFext(f.basename, "exe"))
            m.add('execute', function(){ _me.execapp(f); }, "e");
        // document.addEventListener('mousedown', function(){
        addEvent(document, 'mousedown', function(){
            m.destroy();
        });
        // m.show(el, event.pageX, event.pageY);
        m.show(el, event.x, event.y);
    }
    FileList.prototype.addTree=function(f, page, idx){
        var el = ce("div"), _me = this;
        var dl = el.ce("dl");
        var dd = dl.ce("dd", {"style":"float:left"});
        var furl = ds(f)
        var a = dd.ce("a", {"href": encodeURI(furl), "page":page, "idx":idx, "isfile":f.isfile });
        a.bind("click", fclk);
        a.ce("span").appendChild(fico(f));
        a.ce("h4", {"class":"il"}).innerHTML = "&nbsp;&nbsp;" + ellipsisstring(f.basename, 60);
        if(!f.isfile) dd.appendChild(this.download(f));
        if(existFext(f.basename, "exe")) dd.appendChild(this.execapp(f));
        var d = new Date(f.mtime*1000);
        var t= num2(d.getMonth()+1)
               +"/"+num2(d.getDate())
               +"<span class=\"hidden-xs\">/"+d.getFullYear() + "</span>"
               +" " + num2(d.getHours())
               +":" + num2(d.getMinutes())
               +"<span class=\"hidden-xs\">:" + num2(d.getSeconds()) + "</span>"
               ;
        dl.ce("dd") .innerHTML = "<span title=\""+ d.getFullYear()+"-"
            + num2(d.getMonth()+1) + "-"
            + num2(d.getDate()) + ""
            +"\">"+t+"</span>";
        dl.ce("dd") .innerHTML = fsizef(f.size);
        dl.bind("touchstart",onQrcode);
        dl.bind("mouseover",onQrcode);
        dl.bind("contextmenu", function(){
            event.preventDefault();
            event.stopPropagation();
            _me.contextMenu(f, dl);
            return false;
        });
        this.$el.appendChild(el);
    }
    FileList.prototype.addIcoList=function(f, page, idx){
        var el = ce('div', {'class':'smlpic'}), _me = this;
        var dl = el.ce('dl');
        var dd = dl.ce('dd');
        var a = dd.ce("a", {"href": encodeURI(f.path+'/'+ f.basename), "page":page, "idx":idx, "isfile":f.isfile });
        a.bind("click", fclk);
        a.appendChild(fthum(f));
        dd = dl.ce('dd');
        dd.ce("h4").innerHTML = ellipsisstring(f.basename);
        var d = new Date(f.mtime*1000);
        dd.ce("div", {'class':'attr'}).innerHTML = "size:" + fsizef(f.size) +
            "<br>date : " + d.getFullYear()+'-'+ num2(d.getMonth()) + '-' + num2(d.getDate())+
            "<br>time : " + num2(d.getHours())+":"+ num2(d.getMinutes())+":"+ num2(d.getSeconds());
        dl.bind("touchstart", onQrcode);
        dl.bind("mouseover", onQrcode);
        dl.bind("contextmenu", function(){
            event.preventDefault();
            event.stopPropagation();
            _me.contextMenu(f, dl);
            return false;
        });
        this.$el.appendChild(el);
        return el;
    }
    FileList.prototype.execapp=function(f){
        var file = f.path +'/'+ f.basename;
        file = file.replace(/(.+)\/[^\/]+\/\.\.$/gi, '$1');
        file = file.replace(/(.+)\/\.$/gi, '$1');
        var el = ce("a", {
            "style" : "margin-left:0.05rem; color:green"
            , "href" : ds(f)
        });
        el.bind('click', function(){
            event.preventDefault();
            // window.open("/exec?f="+ encodeURIComponent(this.attributes['href'].value) );
            window.open("/exec"+ this.attributes['href'].value) ;
        });
        el.innerHTML = '&#x25ba;';
        return el;
    }
    FileList.prototype.download=function(f){
        var file = f.path +'/'+ f.basename;
        file = file.replace(/(.+)\/[^\/]+\/\.\.$/gi, '$1');
        file = file.replace(/(.+)\/\.$/gi, '$1');
        var el = ce("a", {
            "style":"transform: rotate(90deg); display: inline-block; margin-left: 0.08rem;color:#3f3f3f;"
            ,"href" : "/index.php?act=pkdwn&f=" + encodeURIComponent(file)
        });
        el.bind('click', function(){pkdwn(this); });
        el.innerHTML = '⇥';
        return el;
    }
    function pkdwn(el){
        event.preventDefault();
        var url = el.href, f = decodeURIComponent(url.replace(/.*&f=([^&]+).*/gi, "$1"));
        var spn = ce("span", {"style":"font-size:0.08rem;color:#f1d88d;margin-left:0.12rem;"});
        spn.innerHTML="Compressing..."
        $(el).after(spn);
        el.style.display = 'none';
        function zip(f, cb, idx){
            ajax({
                url : "/index.php?act=zip&idx="+idx+"&f=" +  encodeURIComponent(f)
                , dataType : "json"
                , success : function(e){
                    if(e.errcode==0 && e.idx == (e.count-1)){cb(e); return; }
                    if(e.errcode==0){
                        spn.innerHTML =(parseInt( (e.idx/e.count)*100 )) +"%";
                        zip(f, cb, e.idx+1);
                        return ;
                    }
                    if(e.errcode!=0){
                        var fper='';
                        if(e.permission){
                            for(var i=0; i<e.permission.length; i++)
                                fper += e.permission[i]+'\n';
                        }
                        spn.innerHTML = e.data;
                        spn.title= e.data + '\n' + fper;
                        spn.style.color="red";
                    }
                }
                , error: function(){
                    spn.innerHTML = "Err: zip";
                }
            });
        }
        zip(f, function(e){
            if(e.errcode !=0){
                span.innerHTML = e.data;
                return ;
            }
            spn.parentElement.removeChild(spn);
            el.style.display = 'inline-block';
            location.href = "/index.php?act=dwnf&f="+encodeURIComponent(e.data);
        }, 0);
    }
    function getfilelst(path, page, callback){
        var sort = cookies.get('sort_name');
        var url='/index.php?act=getfiles&p='+ encodeURIComponent(path) + "&page="+page + "&sort="+encodeURIComponent(sort);
        ajax({
            url : url
            , dataType : "json"
            , success : function(e){
                callback && callback(e);
            }
            , error : function(){
                var el = document.all('hdivlst');
                if(!el)return;
                el.innerHTML = "<span style=\"color:red\">" +
                    this.status + " " +this.statusText +
                    "</span>";
            }
        });
    }
    win.FileList = FileList;
});