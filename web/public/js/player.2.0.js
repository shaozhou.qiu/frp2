(function(global, factory){
  factory(global);
})(window, function(win){
    var doc = document
    , config = {}
    , defcontrols = true;
    /**
     * 初始化界面
     * @return {[type]} [description]
     */
    function iniView(){
        var el = doc.createElement('div')
            ,v = doc.createElement('video')
            ,hhead = doc.createElement('div')
            ,htit = doc.createElement('div')
            ,shareview = createShareView(null, function(){ el.destory(); })
            ,style = "background: #000; width: 100%; height: 100%;"
            ,attrs={"preload":"metadata", "autoplay":"autoplay"};
        if(defcontrols)attrs.controls="controls";
        el.id = "ETPlayer_" + parseInt(Math.random()*999);
        el.style = style;
        el.destory = function(){ this.remove(); if(this.ondestory)this.ondestory(); };
        v.style = style;
        for (var k in attrs) {
            var att = doc.createAttribute(k);
            att.value=attrs[k];
            v.attributes.setNamedItem(att);
        }
        Object.defineProperty(v, "playerlist", {
            get:function(){v.urls;}
            ,set:function(e){v.urls = e; v.idx=0;}
        });
        Object.defineProperty(v, "idx", {
            get:function(){v.playIndex;}
            ,set:function(e){
                if(v>=v.urls.length || v<0)return;
                v.playIndex = e;
                var d = v.urls[e], source="", url='';
                for (var t in d.urls) {
                    if(url==null || url=="")url=d.urls[t];
                    source +="<source src=\""+d.urls[t]+"\" type=\"video/"+t+"\">";
                }
                shareview.url = url;
                v.innerHTML = source;
                htit.innerText = d.title;
            }
        });
        hhead.style="z-index:"+(document.all.length+99)+";position: absolute;overflow: hidden;text-overflow: ellipsis; background: #000; color: #fff; opacity: 0.6; padding: 5px 15px; "
        hhead.appendChild(htit);
        hhead.appendChild(shareview);
        hhead.show=function(){
            if(this.tid)return;
            var _me = this;
            this.activetime = (new Date).getTime();
            this.style.display = 'block';
            this.style.visibility = 'visible';
            this.tid = setInterval(function(){
                if((new Date).getTime() - _me.activetime > 2000){
                    _me.style.display = 'none';
                    _me.style.visibility = 'hidden';
                    clearInterval(_me.tid);_me.tid=null;
                }
            }, 1000);
        }
        el.appendChild(hhead);
        el.appendChild(v);
        if(!defcontrols){
            var tools = iniToolsView(v);
            el.appendChild(tools);
            el.addEventListener('mousemove', function(){tools.show(); });
        }
        el.addEventListener('mousemove', function(){hhead.show();});
        return {container:el, video:v};
    }
    // 分享工具
    function createShareView(url, fun){
        var tlsUtils=[
            {img:"iina", proto:"iina://weblink?url={url}"}
            ,{img:"potplayer", proto:"potplayer://{url}"}
            ,{img:"vlc", proto:"vlc://{url}"}
            ,{img:"nplayer", proto:"nplayer-{url}"}
            ,{img:"infuse", proto:"infuse://x-callback-url/play?url="}
            ,{img:"mxplayer", proto:"intent:{url}"}
            ,{img:"close", proto:null, click:fun}
        ]
        , el = doc.createElement('ul');
        el.style="margin:0px";
        for(var i=0; i<tlsUtils.length; i++){
            var d = tlsUtils[i];
            var li = doc.createElement('li')
                , a = doc.createElement('a')
                , img = doc.createElement('img');
            li.style="display:inline-block;float:left;margin:0px 5px; ";
            img.style="width:32px;";
            img.src="/nginx_autoindex/img/"+ d.img +".webp";
            a.proto= d.proto;
            a.href= url?d.proto.replace("{url}", url):"javascript:void(0);";
            if(d.click)a.onclick=d.click;
            a.appendChild(img);
            li.appendChild(a);
            el.appendChild(li);
        }
        Object.defineProperty(el, "url", {set:function(v){
            var as = this.getElementsByTagName('a');
            for(var i=0; i<as.length; i++){
                var a = as[i];
                if(!a.proto)continue;
                a.href = a.proto.replace("{url}", v);
            }
        }});
        return el;
    }
    // 工具栏 界面
    function createProcessbar(min, max){
        var el = doc.createElement('span')
            ,bar = doc.createElement('span');
        el.style = "display:inline-block;padding:5px;width:80%;background:#0a0a0a;";
        bar.style = "display:inline-block;height:5px;width:10%;background:#2a2a2a";
        el.min = min?min:0; el.max = max?max:100;
        el.appendChild(bar);
        Object.defineProperty(el, "idx", {set:function(v){
            var l = (el.max - el.min)/100;
            bar.style.width= (l*v/100) + "%";
        }});
        return el;
    }
    // 工具栏 界面
    function iniToolsView(v){
        var height = 30
        ,el = doc.createElement('div')
        ,btnplay = doc.createElement('span')
        ,pb = createProcessbar()
        ,btncss="padding: 5px 10px; display: inline-block; background: red; border-radius: 11px; color: #fff; margin:2px 5px; ";
        btnplay.innerHTML = "||";
        btnplay.style = btncss;
        el.style="width: 100%; height: "+height+"px; margin-top: "+(height*-1)+"px;display: none;position: relative;  background: #0c0c0c;";
        el.hide=function(){
            this.style.display = 'none';
            this.style.visibility = 'hidden';
            clearInterval(this.htid);
            this.htid = null;
        }
        el.show=function(){
            var _me = this;
            this.style.display = 'block';
            this.style.visibility = 'visible';
            this.activetime = (new Date).getTime();
            if(this.htid)return;
            this.htid = setInterval(function(){
                if((new Date).getTime() - _me.activetime > 2000){
                    _me.hide();
                }
            }, 1000);
        }
        btnplay.addEventListener("click", function(e){
            if(this.innerText == '||'){
                this.innerText = ">"; v.pause();
            }else{
                this.innerText = "||"; v.play();
            }
        });
        v.addEventListener("timeupdate", function(e){
            // var second = parseInt(e.timeStamp);
            var second = parseInt(this.currentTime);
            var maxsecond = parseInt(this.duration);
            pb.max = maxsecond;
            pb.idx = second;
        });
        v.addEventListener("ended", function(e){this.idx++; });
        el.appendChild(btnplay);
        el.appendChild(pb);
        return el;
    }
    /**
     * 初始化事件控制
     * @return {[type]} [description]
     */
    function iniEvent(obj, playerlist, idx){
        var ifm = doc.createElement('iframe');
        ifm.style = "width:1px;height:1px;position: absolute;";
        ifm.onload = function(){ifm.remove(); iniData(obj, playerlist, idx); };
        obj.container.appendChild(ifm);
    }
    /**
     * 初始化数据
     * @param  {[type]} obj [description]
     * @return {[type]}     [description]
     */
    function iniData(obj, playerlist, idx){
        obj.video.playerlist = playerlist;
        obj.video.idx = idx;
    }
    function ajax(obj){
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) return;
            if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304) {
                obj.success && obj.success(xhr.responseText, xhr);
            }else{
                obj.error && obj.error(xhr);
            }
            obj.complete && obj.complete(xhr);
        };
        xhr.open(obj.type, obj.url, true);
        xhr.send(null);
    }
    /**
     * 自定议播放器
     * @param  {[type]} playerlist 播放列表
     *                             [{title:'video1', url:{mp4:'xx.mp4', webm:'xx.webm'}}, ...]
     * @param  {[type]} idx        播放位置，默认为0即，第一个视频
     * @return {[type]}            [description]
     */
    function ETPlayer(playerlist, idx){
        var obj = iniView();
        iniEvent(obj, playerlist, idx);
        return obj.container;
    }
    win.player=ETPlayer;
})