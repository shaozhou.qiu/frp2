(function(global, factory, title, data){
    factory(global, title, data);
})(window, function(win, title, data){
    function ajax(o){
        return (function(o, factory){
            var xhr=null;
            if (window.XMLHttpRequest)
                xhr = new XMLHttpRequest();  // 判断浏览器类型并且创建对象
            else
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            return factory(xhr, o);
        })(o, function(xhr, opt){
            if(!opt.url)return;
            var o={url:'', type:'get', async:true, data:''
                    , requestheader:{"Content-type":"application/x-www-form-urlencoded"}
                    , success:function(){}
                    , error:function(){}
                    , complete:function(){}
                };
            for(var k in opt){
                if(typeof(opt[k]) != 'object' || o[k]==undefined){
                    o[k]=opt[k]; continue;
                }
                for(var k1 in opt[k]){ o[k][k1]=opt[k][k1]; continue; }
            }
            if(o.processData == false){}
            if(o.contentType == false){
                delete o.requestheader['Content-type'];
            }
            xhr.onreadystatechange=function(){
                if(xhr.readyState==4 && xhr.status == 200){
                    var rps = xhr.response;
                    if(!rps){
                        try{
                            rps = xhr.responseText;
                            if(this.responseType == 'json')
                                rps=new Function("return "+rps)();
                            //rps=JSON.parse(rps);
                        }catch(e){}
                    }
                    if(o.dataType == 'json' && typeof(rps) != "object"){
                        rps = JSON.parse(rps);
                    }
                    o.success.call(xhr, rps);
                } else if(xhr.readyState==4 && (xhr.status!=200 || xhr.status!=304) ){
                    o.error.call(xhr);
                }
                if(xhr.readyState==4)
                    o.complete.call(xhr);
            };
            try{
                xhr.responseType = (o.dataType)?o.dataType:"text";
            }catch(e){}
            if(o.type.toLocaleLowerCase() == 'post'){
                xhr.open('POST', o.url, o.async);
                for(var k in opt.requestheader){
                    xhr.setRequestHeader(k, opt.requestheader[k]);
                }
                xhr.send(o.data);
            }else{
                xhr.open('GET', o.url, o.async);
                xhr.send();
            }
        });
    }
    win.ajax = ajax;
});