<?php
function getBlobByTime($ts, $f, $duration){
  $def = blob($f, $duration);
  var_dump($def);
}
function getBlob($f, $duration, $n){
    $fp = fopen($f, "r");
    if(!$fp)return null;

    fclose($fp);
}
function blob($f, $duration){
    $fs = filesize($f);
    $ss = floor($fs/$duration);
    $bs = $ss*20; // 1M
    $bc = floor($fs/$bs);
    if($fs%$bs>0) $bc++;
    return array(
      "size" => $bs
      ,"count" => $bc
      ,"sper" => $ss
    );
}


$f = $_GET["f"]??null;
if(!$f){
    echo json_encode(array(
      "errcode" => 10001
      ,"data" => "Err : No media file specified"
    ));
    die;
}
$f = rawurldecode($f);
$f = preg_replace("/.*\/\/".$_SERVER["HTTP_HOST"]."\/.*?(\/.*)/im", "$1", $f); 

if(!file_exists($f)){
    echo json_encode(array(
      "errcode" => 10002
      ,"data" => "Err : not found video file"
      ,"file" => $f
    ));
    die;
}
$fs = filesize($f);
$duration = ($_GET["duration"]??0)*1;
$ts = $_GET["ts"]??null;
if($ts == null){
  echo json_encode(array(
     "errcode" => 0
      ,"data" => "ok"
      ,"file" => $f
      ,"duration" => $duration
      ,"size" => $fs
      ,"blob" => blob($f, $duration)
  ));
}


#header("Accept-Ranges: 0-$fs");
#header("Content-Type: video/mp4");
#header('Access-Control-Allow-Origin: *');
#header("Content-Range: bytes {$start}-{$end}/{$size}");
#header("Content-Length: $length");

getBlobByTime($ts, $f, $duration);
