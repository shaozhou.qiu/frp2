<?php
namespace EtSoftware;

class CacheDataObject{
    private $data;
    private $time;
    private $ttl;
    public function __construct($data=null, $ttl=60){
        $this->data = $data;
        $this->time = time();
        $this->ttl = $ttl>0?$ttl:2592000;
    }
    public function load($obj){
        if(is_string($obj)){
            if(preg_match("/^\s*\{.*\}\s*$/im", $obj)){
                $obj=json_decode($obj, true);
                return $this->load($obj);
            }
            $this->data = $obj;
            return $this;
        }
        $this->data = $obj['data']??null;
        $this->ttl = $obj['ttl']??60;
        $this->time = $obj['time']??0;
        return $this;
    }
    public function getValue(){
        if(time() - $this->time > $this->ttl)return null;
        return $this->data;
    }
    public function toString(){
        if(time() - $this->time > $this->ttl)return null;
        return json_encode(array(
            'data'=>$this->data
            ,'ttl'=>$this->ttl
            ,'time'=>$this->time
        ));
    }
}
class Cache{
    private $app = NULL;
    public function __construct(){
        $this->app = $this->newMemcached();
        if($this->app == NULL)
            $this->app = $this->newAPCu();
        if($this->app == NULL)
            $this->app = $this->newFile();
    }
    private function newFile(){
        return new Class{
            public function get($k){
                $fn = "/tmp/frp_$k.cache";
                if(!file_exists($fn))return NULL;
                if(!is_writable($fn))return NULL;
                $f = fopen($fn, "r");
                if(!$f)return NULL;
                try{
                    $data = fread($f, filesize($fn));
                    fclose($f);                    
                }catch(Exception $e){
                    var_dump($e); die;
                }
                $cdo = new CacheDataObject();
                $cdo->load($data);
                $data = $cdo->getValue();
                if($data == null) $this->delete($k);
                return $data;
            }
            public function set($k, $v, $ttl){
                $fn = "/tmp/frp_$k.cache";                
                $v = (new CacheDataObject($v, $ttl))->toString();
                $f = fopen($fn, "w+");
                if(!$f)return NULL;
                $data = fwrite($f, $v, strlen($v));
                fclose($f);
                return true;
            }
            public function delete($k){
                $fn = "/tmp/frp_$k.cache";
                if(!file_exists($fn))return NULL;
                if(!is_writable($fn))return NULL;
                unlink($fn);
                return true;
            }
        };
    }
    private function newAPCu(){
        if (!function_exists('apcu_store')) return NULL;
        return new Class{
            public function get($k){
                return apcu_fetch($k);
            }
            public function set($k, $v, $ttl){
                return apcu_store($k, $v, $ttl);
            }
            public function delete($k){
                return apcu_delete($k);
            }
        };
    }
    private function newMemcached(){
        if (extension_loaded('memcached')) {
            return new Class{
                public function get($k){
                    $app = new Memcached();
                    $app->get($k);
                    $cdo = new CacheDataObject();
                    $cdo->load($app);
                    $data = $cdo->getValue();
                    if($data == null) $this->delete($k);
                    return $data;
                }
                public function set($k, $v, $ttl){
                    $v = (new CacheDataObject($v, $ttl))->toString();
                    $app = new Memcached();
                    $app->set($k, $v);
                    return true;
                }
                public function delete($k){
                    (new Memcached())->delete($k);
                    return true;
                }
            };
        }
        return NULL;
    }
    public function getobj($key){
        $data = $this->get($key);
        if(!$data)return $data;
        $data = json_decode($data, true);
        return $data["obj"];
    }
    public function get($key){
        if(!$this->app)return NULL;
        return $this->app->get($key);
    }

    public function setobj($key, $obj, $ttl=300){
        $data = array(
            "type" => gettype($obj)
            ,"obj" => $obj
        );
        $this->set($key, json_encode($data), $ttl);
    }
    public function set($key, $val, $ttl=300){
        if(!$this->app)return NULL;
        return $this->app->set($key, $val, $ttl);
    }
    public function delete($key){
        return $this->app->delete($key);
    }

}
