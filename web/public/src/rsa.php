<?php 
namespace EtSoftware;

class RSA{
	private $publicKey;
	private $privateKey;
	private $fn;
	public function __construct(){
		if(!$this->fn)
			$this->fn = "rsa.key";
		if(!file_exists($this->fn))
			$this->newRsa();
		$this->readRsa();
	}
	private function readRsa(){
		if(!file_exists($this->fn))
			return null;
		if(filesize($this->fn)<0)return;
		$fd = fopen($this->fn, "rb");
		if(!$fd)return null;		
		$d = fread($fd, filesize($this->fn));
		fclose($fd);
		if(preg_match("/^[ \-]+BEGIN PRIVATE KEY[\w\W]+END PRIVATE KEY[ \-]+/im", $d, $m)){
			$this->privateKey = $m[0];
		}
		if(preg_match("/^[ \-]+BEGIN PUBLIC KEY[\w\W]+END PUBLIC KEY[ \-]+/im", $d, $m)){
			$this->publicKey = $m[0];
		}
		return true;
	}
	private function newRsa(){
		$rsa = $this->create();
		if(file_exists($this->fn))
			unlink($this->fn);
		$fd = fopen($this->fn, "wb+");
		if(!$fd)return null;
		$d = $rsa['PrivateKey'].$rsa['PublicKey'];
		fwrite($fd, $d, strlen($d));		
		fclose($fd);
		return true;
	}
	private function create(){
		$privKeyDetails = openssl_pkey_new([
		    'private_key_bits' => 2048,
		    'private_key_type' => OPENSSL_KEYTYPE_RSA,
		]);
		// 从私钥详情中提取私钥
		$privKey = '';
		openssl_pkey_export($privKeyDetails, $privKey);

		// 获取公钥
		$pubKeyResource = openssl_pkey_get_details($privKeyDetails)['key'];

		// 将公钥保存为字符串
		$pubKey = $pubKeyResource;
		return array(
			"PrivateKey" => $privKey
			,"PublicKey" => $pubKey
		);
	}
	private function encrypted($data, $keyf, $b=2){
		// 使用公钥加密数据
		$data = $keyf($data);
		if($b == 16)
			return bin2hex($data);
		return $data;
	}	
	private function hex2bin($data){
		if(preg_match_all("/../im", $data, $mc, PREG_SET_ORDER)){
			$reVal = "";
			foreach($mc as $m){
				$reVal .= chr(hexdec($m[0]));
			}
			return $reVal;
		}
	}
	private function decrypt($data, $keyf, $b=null){		
		if($b == 16)
			$data = $this->hex2bin($data, $b);
		return $keyf($data);
	}
	public function getKeys(){
		return array(
			"publicKey" => $this->publicKey
			, "privateKey" => $this->privateKey
		);
	}
	public function getPublicKey(){
		return $this->publicKey;
	}
	public function getPrivateKey(){
		return $this->privateKey;
	}
	public function encryptedPrivateKey($data, $b=16){
		return $this->encrypted($data, function($data){
			$reVal="";
			if (!openssl_private_encrypt($data, $reVal, $this->privateKey)) {
			    return null;
			}
			return $reVal;
		}, $b);
	}
	public function encryptedPublicKey($data, $b=16){
		return $this->encrypted($data, function($data){
			$reVal="";
			if (!openssl_public_encrypt($data, $reVal, $this->publicKey)) {
			    return null;
			}
			return $reVal;
		}, $b);
	}
	public function decryptPrivateKey($data, $b=16){
		return $this->decrypt($data, function($data){
			if(!openssl_private_decrypt($data, $reVal, $this->privateKey))
				return null;
			return $reVal;
		}, $b);
	}
	public function decryptPublicKey($data, $b=16){
		return $this->decrypt($data, function($data){
			if(!openssl_public_decrypt($data, $reVal, $this->privateKey))
				return null;
			return $reVal;
		}, $b);
	}
}