<?php
namespace EtSoftware;

include_once "Image.php";
include_once "FileUtil.php";


class Thumbnail{
	private $img;
	private $file;
	private $quality;
	private $action=array();
	public function __construct($f){
		$this->file = $this->createCatchFile($f);
		$this->quality = 90;
	}
	private function load($f){
		if( !file_exists($f)){
			$img = $this->notFound($f);
		}else if( !is_readable($f)){
			$img = $this->permissionDenied($f);
		}else{
			$img = $this->loadFiles($f);
		}
		return $img;
	}
	private function loadFiles($f){
		$fu = new FileUtil();
		$img=null;
		if($fu->is_video($f)){
			$img = $this->videotoimg($f);
		}else if($fu->is_image($f)){
			$img = new Image($f);
		}else{
			$img = $this->default($f);
		}
		return $img;

	}
    private function videotoimg($f){        
        $info = shell_exec("ffmpeg -i \"$f\" 2>&1;");
        $ss="05";
        if($info){
            if(preg_match("/.*Duration:\s*(\d+):(\d+):(\d+).*/im", $info, $m)){
                $ss =   ($m[1]*60 + $m[2])*60 + $m[2];
                $ss = round($ss*0.3);
            }
        }
        $hh=substr("00".floor($ss/360), -2);
        $mm=substr("00".floor($ss/60), -2);
        $ss=substr("00".floor($ss%60), -2);
        $fn= $GLOBALS["tPath"]."/thum-".sha1($f).".jpg";
        shell_exec("ffmpeg  -ss 00:$mm:$ss -i \"$f\" -vframes 1 $fn -y");
        $wpic = $_SERVER["DOCUMENT_ROOT"]."/img/play.png";
        if(file_exists($wpic)){
            $img = new  Image($fn);
            $img->watermarkWithFile($wpic);
            return $img;
        }
        return null;
    }

	private function createCatchFile($f){
		$fn = sha1($f).".jpg";
		$tmpP = $_SERVER['DOCUMENT_ROOT']."";
		$rootdir = "tmp/thumbnail";	    
		$rdir = "$rootdir".preg_replace("/(\w{3})/im", "/$0", substr($fn, 0, 12));	    
	    if(!is_dir("$tmpP/$rdir"))(new FileUtil)->mkdirp("$tmpP/$rdir");
		$file = "$tmpP/$rdir/$fn";
		$url = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_ADDR"];
        $url .= "/$rdir/$fn";
        return array(
        	"url"=>$url
        	, "file"=>$f
        	, "filename"=>"$fn"
        	, "extension"=>"jpg"
        	, "catchFile"=>$file
        	, "path"=>"$tmpP/$rdir"
        	, "rpath"=>"$tmpP/$rootdir"
        );
	}
	private function default($f){
		$info = pathinfo($f);
        $txt = strtoupper($info['extension']);
		$fd = fopen($f, "r");
		if(!$fd)return null;
		$txt="";
		$lc=20;
		while ($lc > 0) {
			if(feof($fd))break;
			$line = fread($fd, 50);
			$txt .= "$line\n";
            $lc--;
        }
		fclose($fd);
		$img = new Image(320, 240, '#1c1c1c');
		$img->drawString($txt, 5, "#555555", 10, 10);
		return $img;
	}
	private function notFound($f){
		$img = new Image(180, 100, "#111111");
		$img->drawString("Not Found!", 5, "#ff0000");
		return $img;
	}
	private function permissionDenied($f){
		$img = new Image(180, 100, "#111111");
		$img->drawString("Permission Denied!", 5, "#ff0000");
		return $img;
	}
	
	public function resetFile($flag){
		$this->file['catchFile'] = $this->file['path']."/". sha1($this->file['path']."$flag").".". $this->file['extension'];
	}
	public function resize($w, $h=0){
		if($w<2048 || $h<2048)$this->quality=90;
		if($w<1024 || $h<1024)$this->quality=80;
		if($w<800 || $h<800)$this->quality=70;
		if($w<600 || $h<600)$this->quality=60;
		if($w<400 || $h<400)$this->quality=50;
		if($w<200 || $h<200)$this->quality=40;
		if($w<100 || $h<100)$this->quality=30;
		$this->resetFile("$w-$h");
		array_push($this->action, array("resize", $w, $h));
	}	

	public function response() {
		// $this->img->response();
		
	}
	public function getFile() {
		if(file_exists($this->file['catchFile']))
			return $this->file['catchFile'];
		$img = $this->load($this->file['file']);		
		if($img==null) return null;
		foreach($this->action as $k=>$v){
			if($v[0]=="resize")
				$img->resize($v[1], $v[2]);
		}
		$img->save($this->file['catchFile'], $this->quality);
		$img->destroy();
		$img = null;
		if(file_exists($this->file['catchFile'])){
			return $this->file['catchFile'];
		}
		return null;
	}
	public function destroy(){
		// $this->img->destroy();
		
	}
}