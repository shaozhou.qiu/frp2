<?php
namespace EtSoftware;

include_once("FirewallIplimit.php");

class Firewall{
    private $libs=array(
        "FirewallIplimit"
    );    
    public function __construct() {
        $ip = $this->getUserRealIP();
        foreach($this->libs as $l){
            if(!class_exists($l))continue;
            $l = new $l();
            $ret = $l->run($ip);
            if(!$ret || $ret['errcode'] == 0 )continue;
            $this->error($ret);
            die;
        }
    }

    private function error($msg=null) {
        // 发送 404 Not Found 响应头
        header("HTTP/1.0 403 Not Found");
        // 可选：输出一些自定义的 404 错误信息
        echo "<h1>Error 403 - Forbidden</h1>";
        echo "<p>You are not allowed to access this resource.</p>";
        if($msg){
            if(is_array($msg)) $msg = json_encode($msg, true);
            echo $msg;
        }
        exit;
    }

    private function getUserRealIP() {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipAddresses = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $realIP = trim($ipAddresses[0]);
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realIP = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_REAL_IP'])) {
            $realIP = $_SERVER['HTTP_X_REAL_IP'];
        } else {
            $realIP = $_SERVER['REMOTE_ADDR'];
        }
        return filter_var($realIP, FILTER_VALIDATE_IP); // 使用filter_var过滤无效的IP地址
    }    
    
}

new Firewall;