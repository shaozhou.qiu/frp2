<?php
namespace EtSoftware;

class IpList{
    private $list=array();
    public function __construct() {
        $this->get();
    }
    public function get(){
        $this->list = array(
            "whitelist" => $this->getWhitelist()
            , "blacklist" => $this->getBlacklist()
        );
        return $this->list;
    }
    public function isAllow($ip){
        if(in_array($ip, $this->list["whitelist"]))return true;
        if(in_array($ip, $this->list["blacklist"]))return false;
        if(count($this->list["whitelist"]) >0)
            return false;
        return true;
    }
    public function isRefuse($ip){
        if(in_array($ip, $this->list["blacklist"]))return true;
        return false;
    }
    private function getWhitelist(){return $this->_getIplist("whitelist");}
    private function getBlacklist(){return $this->_getIplist("blacklist");}
    private function _getIplist($fn){
        $fn = $_SERVER["DOCUMENT_ROOT"]."/ip-$fn.dat";
        if(!file_exists($fn)){
            $fd = fopen($fn, "w+");
            if($fd){
                $str="# 127.0.0.1";
                fwrite($fd, $str);
                fclose($fd);
            }
            return array();
        }
        $fd = fopen($fn, "r");
        if(!$fd)return array();
        $reVal = array();
        while (($l = fgets($fd)) !== false) {
            $l = preg_replace("/\s*#.*/im", "", $l);
            if(preg_match("/(^\s*#.*)|(^\s*$)/im", $l))continue;
            $l = preg_replace("/[\r\n\s]*$/im", "", $l);
            array_push($reVal, $l);
        }
        fclose($fd);
        return $reVal;
    }
}