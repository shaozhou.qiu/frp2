<?php
namespace EtSoftware;

include_once("id.php");
include_once("Cache.php");


class Share{
    private $key;
    public function __construct(){
        if (session_status() == PHP_SESSION_NONE) {
            // 如果会话没有启动，则启动会话
            session_start();
        }
        $uri = urldecode($_SERVER['REQUEST_URI']);
        if(preg_match("/[\/&\?=]LCK-([0-9A-Za-z]+)/im", $uri, $m))
            $this->key = $m[1];
        if($this->key){            
            $ret = $this->login();
            if($ret){
                echo json_encode($ret);die;
            }
        }
    }
    private function checklogin(){
        if(!$this->key)return array(
            'errcode' => 30003
            , 'data' => "Landing is not necessary"
        );
        $cache = new Cache();
        $pwd = $cache->get($this->key."_pwd");
        if($pwd==null)return array(
            'errcode' => 0
            , 'data' => "ok, No password required"
        );
        if (!isset($_SESSION[$this->key]) || $_SESSION[$this->key] !== true) {
            // 如果未登录，则重定向到登录页面
            // header('Location: share.html?key=LCK-'.$this->key);
            // exit;
            return array(
                'errcode' => 30001
                , 'data' => "Err : no landing!"
            );
        }
        return array(
            'errcode' => 0
            , 'data' => "ok"
        );
    }
    private function login(){
        $spwd = $_GET['sharepwd']??$_POST['sharepwd']??null;
        if(!$spwd)return ;
        $cache = new Cache();
        $pwd = $cache->get($this->key."_pwd");
        if($pwd == $spwd){
            $_SESSION[$this->key] = true;
            return array(
                'errcode' => 0
                , 'data' => 'ok'
            );
        }
        return array(
            'errcode' => 30001
            , 'data' => "Err : Authentication failed! "
        );

    }
    public function author(){        
        $ret = $this->checklogin();
        if($ret['errcode']==30001){
            echo json_encode($ret);die;
        }            
        return $ret;

    }
    public function decode($path){
        if(!$path)return null;
        if(is_dir($path))return $path;
        if(file_exists($path))return $path;
        if(!preg_match("/\/LCK-([^\/]+)(.*)/im", $path, $m)){
            return $path;
        }
        $v = (new Cache)->get($m[1]);
        if($v) return $v.$m[2];
    }
    public function encode($path, $ttl, $pwd){
        $path = urldecode($path);
        $id = new ID();
        $key = $id->toString();
        $url = $_SERVER["REQUEST_SCHEME"]."://"
                .$_SERVER["HTTP_HOST"]
                .(($_SERVER["SERVER_PORT"]==80)?"":":".$_SERVER["SERVER_PORT"])
                ."/LCK-".$key;
        $cache = new Cache();
        $ret = $cache->set($key, $path, $ttl);
        if($pwd)
            $cache->set($key."_pwd", $pwd, $ttl);
        return array(
            'errcode'=>0
            , 'data'=>$url
        );

    }
    
}
