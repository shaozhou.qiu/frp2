<?php
namespace EtSoftware;

include_once "Shell7z.php";
include_once "ShellTar.php";


class ShellISO{
	
	public function __construct() {
	
	}
	private function _compress($src, $fn, $label, $cmd){
		$ret = shell_exec("if command -v mkisofs &>/dev/null; then echo 'ok'; else echo ''; fi");
	    if(!preg_match("/.*ok.*/im", $ret)){
			return array( 'errcode'=>4001 , 'data'=>"mkisofs is not installed on the server" );
	    }
	    $command = "$cmd -J -joliet-long -input-charset utf-8 -V \"$label\" -o \"$fn\" \"$src\" ";
	    $ret = shell_exec($command);	    
	    if(is_file($fn))
			return array( 'errcode'=>0 , 'data'=>"$fn" );
		else{

			return array( 'errcode'=>100 , 'data'=>"Failed to generate ISO file with $cmd!");
		}
	}
	public function compress($src, $fn, $pwd=null, $label="ShellISO"){
	    $data = $this->_compress($src, $fn, $label, "genisoimage");
	    if($data['errcode'] != 0)
	    	$data = $this->_compress($src, $fn, $label, "mkisofs");
	    rrmdir($src);
	    return $data;
	}

	// 解压
	public function decompress($fn, $des=null, $pwd=null){
		return (new Shell7z)->decompress($fn, $des, $pwd);
	}


	
}