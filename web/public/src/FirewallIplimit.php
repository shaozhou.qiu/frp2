<?php
namespace EtSoftware;

include_once("IpList.php");
include_once("Cache.php");
class FirewallIplimit{    
    private $visitsNum = 50; // 访问数量限制
    private $timeLimit = 1; // 1秒内
    public function __construct() {
    }
    public function run($ip) {
        $ret = $this->isRefuse($ip);
        if($ret['errcode']!=0)return $ret;
        $ret = $this->frequentVisits($ip);
        if($ret['errcode']!=0)return $ret;
    }
    private function frequentVisits($ip) {
        $cache = new Cache();
        $ipinfos = $cache->getobj("frequentVisitsIpInfo");
        if(!$ipinfos)$ipinfos=array();
        $ips = array_column($ipinfos, 'ip');
        if(in_array($ip, $ips)){
            $idx = array_search($ip, $ips);        
            $ipinfos[$idx]["visi"]++;
            if((time()-$ipinfos[$idx]['time']) < $this->timeLimit ){
                if($ipinfos[$idx]['visi'] > $this->visitsNum ){
                    $ipinfos[$idx]['time'] = time()+($ipinfos[$idx]['visi'] - $this->visitsNum);
                    $cache->setobj("frequentVisitsIpInfo", $ipinfos);
                    return array(
                        "errcode" => 1001
                        , "data" => "Err : Too frequent visits!"
                    );
                }
            }else{
               $ipinfos[$idx]['time'] = time();
               $ipinfos[$idx]['visi'] = 0;
            }
        }else{
            array_push($ipinfos, array(
                'ip'=>$ip
                , 'visi'=>0
                , 'time'=>time()
            ));
        }
        $cache->setobj("frequentVisitsIpInfo", $ipinfos);
        return array("errcode" => 0 , "data" => "ok!" );
    }
    private function isRefuse($ip) {
        $iplst = new IpList;
        if($iplst->isRefuse($ip)){
            return array(
                "errcode" => 1000
                , "data" => "Err : The server denied you access!"
            );
        }
        return array("errcode" => 0 , "data" => "ok!" );
    }
    
}