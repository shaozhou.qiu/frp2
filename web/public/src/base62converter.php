<?php 
namespace EtSoftware;

class Base62Converter {
    public function todec($base62) {
        if (!$base62) return 0;
        $len = strlen($base62);
        if ($len < 1) return null;
        if ($len > 1) {
            $firstChar = substr($base62, 0, 1);
            $remainingChars = substr($base62, 1);
            return $this->todec($firstChar) * pow(62, $len - 1) + $this->todec($remainingChars);
        }
        $st = $this->st();
        return array_search($base62, $st);
    }
    
    private function st() {
        $c = [];
        for ($i = 0; $i < 10; $i++) {
            $c[] = strval($i);
        }
        for ($i = 65; $i < 91; $i++) {
            $c[] = chr($i);
        }
        for ($i = 97; $i < 123; $i++) {
            $c[] = chr($i);
        }
        return $c;
    }
    
    public function formdec($dec) {
        if ($dec < 0) return null;
        if ($dec > 61) {
            $data = [];
            $d = $dec;
            do {
                $y = $d % 62;
                $data[] = $y;
                $d = (int)($d / 62);
            } while ($d > 0);
            
            $reVal = "";
            foreach (array_reverse($data) as $d) {
                $reVal .= $this->st()[$d];
            }
            return $reVal;
        }
        return $this->st()[$dec];
    }
}