<?php
namespace EtSoftware;

class Dir{
    public function __construct(){
    }
    public function rmdirs($p){
        if(!$p)return null;
        if(!is_dir($p)) return null;
        $fs = glob("$p/*");
        $reVal=array("file"=>0, "dir"=>0);
        foreach($fs as $f){
            if(is_dir($f)){
                $e = $this->rmdirs($f);
                if($e){
                    $reVal["dir"] += $e["dir"];
                    $reVal["file"] += $e["file"];
                }
                continue;
            }
            unlink($f);
            $reVal["file"]++;
        }
        rmdir($p);
        $reVal["dir"]++;
        return $reVal;
    }
    public function mkdirs($p){
        if(!$p)return null;
        if(preg_match_all("/\/[^\/]+/im", $p, $mc, PREG_SET_ORDER)){
            $dir = "";
            foreach($mc as $m){
                $dir .= $m[0];
                if(is_dir($dir))continue;
                mkdir ($dir);
            }
            return $dir;
        }
        return null;
    }
}
