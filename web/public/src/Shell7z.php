<?php

namespace EtSoftware;

class Shell7z{
	
	public function __construct() {
	
	}
	public function compress($src, $fn, $pwd=null){
		$p= (!!$pwd)?"-p$pwd":"";
		$cmd = escapeshellcmd("nohup 7z a -mx=0  $p \"$fn\" \"$src\" "). " 2>&1 >/dev/null &";
		$ret = shell_exec($cmd);
		if(preg_match("/error/im", $ret)){
			return array( 'errcode'=>3001 , 'data'=>"$ret", 'dest'=>$fn, 'src'=>$src );
		}
		return array( 'errcode'=>0 , 'data'=>"$fn" );
	}

	// 解压
	public function decompress($fn, $des, $pwd=null){
		if($des==null)
			$des = preg_replace("/(.*)\..+$/im", "$1", $fn);
		$p= (!!$pwd)?"-p$pwd":"";
		$cmd = escapeshellcmd("7z x $p -o\"$des/\" \"$fn\"");
	    $ret = shell_exec($cmd);
	    return array( 'errcode'=>0 , 'data'=>"$des" );
	}
	
}
