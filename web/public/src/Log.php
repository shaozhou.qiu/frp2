<?php
namespace EtSoftware;

include_once "FileUtil.php";


class Log{
    private $fn = null;
    public function __construct(){
        $this->createlogfile();
    }
    public function warn($data){
        return $this->_log("warn", $data);
    }
    public function error($data){
        return $this->_log("Err", $data);
    }
    public function log($data){
        return $this->info($data);
    }
    public function info($data){
        return $this->_log("Info", $data);
    }
    private function createlogfile(){
        $p = "/var/log/web/".date("Y/m/");
        if($this->mklf($p) == null){
            $p = $_SERVER["DOCUMENT_ROOT"]."/log/".date("Y/m/");
        }
        $ret = $this->mklf($p);
        if(!is_writable($ret) || $ret == null){
            throw new \Exception("Err : $p no permissions", 1);                
        }
        $fn = basename($_SERVER['SCRIPT_FILENAME']);
        if(!$fn) $fn = "weblog";
        $this->fn = "$p/$fn-".date("Ymd").".log";
        $this->autoUnLink($p);
        return $this->fn;
    }
    private function autoUnLink($p){
        $fs = glob("$p/*");
        $t = 60*60*24*16;
        $n = 0;
        foreach($fs as $f){
            if(is_dir($f)){
                $n += $this->unlinkof($f);
                continue;
            }
            $ft = fileatime($f);
            $nt = time();
            if($t > ($nt - $ft))
                continue;
            unlink($f);
            $n++;
        }
        return $n;
    }
    private function mklf($p){
        $fn = basename($_SERVER['SCRIPT_FILENAME']);
        if(!$fn) $fn = "weblog";
        if(!is_dir("$p")){
            if(preg_match_all("/[^\/]+/im", $p, $mc, PREG_SET_ORDER)){
                $tp="";
                foreach($mc as $m){
                    $tp .= "/".$m[0];
                    if(is_dir($tp) )continue;
                    try {
                        if(preg_match("/(.*)\/.*/im", $tp, $m)){
                            if(!is_writable($m[1]))return null;
                        }
                        (new FileUtil)->mkdirp($tp);                        
                    } catch (\Exception $e) {
                        echo $tp; die;
                        return null;
                    }
                }
            }
        }
        return is_writable($p)?$p:null;
    }
    private function _log($t, $data){
        if(is_array($data)){
            return $this->_log($t, json_encode($data, true));
        }else if (gettype($data)=='object'){
            return $this->_log($t, serialize($data));
        }
        if($this->fn == null)return false;
        $msg = date("Y-m-d h:i:m")."\t".$this->getUserRealIP();
        $msg .= "\t".($_SERVER['PHP_AUTH_USER']??null);
        $msg .= "\t$t\t".preg_replace("/[\r\n]+/im", "", $data)."\n";

        $fd = fopen($this->fn, "a+");
        if(!$fd)return false;
        fwrite($fd, $msg, strlen($msg));
        fclose($fd);
        return true;
    }
    private function getUserRealIP() {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipAddresses = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $realIP = trim($ipAddresses[0]);
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realIP = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_REAL_IP'])) {
            $realIP = $_SERVER['HTTP_X_REAL_IP'];
        } else {
            $realIP = $_SERVER['REMOTE_ADDR'];
        }
        return filter_var($realIP, FILTER_VALIDATE_IP); // 使用filter_var过滤无效的IP地址
    }
}
