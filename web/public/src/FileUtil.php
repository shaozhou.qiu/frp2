<?php
namespace EtSoftware;

class FileUtil{
	public function __construct(){
	}
	public function mkdirp($p){
		$reVal = null;
		if(preg_match_all("/(\/[^\/]+)/im", $p, $mc, PREG_SET_ORDER)){
			foreach($mc as $m){
				$reVal .= $m[0];
				if(is_dir($reVal))continue;
				try{
					$pdir = preg_replace("/(.+)\/.+/im", "$1", $reVal);
					if(!is_writable($pdir)) return null;
					mkdir($reVal);
				}catch(Exception $e){
					return null;
				}
			}
		}
		return $reVal;
	}
	public function unlinkfile($fn){
	    if(!$fn){
	        echo(json_encode(array(
	            'errcode' => 1000
	            , 'data' => $fn
	        ), true));
	        die;
	    }
	    $fn = urldecode($fn);
	    $url = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_ADDR"];
	    $fn= preg_replace("/".preg_quote($url, "/")."(.*)/im", "$1", $fn) ;
	    $ret = is_file($fn)?unlink($fn):rmdir($fn);
	    echo(json_encode(array(
	        'errcode' => 0
	        , 'data' => "ok"
	    )));
	    die;
	}
	public function readBFile($f){
	    if(!$f || !is_file($f)) return null;
	    if( headers_sent() ) die('Headers Sent');
	    $fs = filesize($f);
	    if($fs<1){ return ""; }
	    $hdl = fopen($f, "rb");
	    $content = null;
	    if($hdl){
	        if(flock($hdl, LOCK_EX)){
	            $content = fread($hdl, $fs);
	            flock($hdl, LOCK_UN);
	        }
	        fclose($hdl);
	    }
	    if(function_exists("mb_detect_encoding")){
	        $currentEncoding = mb_detect_encoding($content, 'ASCII', true); // 检测当前编码，确保是ASCII
	        if ($currentEncoding === 'ASCII') {
	            $content = iconv('ASCII', 'UTF-8', $content);
	        }
	    }
	    return $content;
	}
	public function readtext($f){
	    if($f == null)return null;
	    $f = rawurldecode($f);
	    echo $this->readBFile($f);
	}
	public function savefile($f){
	    if($f == null)return null;
	    $f = rawurldecode($f);
	    $data = file_get_contents("php://input");
	    $data = urldecode($data);
	    $data = str_replace("\xC2\xA0", ' ', $data); // 将不换行空格（U+00A0）替换为普通空格（U+0020）
	    try{
	        $fd = fopen($f, "wb+");
	        if(flock($fd, LOCK_EX)){
	            fwrite($fd, $data);
	            flock($fd, LOCK_UN);
	        }
	        fclose($fd);
	        echo(json_encode(array(
	            "errcode" => 0
	            ,"data" => "ok"
	        )));
	    }catch(Exception $e){
	        echo(json_encode(array(
	            "errcode" => 1001
	            ,"data" => "Error : Permission denied!"
	        )));
	    }
	}
	public function touchFile($f){
	    if($f == null){ echo "Err : not file!"; die; }
	    $f = urldecode($f);
	    if(touch($f))
	        echo "Ok";
	    else
	        echo "Err : no permissions";
	    die;
	}
	private function fileattr($f){
	    return array(
	        "size" => filesize($f)
	        ,"mtime" => filemtime($f)
	        ,"atime" => fileatime($f)
	        ,"ctime" => filectime($f)
	    );
	}
	private function filepms8($f){
	    $v = $this->filepms($f);
	    return substr(sprintf("%o", $v), -4);
	}
	private function filepms($f){
	    $fps = fileperms($f);
	    return $fps?$fps:0;
	    #return substr(sprintf("%o", $fps), -4);
	}
	// 获取文件列表
	public function getfiles($p){
	    ini_set('memory_limit', '256M');
	    set_time_limit(30);
	    $ps=$_GET["ps"]??200;
	    if($p)  $p = rawurldecode($p);
	    $p = preg_replace("/^\/datastorage/im", "", $p);
	    $page=$_GET["page"]??1;
	    $sort=$_GET["sort"]??"";
	    $offset=$ps*($page-1);
	    $lckp = null;
	    if($p != null || !is_dir($p)){
	        $share = new Share();
	        $rlp = $share->decode($p);
	        if($rlp){$lckp = $p; $p = $rlp; }
	    }
	    if(is_file($p)){
	        return array(
	            'errcode'=>1003
	            ,'data'=>"Err : It's not a path!"
	            ,'extension'=> pathinfo($p)['extension']
	        );
	    }
	    if($p == null || !is_dir($p)){
	        return array(
	            'errcode'=>1001
	            ,'data'=>"The specified directory does not exist!"
	        );
	    }
	    $data = array('files'=>array(), 'folders'=>array(), 'nav'=>array());
	    $pars = "la";
	    if(preg_match("/^([+\-]?)(.*)$/im", $sort, $m)){
	        if($m[2] == "mtime")
	            $pars .= "t";
	        else if($m[2] == "size")
	            $pars .= "S";
	        if($m[1] == "-")
	            $pars .= "r";
	    }
	    $ckey = sha1($p);
	    if($page <2) (new Cache)->delete($ckey);
	    $files = (new Cache)->get($ckey);
	    if($files == NULL)
	        $files = shell_exec("ls -$pars \"$p\"");
	    (new Cache)->set($ckey, $files);
	    $regex = "^[a-z\+\-]+([ ]+\S+){5,}[ ]+((\d+:\d+)|(\d+))[ ]+(.+)$";
	    if(preg_match_all("/^$regex/im", $files, $mc, PREG_SET_ORDER)){
	       // var_dump($files);
	       // var_dump($mc);
	       // die;
	        $n=0;
	        foreach($mc as $m){
	            if($offset > 0){
	                $offset--;
	                continue;
	            }
	            $n++;
	            $fn = $m[5];
	            $d = array(
	                "basename" => $fn
	                ,"path"=> ($lckp!=null)?$lckp:preg_replace("/(.+?)\/$/im", "$1", $p)
	                ,"isfile"=> is_file("$p/$fn")
	                ,"perms"=> $this->filepms("$p/$fn")
	                ,"perms8"=> $this->filepms8("$p/$fn")
	            );
	            $d = array_merge($d, $this->fileattr("$p/$fn"));
	            $d = array_merge($d, array(
	                "dirname" => $lckp!=null?$lckp:$p
	                , "extension" => preg_replace("/.*\.(\w+)$/im", "$1", $fn)
	                , "filename" => preg_replace("/(.*)\.(\w+)$/im", "$1", $fn)
	            ));
	            if( $d['basename'] == "." || $d['basename'] == ".." ){
	                if($page >1)continue;
	                array_push($data['nav'], $d);
	                continue;
	            }
	            if( $d['isfile']){
	                array_push($data['files'], $d);
	            }else{
	                array_push($data['folders'], $d);
	            }
	            if($n >= $ps)break;
	        }
	    }
	    sort($data['nav']);
	    $lstData = array_merge($data['nav'], $data['folders'], $data['files']);
	    return array(
	      "errcode" => 0
	      , "path" => $lckp!=null?$lckp:$p
	      , "files" => $lstData
	      , "page" => $page
	    );
	}
	
	public function is_video($f){
		$info = pathinfo($f);
        $ext = strtolower($info['extension']);
	    $exts = "wm,amv,mp4,mpeg,avi,flv,rm,rmvb";
	    return !!strstr($exts, $ext);
	}
	public function is_image($f){
		$info = pathinfo($f);
        $ext = strtolower($info['extension']);
	    $exts = "jpg,jpeg,bmp,gif,png,webp";
	    return !!strstr($exts, $ext);
	}
    public function contentType($f){
        $path_parts = pathinfo($f);
        $ext = strtolower($path_parts["extension"]);
        $reval = "";
        // Determine Content Type
        switch ($ext) {
          case "pdf": $reval="application/pdf"; break;
          case "exe": $reval="application/octet-stream"; break;
          case "zip": $reval="application/zip"; break;
          case "doc": $reval="application/msword"; break;
          case "xls": $reval="application/vnd.ms-excel"; break;
          case "ppt": $reval="application/vnd.ms-powerpoint"; break;
          case "gif": $reval="image/gif"; break;
          case "png": $reval="image/png"; break;
          case "jpeg":
          case "jpg": $reval="image/jpg"; break;
          default: $reval="application/force-download";
        }
        return $reval;
    }

	
}