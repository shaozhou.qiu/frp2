<?php
namespace EtSoftware;

class Image{
    private $img = null;
    public function __construct($w, $h=null, $bg=null){
        if (extension_loaded('gd') && function_exists('imagecreatetruecolor')) {
           // echo "GD 库已安装。\n";
        } else {
            $e="E : The gd library is not installed!";
            echo $e;
            die;
        }

        if(is_string($w) && $h==null && $bg ==null){
            $this->img = $this->loadImage($w);
        }else if(is_numeric($w) && is_numeric($h)){
            $this->img = $this->create($w, $h, $bg);
        }
        if($this->img == NULL){
            throw new Exception("Err : cant't create image!: ");
        }
    }
    private function string2color($color){
        // 移除十六进制颜色前缀#
        $color = ltrim($color, '#');
        // 将十六进制值转换为整数
        $r = hexdec(substr($color, 0, 2));
        $g = hexdec(substr($color, 2, 2));
        $b = hexdec(substr($color, 4, 2));
        return array($r, $g, $b);
    }
    private function loadImage($file){
        if($file == null)return null;
        if(!is_readable($file))return null;
        // 获取文件扩展名
        $info = pathinfo($file);
        $extension = strtolower($info['extension']);
        $image = null;
        // 根据扩展名选择函数
        switch ($extension) {
            case 'avif':
                $image = imagecreatefromavif($file);
                break;
            case 'bmp':
                $image = imagecreatefrombmp($file);
                break;
            case 'gd':
                $image = imagecreatefromgd($file);
                break;
            case 'gd2':
                $image = imagecreatefromgd2($file);
                break;
            case 'gd2part':
                $image = imagecreatefromgd2part($file);
                break;
            case 'gif':
                $image = imagecreatefromgif($file);
                break;
            case 'jpeg':
            case 'jpg':
                $image = imagecreatefromjpeg($file);
                break;
            case 'png':
                $image = imagecreatefrompng($file);
                break;
            case 'webp':
                $image = imagecreatefromwebp($file);
                break;
            case 'xbm':
                $image = imagecreatefromxbm($file);
                break;
            case 'xpm':
                $image = imagecreatefromxpm($file);
                break;
            case 'tga':
                // 注意：PHP 默认没有提供 imagecreatefromtga 函数，可能需要第三方库
                // 示例中假设有这个函数
                $image = imagecreatefromtga($file);
                break;
            case 'wbmp':
                $image = imagecreatefromwbmp($file);
                break;
            case 'string':
                // 如果文件是以字符串形式提供的，可以使用 imagecreatefromstring
                $image = imagecreatefromstring($file);
                break;
            default:
                throw new Exception("Unsupported image format: " . $extension);
        }
        return $image;

    }
    /**
     *  创建一幅空白图片
     * */
    private function create($w, $h, $bgcolor=null){
        if(is_string($bgcolor)){
            $bgcolor = $this->string2color($bgcolor);
        }
        $img = imagecreatetruecolor($w, $h);
        if($bgcolor){
            // 设置背景颜色
            $bgc = imagecolorallocate($img, $bgcolor[0]??0, $bgcolor[1]??0, $bgcolor[2]??0);
            imagefill($img, 0, 0, $bgc);
        }
        return $img;
    }   
    /**
     *  写入文字
     * */
    public function drawString($text, $fontsize=5, $color="#0000", $x=null, $y=null){
        if(!$text)return $this;
        if($x==null){
            return $this->drawString($text, $fontsize, $color, (imagesx($this->img)-imagefontwidth($fontsize) * strlen($text))/2 , $y);
        }
        if($y==null){
            return $this->drawString($text, $fontsize, $color, $x, (imagesy($this->img)-imagefontwidth($fontsize))/2);
        }
        if(is_string($color)){
            $color = $this->string2color($color);
        }
        $lineHeight = imagefontwidth($fontsize)/2;
        // 设置字体颜色
        $txtColor = imagecolorallocate($this->img, $color[0], $color[1], $color[2]);
        if(preg_match_all("/[^\n]+/im", $text, $mc, PREG_SET_ORDER)){
            $cy = $y;
            foreach($mc as $k=>$m){
                $cy += imagefontwidth($fontsize)*$k + $lineHeight;
                imagestring($this->img, $fontsize, $x, $cy, $text, $txtColor);
            }
        }
        return $this;
    }
    /**
     * 放大或缩小
     **/
    public function resize($w, $h=0){
        $img = $this->_resize($this->img, $w, $h);
        imagedestroy($this->img);
        $this->img=$img;
        return $this;
    }
    /**
     * 放大或缩小
     **/
    private function _resize($img, $w, $h=0){
        $iw = imagesx($img);
        $ih = imagesy($img);
        $r = $iw/$ih;
        if($w==0 && $h ==0){
            $w = $iw;
            $h = $ih;
        }else{
            if($h==0)$h = $w/$r;
            if($w==0)$w = $h*$r;            
        }
        $nimg = imagescale($img, $w, $h);
        return $nimg;
    }
    /**
     * 添加水印
     * */
    public function watermarkWithFile($fn){
        if(!file_exists($fn))return null;
        $img = $this->loadImage($fn);
        return $this->watermarkWithImage($img);
    }
    public function watermarkWithImage($img){
        if(!$img)return null;
        return $this->watermarkWithImgPos($this->img, $img, 0, 0);
    }
    public function watermarkWithImgPos($dstImg, $srcImg, $pos, $zone){
        if(!$dstImg || !$srcImg)return ;
        $dstW = imagesx($dstImg);
        $dstH = imagesy($dstImg);
        $srcX = 0; $srcY = 0; 
        $srcW = imagesx($srcImg); $srcH = imagesy($srcImg);
        if($zone == 0){
            $simg = $this->_resize($srcImg, $dstW-$dstW*0.5, $dstH-$dstH*0.5);
            imagedestroy($srcImg);
            $srcImg=$simg;
            $srcW = imagesx($srcImg); $srcH = imagesy($srcImg);
        }
        $dstX=0; $dstY=0;
        switch($pos){
        case 1:
            $dstX = $dstW - $srcW;
            break;
        case 2:
            $dstX = $dstW - $srcW;
            $dstY = $dstH - $srcH;
            break;
        case 3:
            $dstY = $dstH - $srcH;
            break;
        case 4:
            $dstX=0; $dstY=0;
            break;
        default:
            $dstX = ($dstW - $srcW)/2;
            $dstY = ($dstH - $srcH)/2;
        }
        imagecopy($dstImg, $srcImg, $dstX, $dstY, $srcX, $srcY, $srcW, $srcH);
    }

    public function save($file=null, $quality=90){
        if(!$file)return;
        $info = pathinfo($file);
        $extension = strtolower($info['extension']);
        $img = $this->img;
        switch ($extension) {
            case 'avif':
                return imageavif($img, $file, $quality);
            case 'bmp':
                return imagewbmp($img, $file);
            case 'gd':
                return imagegd($img, $file);
            case 'gd2':
                return imagegd2($img, $file);
            case 'gif':
                return imagegif($img, $file);
            case 'jpeg':
            case 'jpg':
                return imagejpeg($img, $file, $quality);
            case 'png':
                return imagepng($img, $file);
            case 'webp':
                return imagewebp($img, $file, $quality);
            case 'xbm':
                return imagexbm($img, $file);
            case 'xpm':
                return imagexpm($img, $file);
            case 'tga':
                // 注意：PHP 默认没有提供 imagetga 函数，可能需要第三方库
                return imagetga($img, $file);
            case 'wbmp':
                return imagewbmp($img, $file);
            default:
                throw new Exception("Unsupported image format: " . $extension);
        }
        return $file;
    }
    public function response($type = IMAGETYPE_WEBP) {
        $img = $this->img;
        switch ($type) {
            case IMAGETYPE_GIF:
                header('Content-Type: image/gif');
                imagegif($img);
                break;
            case IMAGETYPE_JPEG:
                header('Content-Type: image/jpeg');
                imagejpeg($img);
                break;
            case IMAGETYPE_PNG:
                header('Content-Type: image/png');
                imagepng($img);
                break;
            case IMAGETYPE_WEBP:
                header('Content-Type: image/webp');
                imagewebp($img);
                break;
            case IMAGETYPE_XBM:
                header('Content-Type: image/x-xbitmap');
                imagexbm($img);
                break;
            case IMAGETYPE_WBMP:
                header('Content-Type: image/vnd.wap.wbmp');
                imagewbmp($img);
                break;
            default:
                throw new Exception("Unsupported image type: " . $type);
        }
    }
    public function destroy(){
        imagedestroy($this->img);
    }
}