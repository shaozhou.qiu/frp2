<?php
namespace EtSoftware;

class ShellTar{
	
	public function __construct() {
	
	}
	public function compress($src, $fn, $pwd=null){
		$p= (!!$pwd)?"-p$pwd":"";
		$cmd = "";
		if(preg_match("/^(.*)\/(.+)$/im", $src, $m)){
			$cmd = "cd ".$m[1].";";
			$src = $m[2];
		}
        if(preg_match(".*\.((tar)|(tar.gz))$", $fn)){
        }else{
            $fn .= ".tar.gz";
        }
		$cmd .= escapeshellcmd("tar czvf \"$fn\" \"$src\" 2>&1 >/dev/null ;");
		$ret=shell_exec($cmd);
		return array( 'errcode'=>0 , 'data'=>"$fn" );
	}

	// 解压
	public function decompress($fn, $des=null, $pwd=null){
		$p= (!!$pwd)?"-p$pwd":"";
		if($des==null)
			$des = preg_replace("/(.*)\..+$/im", "$1", $fn);
		$cmd = escapeshellcmd("tar -xvf $fn -C \"$des/\"" );
	    $ret = shell_exec($cmd);
	    return array( 'errcode'=>0 , 'data'=>"$des" );
	}
	
}
