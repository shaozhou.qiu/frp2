<?php
namespace EtSoftware;

include_once("IpList.php");

class Author{
    private $users = array();    
    public function __construct($passwd=null) {
        if($this->checkip()["errcode"] == 0){
            return ;
        }
        $passwd = $_SERVER["DOCUMENT_ROOT"]."/passwd";
        if(!file_exists($passwd))return;
        if (!isset($_SERVER['PHP_AUTH_USER'])){
            header("HTTP/1.0 401 Unauthorized");
            header("www-Authenticate:Basic realm=\"EtSoftware Author\"");
            echo " <link rel=\"stylesheet\" href=\"/css/public.css\">";
            echo "Err : no permissions!";
            exit;
        }
        $ret = $this->login($_SERVER['PHP_AUTH_USER']??null, $_SERVER['PHP_AUTH_PW']??null);
        if($ret["errcode"] != 0){
            if($this->loginTimeLimit()){
                $this->faild("Too many requests. Please try again later.");
                die;
            }
            $this->faild("You are not allowed to access this resource.");
            unset($_SERVER['PHP_AUTH_USER']);
            unset($_SERVER['PHP_AUTH_PW']);
            die;
        }
    }
    private function checkip(){
        $iplst = new IpList;
        $ip = $this->getUsrIP();
        if($iplst->isRefuse($ip)){
            $this->faild("You are not allowed to access this resource."); die;
        }
        if($iplst->isAllow($ip)){
            return array("errcode"=>0, "data"=>"ok");
        }
        return array("errcode"=>2, "data"=>"faild");

    }    
    private function getUsrIP(){
        $forwardedFor = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
        $ips = explode(',', $forwardedFor);
        // 移除空格并取第一个非私有IP地址，以防伪造
        foreach ($ips as $ip) {
            $ip = trim($ip);
            if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {
                $clientIp = $ip;
                break;
            }
        }
        if (!$clientIp) {
            $clientIp = $_SERVER['REMOTE_ADDR'];
        }
        return $clientIp;
    }   
    private function parsePwd($v){
        if(preg_match_all("/\{([YmdHisyMFDlhgGAa]+)\}/im", $v, $mc, PREG_SET_ORDER)){
            $t = time();
            foreach($mc as $m){
                $tv = date($m[1], $t);
                $v = preg_replace("/".preg_quote($m[0])."/im", $tv, $v);
            }
        }
        return $v;
    }
    private function getPasswd(){
        $passwd = $_SERVER["DOCUMENT_ROOT"]."/passwd";
        if(!file_exists($passwd))return ;
        $fd = fopen($passwd, "r");
        if(!$fd)return ;
        while (($l = fgets($fd)) !== false) {
            if(preg_match("/^\s*([^#\/]\w+?)\:(.+?)[\r\n\s]*$/im", $l, $m)){
                $pwd = $this->parsePwd($m[2]);
                array_push($this->users, array(
                    "usr" => $m[1]
                    , "passwd" => $pwd
                ));
            }
        }
        fclose($fd);
    }
    public function login($usr, $pwd){        
        $this->getPasswd();
        foreach ($this->users as $k => $v) {
            if($v["usr"] == $usr && $v["passwd"] == $pwd )
                return array("errcode"=>0, "data"=>"ok");
        }
        return array("errcode"=>1, "data"=>"faild");
    }
    private function loginTimeLimit(){
        $ip = $_SERVER['REMOTE_ADDR']; // 获取客户端 IP 地址
        $nt = microtime(true);
        // 文件路径，用于存储每个 IP 的访问记录
        $fn = sys_get_temp_dir() . "/frp_limit_{$ip}.txt";
        $usr = array(
            'ip'=>$ip
            // 获取当前时间戳 1736300613.7524
            , 'time'=>$nt
            , 'sum'=>0
            , 'lock'=>0
        );
        // 读取文件中的历史请求记录
        if (!file_exists($fn)) {
            $d = json_encode($usr, true);
            $fd = fopen($fn, "w+");
            fwrite($fd, $d, strlen($d));
            fclose($fd);
        }
        $ret = false;
        if (file_exists($fn)) {
            $usr = json_decode(file_get_contents($fn), true);

            $usr['sum']++;
            $tt = $nt - $usr['time'];
            if($usr['lock'] == '1'){
                if($tt > 5*60*60){ unlink($fn); }
                return true; 
            }

            if($tt < 1 && $usr['sum'] >5){
                $ret = true;
            }else if($tt < 10 && $usr['sum'] >10){
                $ret = true;
            }else if($tt < 30 && $usr['sum'] >30){
                $usr['lock'] == '1';
                $ret = true;
            }
            if(!$ret){
                $d = json_encode($usr, true);
                $fd = fopen($fn, "w+");
                fwrite($fd, $d, strlen($d));
                fclose($fd);
            }
            if($tt > 5*60 || $usr['sum']>35 ){ unlink($fn); }
        }

        return $ret;

    }
    public function faild($msg){
        if(null == $msg)
            $msg="You are not allowed to access this resource.";
        // 清除之前可能设置的所有输出缓冲
        while (ob_get_level()) ob_end_clean();
        // 设置HTTP响应状态码为403 Forbidden
        http_response_code(403);
        // 输出错误信息
        header('Content-Type: text/html; charset=utf-8');
        echo '<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>403 Forbidden</title>
        </head>
        <body>
            <h1>Error 403 - Forbidden</h1>
            <p>'.$msg.'</p>
        </body>
        </html>';
    }
}

    

new Author;