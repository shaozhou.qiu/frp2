<?php 
namespace EtSoftware;

include_once "base62converter.php";

class ID{
	private $time;
	private $data;
	private $b62;
	public function __construct($id=null){
		$this->b62 = new Base62Converter();		
		$this->time = time();		
		$this->data = array();
		if($id)
			$this->decode($id);
	}
	public function attach($data){
		array_push($this->data, $data);
		return $this;
	}	
	public function toString(){
		return $this->encode();
	}
	private function encode(){
		$reVal = $this->b62->formdec(
			time()
			. rand(0,99)
		);
		// $reVal = $this->b62->formdec(time());
		return $reVal;
	}
	private function decode($id){
		return $this->b62->todec($id);
	}
	
	
}