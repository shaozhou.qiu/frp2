<?php
namespace EtSoftware;

include_once "Shell7z.php";
include_once "ShellTar.php";
include_once "ShellISO.php";
include_once "Dir.php";

class Compress{
	
	public function __construct() {
	
	}
	private function app($fn){
		if(preg_match("/.*\.7z/im", $fn, $m)){
			return new Shell7z;
		}else if(preg_match("/.*\.iso/im", $fn, $m)){
			return new ShellISO;
		}else{
			return new ShellTar;
		}
		return null;
	}
    /*
     *  $src  源文件
     *  $fn   压缩后的文件名
     *  $path 指定$src存放在$fn哪个路径
     */
	public function compress($src, $fn, $path=null, $pwd=null){
        if($path){
            $droot = $_SERVER["DOCUMENT_ROOT"]."/tmp/".sha1($fn);
            $dir = new Dir;
            $dp = $dir->mkdirs($droot."/$path");
			$ret = array( 'errcode'=>2003 , 'data'=>"Err : Can't create path!" );
            if($dp){
			    $ret = array( 'errcode'=>2002 , 'data'=>"Err : Can't copy file!" );
                $nfn = preg_replace("/.*\/([^\/]+)$/im", "$1", $src);
                if(copy($src, "$dp/$nfn")){
                    $ret = $this->compress("$droot/", $fn);
                }else{
                	$ret['files'] = array($src);
                }
                $dir->rmdirs($droot);
            }
            return $ret;
        }
        if(preg_match("/^(.*)\/\*?$/im", $src, $m)){
            $fs = glob("$m[0]/*");
            foreach($fs as $f){
                $ret = $this->compress($f, $fn);
                if($ret['errcode'] != 0)return $ret;
            }
            return $ret;
        }
		$app = $this->app($fn);		
		if($app == null){
			return array("errcode"=>2001, "data"=>"Unknown format");
		}
		return $app->compress($src, $fn, $pwd);
		return array("errcode"=>0, "data"=>$ret);
	}

	// 解压
	public function decompress($fn, $des=null, $pwd=null){
		$app = $this->app($fn);		
		if($app == null){
			return array("errcode"=>2001, "data"=>"Unknown format");
		}
		return $app->decompress($fn, $des, $pwd);
	}
	
}
